if { [ info exists env(pfBoard) ] } { set pfBoard $env(pfBoard) } { set pfBoard "VCU118" }
if { [ info exists env(pfReg) ] } { set pfReg $env(pfReg) } { set pfReg "HGCal" }
if { [ info exists env(fakePuppi) ] } { set fakePuppi $env(fakePuppi) } { set fakePuppi "0" }
if { [ info exists env(nTest) ] } { set nTest $env(nTest) } { set nTest "48" }
if { [ info exists env(pfFreq) ] } { set pfFreq $env(pfFreq); } { set pfFreq "180";  }
if { [ info exists env(hgcalSide) ] } { set hgcalSide $env(hgcalSide) } { set hgcalSide 1 } 
if { [ info exists env(egID) ] } { set egID $env(egID) } { set egID "CompID" }

set postFreq "_${pfFreq}"
if { $pfFreq == 240 } {
     set regOutII 4;
} else {
     if { $pfFreq == 180 || $pfFreq == 360 } {
        set regOutII 6;
    }
}
if { $hgcalSide == "-1" } {
    set postFreq "${postFreq}_hgcNeg"
} elseif { $hgcalSide != "1" } {
    error "Unsupported hgcalSide ${hgcalSide}"
} 

set CMSSW_BASE $env(CMSSW_BASE)
set emulators "${CMSSW_BASE}/src/L1Trigger/Phase2L1ParticleFlow/src/"
set regionizerCFlags " -DPFFREQ=${pfFreq} -DREGOUTII=${regOutII} -DNTEST=${nTest} -DPUPPI_ISFAKE=${fakePuppi} -DHGCAL_SIDE=${hgcalSide}";

if { [ info exists env(gttLatency) ] } {
    set gttLatency $env(gttLatency);
    set regionizerCFlags "${regionizerCFlags} -DGTTLATENCY=${gttLatency}"
    set postFreq "${postFreq}_gttLatency${gttLatency}"
}


set cflags "-std=c++14 -I${CMSSW_BASE}/src -DREG_${pfReg} -DBOARD_${pfBoard} ${regionizerCFlags}"

set sample TTbar_PU200

set dumpFileName "${sample}_${pfReg}.dump"

if { ${pfReg} == "HGCal" && ${egID} == "CompID" } { 
    set cflags "${cflags} -DEGCOMPOSITEID" 
} elseif { ${pfReg} == "HGCal" && ${egID} == "EllID" } {
    set postFreq "_ellEgID${postFreq}"
    set dumpFileName "${sample}_${pfReg}Elliptic.dump"

}

open_project -reset "project_csim_layer1_${pfReg}_full_tmux6${postFreq}"


add_files -tb layer1_full_tmux6_test.cpp -cflags "${cflags}"
add_files -tb utils/tmux18_utils.cpp -cflags "${cflags}"
add_files -tb utils/hgcal_header_skipper.cpp -cflags "${cflags}"
add_files -tb ${CMSSW_BASE}/src/DataFormats/L1TParticleFlow/src/layer1_emulator.cpp -cflags "${cflags}"
add_files -tb ${CMSSW_BASE}/src/L1Trigger/Phase2L1ParticleFlow/data/compositeID.json  -cflags "${cflags}"
add_files -tb ${emulators}/l1-converters/tkinput_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/l1-converters/muonGmtToL1ct_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/l1-converters/hgcalinputt_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/pf/pfalgo_common_ref.cpp   -cflags "${cflags}"
add_files -tb ${emulators}/pf/pfalgo2hgc_ref.cpp   -cflags "${cflags}"
add_files -tb ${emulators}/puppi/linpuppi_ref.cpp   -cflags "${cflags}"
add_files -tb ${emulators}/regionizer/multififo_regionizer_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/regionizer/regionizer_base_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/egamma/pfeginput_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/egamma/pftkegalgo_ref.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-converters/pv/firmware/vertex.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-converters/muons/ref/muoninput_ref.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-regionizer/multififo/tdemux/firmware/tdemux.cpp   -cflags "${cflags}"
add_files -tb ../correlator-common/l1-regionizer/multififo/utils/dummy_obj_packers.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/utils/pattern_serializer.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/utils/test_utils.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/dumpfiles/${dumpFileName}

open_solution -reset "solution"
set_part {xcvu9p-flga2104-2L-e}
create_clock -period 2.5

csim_design -argv ${dumpFileName}

exit

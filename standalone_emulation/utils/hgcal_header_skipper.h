#ifndef hgcal_header_skipper_h
#define hgcal_header_skipper_h

#include <ap_int.h>

class HGCalHeaderSkipper {
    public: 
        HGCalHeaderSkipper() {}
        HGCalHeaderSkipper(unsigned int isector, unsigned int itslice, unsigned int ntowers) :
            isector_(isector), itslice_(itslice), ntowers_(ntowers),
            index_(-1)
            {}
        bool operator()(const ap_uint<64> data_in[4], 
                        const bool orbit_in[4], 
                        const bool start_in[4], 
                        const bool end_in[4], 
                        const bool valid_in[4], 
                        ap_uint<64> data_out[4], 
                        bool orbit_out[4],
                        bool start_out[4],
                        bool end_out[4],
                        bool valid_out[4]);
        bool operator()(const ap_uint<64> data_in[4],
                        const bool valid_in[4],
                        const bool orbit_in[4],
                        const bool start_in[4],
                        const bool end_in[4],
                        ap_uint<256 + 4> &word_out);
        bool operator()(const ap_uint<64> data_in[4], 
                        const bool orbit_in[4], 
                        const bool start_in[4], 
                        const bool end_in[4], 
                        const bool valid_in[4], 
                        ap_uint<256> & word_out, 
                        bool &orbit_out,
                        bool &start_out,
                        bool &end_out, 
                        bool &valid_out) ;
        void reset() { index_ = -1; }
    private:
        unsigned int isector_, itslice_, ntowers_;
        int index_;
        bool neworbit_;
        bool closed_;


};
#endif

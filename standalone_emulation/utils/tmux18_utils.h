#ifndef serenity_standalone_emulation_tmux18_utils_h
#define serenity_standalone_emulation_tmux18_utils_h

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <queue>
#include <cassert>
#include <algorithm>

#include "DataFormats/L1TParticleFlow/interface/layer1_emulator.h"


template<typename T, unsigned int TM6CLOCKS>
struct TM18LinkTriplet {
    struct Flags { 
        bool orbit, start, end, valid; 
        Flags(bool is_valid=false) : orbit(false), start(false), end(false), valid(is_valid) {}
        Flags(bool orbit_tag, bool start_event, bool end_event, bool is_valid=true) : 
            orbit(orbit_tag), start(start_event), end(end_event), valid(is_valid) {}
    };
    std::queue<std::pair<T,Flags>> links[3];
    unsigned int framesize_;
    
    TM18LinkTriplet(unsigned int framesize=3*TM6CLOCKS-3) :
        framesize_(framesize)
    {
        init();
    }

    void init() {
        for (auto & l : links) { while (!l.empty()) l.pop(); }
        for (int i = 0; i <  TM6CLOCKS; ++i)  links[1].emplace(T(0), false);
        for (int i = 0; i < 2*TM6CLOCKS; ++i) links[2].emplace(T(0), false);
    }

    template<typename C>
    void push_event(unsigned int iev, const C & objs, bool newOrbit, bool padWithValid=true) {
        auto & q = links[iev % 3];
        unsigned int nobj = objs.size();
        unsigned int nwords = nobj;
        unsigned int n = std::min<unsigned int>(framesize_, nwords); // let's leave 3 empty frames at the end, so they get reassebled as 1 row of nulls
        unsigned int iw = 0;
        for (unsigned int i = 0; i < n; ++i, ++iw) {
            q.emplace(objs[i], Flags(newOrbit && i == 0, i == 0, i == n-1)); 
        }

        for (; iw < 3*TM6CLOCKS; ++iw) {
            q.emplace(T(0), Flags(padWithValid ? (iw < framesize_) : (iw == 0)));
        }
    }

    void push_pause(unsigned int nclocks, bool valid=false) {
        for (auto & q : links) {
            for (unsigned int i = 0; i < nclocks; ++i) {
                q.emplace(T(0), false);
            }
        }
    }

    template<typename C>
    void pop_frame(C & channels, unsigned int start=0, unsigned int stride=1, bool maybenull=false) {
        for (int i = 0; i < 3; ++i) {
            if (maybenull && links[i].empty()) {
            	assert(start+i*stride < channels.size());
                channels.data [start+i*stride] = 0;
                channels.orbit[start+i*stride] = false;
                channels.start[start+i*stride] = false;
                channels.end[start+i*stride] = false;
                channels.valid[start+i*stride] = false;
                continue;
            }
            if (links[i].empty()) { 
                printf("ERROR: link %d is empty (start = %u, stride = %u)\n", i, start, stride); fflush(stdout); 
                continue;
            }
            assert(!links[i].empty());
            auto obj = links[i].front();
            if (start+i*stride >= channels.size()) {
            	printf("ERROR: link %d is out of bounds(start = %u, stride = %u, n = %u)\n", i, start, stride, channels.size());
            	fflush(stdout);
            }
        	assert(start+i*stride < channels.size());
            channels.data [start+i*stride] = obj.first;
            channels.orbit[start+i*stride] = obj.second.orbit;
            channels.start[start+i*stride] = obj.second.start;
            channels.end[start+i*stride] = obj.second.end;
            channels.valid[start+i*stride] = obj.second.valid;
            links[i].pop();
        }
    }

    bool anyEmpty() {
        for (auto & q : links) { 
            if (q.empty()) return true; 
        }
        return false;
    }

};

template<typename T, unsigned int TM6CLOCKS>
class TM18LinkMultiplet {

    public:
        TM18LinkMultiplet(unsigned int N, unsigned int framesize=3*TM6CLOCKS-3) :
            nlinks_(N), links_(N,TM18LinkTriplet<T,TM6CLOCKS>(framesize)) {}

        void init() {
            for (auto & l : links_) l.init();
        }

        template<typename O, typename E>
        void push_links(unsigned int iev, const std::vector<l1ct::DetectorSector<O>> & objs, const E & enc, bool newOrbit, bool padWithValid=true) {
            unsigned int nsec = objs.size();
            assert((nsec >= 1) && (nlinks_ >= nsec) && (nlinks_ % nsec == 0));
            unsigned int links_per_sec = nlinks_ / nsec;
            assert(links_per_sec == 1);
            for (unsigned int i = 0; i < nlinks_; ++i) {
                links_[i].push_event(iev, enc(objs[i]), newOrbit, padWithValid);
            }
        }

        template<unsigned int NWORDS>
        void push_links_parallel(unsigned int iev, const std::vector<l1ct::DetectorSector<ap_uint<64*NWORDS>>> & objs, bool newOrbit, bool padWithValid=true) {
            // saves big objects of NWORDS 64-bit words using in parallel the NBYTES links of each sector
            unsigned int nsec = objs.size();
            assert(nlinks_ == NWORDS*nsec);
            std::vector<ap_uint<64>> words;
            for (unsigned int i = 0; i < nsec; ++i) {
                unsigned int nobj = objs[i].size();
                words.resize(nobj);
                for (unsigned int b = 0; b < NWORDS; ++b) {
                    for (unsigned int j = 0; j < nobj; ++j) {
                        words[j] = objs[i][j](64*b+63,64*b);
                    }
                    links_[i*NWORDS+b].push_event(iev, words, newOrbit, padWithValid);
                }
            }
        }

        template<typename O, typename E>
        void push_link(unsigned int iev, const l1ct::DetectorSector<O> & objs, const E & enc, bool newOrbit) {
            assert(nlinks_ == 1);
            links_.front().push_event(iev, enc(objs), newOrbit);
        }

        void push_pause(unsigned int nclocks, bool valid=false) {
            for (auto & l : links_) {
                l.push_pause(nclocks, valid);
            }
        }


        template<typename C>
        void pop_frame(C & channels, unsigned int start=0, bool group_by_link=true, bool maybenull=false) {
            unsigned int stride = group_by_link ? 1 : nlinks_;
            for (unsigned int i = 0; i < nlinks_; ++i) {
                links_[i].pop_frame(channels, start + (group_by_link ? 3*i : i), stride, maybenull);
            }
        }

        template<unsigned int NWORDS, typename C>
        void pop_frame_parallel(C & channels, unsigned int start=0, bool group_by_link=true, bool maybenull=false) {
            unsigned int stride = group_by_link ? NWORDS : nlinks_;
            assert(nlinks_ % NWORDS == 0);
            for (unsigned int igroup = 0, i = 0; igroup < nlinks_/NWORDS; ++igroup) {
                for (unsigned int iw = 0; iw < NWORDS; ++iw, ++i) {
                    links_[i].pop_frame(channels, start + (group_by_link ? 3*NWORDS*igroup+iw : i), stride, maybenull);
                }
            }
        }

        bool anyEmpty() {
            for (auto & l : links_) if (l.anyEmpty()) return true;
            return false;
        }
    private: 
        unsigned int nlinks_;
        std::vector<TM18LinkTriplet<T,TM6CLOCKS>> links_;
};

class DelayQueue {
    public:
        DelayQueue(unsigned int n) : n_(n), data_(n, 0), ptr_(0) {}
        ap_uint<68> operator()(const ap_uint<68> & in) ;
        void operator()(const ap_uint<64>& in,
                        const bool& in_orbit,
                        const bool& in_start,
                        const bool& in_end,
                        const bool& in_valid,
                        ap_uint<64>& out,
                        bool& out_orbit,
                        bool& out_start,
                        bool& out_end,
                        bool& out_valid);

      private:
        unsigned int n_, ptr_;
        std::vector<ap_uint<68>> data_;
};



#endif

#include "hgcal_header_skipper.h"

bool HGCalHeaderSkipper::operator()(const ap_uint<64> data_in[4], 
                        const bool orbit_in[4], 
                        const bool start_in[4], 
                        const bool end_in[4], 
                        const bool valid_in[4], 
                        ap_uint<64> data_out[4], 
                        bool orbit_out[4],
                        bool start_out[4],
                        bool end_out[4],
                        bool valid_out[4])
{
    bool ok = true;
    if (index_ == -1) { // not initialized yet
        if (valid_in[0]) { // start of a packet
            assert(start_in[0] && !end_in[0]);
            neworbit_ = orbit_in[0];
            closed_ = false;
            for (unsigned int i = 0; i < 4; ++i) {
                data_out[i]  = 0;
                orbit_out[i] = false;
                start_out[i] = false;
                end_out[i] = false;
                valid_out[i] = false;
                if (orbit_in[i] != orbit_in[0] || 
                    start_in[i] != start_in[0] ||  
                    end_in[i] != end_in[0] || 
                    valid_in[i] != valid_in[0]) {
                    printf("HGCal Packet sync error: OSEV[0] = %d%d%d%d, OSEV[%u] = %d%d%d%d in header\n", 
                                int(orbit_in[0]),
                                int(start_in[0]),
                                int(end_in[0]),
                                int(valid_in[0]),
                                i, 
                                int(orbit_in[i]),
                                int(start_in[i]),
                                int(end_in[i]),
                                int(valid_in[i]));
                    ok = false;
                }
                unsigned int magic  = data_in[i](63,48);
                unsigned int tslice = data_in[i](39,32);
                unsigned int sector = data_in[i](31,24);
                unsigned int link   = data_in[i](23,16);
                if (magic != 0xABC0 || tslice != itslice_ || sector != isector_ || link != i) {
                    printf("HGCal Packet header error on sector %u (found %u), tslice %u (found %u), link %u (found %u), magic %x, data %s\n",
                            isector_, sector, itslice_, tslice, i, link, magic, data_in[i].to_string(16).c_str());
                    ok = false;
                }
            }
            if (ok) index_ = 0;
        } else { // idles
            for (unsigned int i = 0; i < 4; ++i) {
                data_out[i]  = 0;
                orbit_out[i] = false;
                start_out[i] = false;
                end_out[i] = false;
                valid_out[i] = false;
                assert(!start_in[i] && !end_in[i] && !orbit_in[i]);
            }
            neworbit_ = false;
        }
    } else if (index_ < ntowers_) { // towers
        for (unsigned int i = 0; i < 4; ++i) {
            if (!valid_in[i] || start_in[i] || (end_in[i] && index_ != ntowers_ - 1) || end_in[i] != end_in[0] || orbit_in[i]) {
              printf("HGCal Packet sync error: OSEV[%u] = %d%d%d%d in tower row %u\n",
                     i,
                     int(orbit_in[i]),
                     int(start_in[i]),
                     int(end_in[i]),
                     int(valid_in[i]),
                     index_);
              ok = false;
            }
            data_out[i]  = 0;
            orbit_out[i] = neworbit_;
            start_out[i] = index_ == 0;
            end_out[i] = end_in[i];
            valid_out[i] = true;
        }
        index_++;
        neworbit_ = false;
        closed_ = end_in[0];
    } else {
        if ((valid_in[0] && closed_) || start_in[0] || orbit_in[0] || (end_in[0] && !valid_in[0])) {
            printf("HGCal Packet error: OSEV[0] = %d%d%d%d in row %u, closed %d\n",
                int(orbit_in[0]),
                int(start_in[0]),
                int(end_in[0]),
                int(valid_in[0]),
                index_,
                int(closed_));
            ok = false;
        }
        for (unsigned int i = 0; i < 4; ++i) {
            if (orbit_in[i] != orbit_in[0] || start_in[i] != start_in[0] || end_in[i] != end_in[0] ||
                valid_in[i] != valid_in[0]) {
                printf("HGCal Packet sync error: OSEV[0] = %d%d%d%d, OSEV[%u] = %d%d%d%d in row %u\n",
                    int(orbit_in[0]),
                    int(start_in[0]),
                    int(end_in[0]),
                    int(valid_in[0]),
                    i,
                    int(orbit_in[i]),
                    int(start_in[i]),
                    int(end_in[i]),
                    int(valid_in[i]),
                    index_);
                ok = false;
            }
            orbit_out[i] = false;
            start_out[i] = false;
            end_out[i] = end_in[i];
            valid_out[i] = valid_in[i];
            data_out[i] = valid_in[i] ? data_in[i] : ap_uint<64>(0);
        } 
        if (end_in[0]) closed_ = true;
        if (!valid_in[0]) index_ = -1;
    }
    return ok;
}

bool HGCalHeaderSkipper::operator()(const ap_uint<64> data_in[4],
                                    const bool orbit_in[4],
                                    const bool start_in[4],
                                    const bool end_in[4],
                                    const bool valid_in[4],
                                    ap_uint<256+4>& word_out) {
  ap_uint<64> data_out[4];
  bool orbit_out[4], start_out[4], end_out[4], valid_out[4];
  bool ok = operator()(data_in, orbit_in, start_in, end_in, valid_in, data_out, orbit_out, start_out, end_out, valid_out);
  word_out[256+0] = orbit_out[0];
  word_out[256+1] = start_out[0];
  word_out[256+2] = end_out[0];
  word_out[256+3] = valid_out[0];
  for (unsigned int i = 0; i < 4; ++i) {
    word_out(64 * i + 63, 64 * i) = data_out[i];
  }
  return ok;
}

bool HGCalHeaderSkipper::operator()(const ap_uint<64> data_in[4],
                                    const bool orbit_in[4],
                                    const bool start_in[4],
                                    const bool end_in[4],
                                    const bool valid_in[4],
                                    ap_uint<256> & word_out, 
                                    bool &orbit_out,
                                    bool &start_out,
                                    bool &end_out, 
                                    bool & valid_out)
{
    ap_uint<64> data_out[4]; 
    bool orbit_tmp[4], start_tmp[4], end_tmp[4], valid_tmp[4];
    bool ok = operator()(data_in, orbit_in, start_in, end_in, valid_in, data_out, orbit_tmp, start_tmp, end_tmp, valid_tmp);
    orbit_out = orbit_tmp[0];
    start_out = start_tmp[0];
    end_out = end_tmp[0];
    valid_out = valid_tmp[0];
    for (unsigned int i = 0; i < 4; ++i) {
        word_out(64*i+63,64*i) = data_out[i];
    }
    return ok;
}


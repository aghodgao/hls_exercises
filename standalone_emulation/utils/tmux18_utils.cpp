#include "tmux18_utils.h"

ap_uint<68> DelayQueue::operator()(const ap_uint<68> & in) 
{
    ap_uint<68> ret = data_[ptr_];
    data_[ptr_] = in;
    ptr_++; 
    if (ptr_ == n_) ptr_ = 0;
    return ret;
}

void DelayQueue::operator()(const ap_uint<64>& in,
                        const bool& in_orbit,
                        const bool& in_start,
                        const bool& in_end,
                        const bool& in_valid,
                        ap_uint<64>& out,
                        bool& out_orbit,
                        bool& out_start,
                        bool& out_end,
                        bool& out_valid) 
{
    ap_uint<68> in68  = in; 
    in68[64] = in_orbit;
    in68[65] = in_start;
    in68[66] = in_end;
    in68[67] = in_valid;
    ap_uint<68> out68 = (*this)(in68);
    out = out68(63,0); 
    out_orbit = out68[64];
    out_start = out68[65];
    out_end   = out68[66];
    out_valid = out68[67];
}

#ifndef serenity_standalone_emulation_utils_channel_h
#define serenity_standalone_emulation_utils_channel_h

#include "../../correlator-common/utils/pattern_serializer.h"

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>
#include <memory>

template<unsigned int NCHANN, unsigned int NBITS>
class Channels {
    public:
        Channels() { clear(); }
        Channels(const char *name, unsigned int nmux = 1, const char *id = "myID") :
            nchann64_((((NCHANN*((NBITS+63)/64))+(nmux-1))/nmux)*nmux), filename_(name), id_(id), nmux_(nmux), fileindex_(0)
        {
            if (filename_.find("%") == std::string::npos) {
                serializer_.reset(new PatternSerializer(name, id_, nchann64_, nmux_));
            } else {
                newFile();
            }
            clear(); 
        }

        Channels<NCHANN,NBITS> & copy(const Channels & other) {
            for (unsigned int i = 0; i < NCHANN; ++i) {
                copy(other, i, i);
            }
            return *this;
        }

        template<unsigned int NOTHER> 
        Channels<NCHANN,NBITS> & copy(const Channels<NOTHER,NBITS> & other, unsigned int isrc, unsigned int idst) {
            data[idst] = other.data[isrc]; 
            orbit[idst] = other.orbit[isrc];
            start[idst] = other.start[isrc];
            end[idst] = other.end[isrc];
            valid[idst] = other.valid[isrc];
            return *this;
        }

        Channels<NCHANN,NBITS> & init(bool orbit_tag, bool start_event, bool end_event, bool isvalid) {
            for (unsigned int i = 0; i < NCHANN; ++i) {
                data[i] = 0; 
                orbit[i] = orbit_tag && (i%nmux_ == 0);
                start[i] = start_event && (i%nmux_ == 0);
                end[i] = end_event && ((i%nmux_) == (nmux_-1));
                valid[i] = isvalid;
            }
            return *this; 
        }

        Channels<NCHANN,NBITS> & clear(bool isvalid=false) {
            return init(false,false,false,isvalid);
        }

        Channels<NCHANN,NBITS> & setOrbit(bool orbit_tag=false) {
            for (unsigned int i = 0; i < NCHANN; ++i) {
                orbit[i] = orbit_tag;
            }
            return *this; 
        }

        Channels<NCHANN,NBITS> & setStart(bool start_event=false) {
            for (unsigned int i = 0; i < NCHANN; ++i) {
                start[i] = start_event;
            }
            return *this; 
        }

        Channels<NCHANN,NBITS> & setEnd(bool end_event=false) {
            for (unsigned int i = 0; i < NCHANN; ++i) {
                end[i] = end_event;
            }
            return *this; 
        }


        unsigned int size() const { return NCHANN; }

        Channels<NCHANN,NBITS> &  dump() { 
            serializer_->packAndWrite(NCHANN, data, orbit, start, end, valid); 
            return *this; 
        }
        Channels<NCHANN,NBITS> &  dumpNCopies(unsigned int N) { 
            static bool falses[NCHANN]; std::fill(falses, falses+NCHANN, false);
            for (unsigned int j = 0; j < N; ++j) {
              serializer_->packAndWrite(
                  NCHANN, data, j == 0 ? orbit : falses, j == 0 ? start : falses, j == N - 1 ? end : falses, valid);
            }
            return *this; 
        }

        Channels<NCHANN,NBITS> &  dumpNulls(unsigned int n, bool valid=false) {
            clear(valid);
            for (unsigned int i = 0; i < n; ++i) dump();
            return *this; 
        }

        Channels<NCHANN,NBITS> & newFile() {
            fileindex_++;
            unsigned int maxlen = filename_.size()+10;
            char buff[maxlen];
            std::fill(buff,buff+maxlen,'\0');
            snprintf(buff, maxlen-1, filename_.c_str(), fileindex_);
            serializer_.reset(new PatternSerializer(buff, id_, nchann64_, nmux_));
            return *this; 
        }

        void getOrNull(unsigned int ilink, const std::vector<ap_uint<NBITS>> & vec, unsigned int index, bool orbit_tag) {
            assert(ilink < NCHANN);
            assert(!vec.empty());
            data[ilink] = (index < vec.size()) ? vec[index] : ap_uint<NBITS>(0);
            orbit[ilink] = orbit_tag;
            start[ilink] = (index == 0);
            end[ilink]   = (index == std::max<unsigned>(vec.size(),1)-1);
            valid[ilink] = (index < vec.size());
        }

        ap_uint<NBITS+4> wext(int ilink) {
            assert(ilink < NCHANN);
            ap_uint<NBITS+4> ret = data[ilink];
            ret[NBITS+0] = orbit[ilink]; 
            ret[NBITS+1] = start[ilink]; 
            ret[NBITS+2] = end[ilink]; 
            ret[NBITS+3] = valid[ilink]; 
            return ret;
        }

        void setExt(int ilink, const ap_uint<NBITS+4> & word) {
            data[ilink] = word(NBITS-1,0);
            orbit[ilink] = word[NBITS+0]; 
            start[ilink] = word[NBITS+1]; 
            end[ilink] = word[NBITS+2]; 
            valid[ilink] = word[NBITS+3]; 
        }

        ap_uint<NBITS> data[NCHANN];
        bool orbit[NCHANN];
        bool start[NCHANN];
        bool end[NCHANN];
        bool valid[NCHANN];
    private:
        std::string filename_, id_; 
        unsigned int nchann64_, nmux_, fileindex_;
        std::unique_ptr<PatternSerializer> serializer_;
};

#endif

#ifndef REG_HGCalNoTK
#define REG_HGCalNoTK
#endif
#define ROUTER_STREAM
#define ROUTER_ISMUX 1
#define ROUTER_ISSTREAM 1
#ifndef PFFREQ
#define PFFREQ 240
#define REGOUTII 2
#define REGPAUSEII 1
#endif
#include "../correlator-common/l1-regionizer/multififo/firmware/regionizer.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/regionizer/multififo_regionizer_ref.h"
#include "../correlator-common/utils/pattern_serializer.h"
#include "../correlator-common/utils/test_utils.h"
#include "../correlator-common/utils/DumpFileReader.h"
#include "../correlator-common/l1-regionizer/multififo/utils/dummy_obj_packers.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/tdemux_ref.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/firmware/tdemux.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/puppi/linpuppi_ref.h"
#include "../correlator-common/puppi/firmware/linpuppi.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/common/bitonic_hybrid_sort_ref.h"
#include "../correlator-common/l1-converters/pv/firmware/vertex.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/hgcalinput_ref.h"
#include "utils/tmux18_utils.h"
#include "utils/channels.h"
#include "utils/channels.h"
#include "utils/hgcal_header_skipper.h"

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <vector>
#include <memory>

#define TLEN REGIONIZERNCLOCKS 
#define NRAWCALOFIBERS 4

#ifndef GTTLATENCY
#if PFFREQ == 240
#define GTTLATENCY 3*(TLEN-2)+10
#else
#define GTTLATENCY 3*(TLEN-2)+120
#endif
#endif

#ifndef NTEST
#define NTEST 48
#endif

#define PAUSES

struct Tester {

    Tester(const std::string & inputFile) :
        inputs(inputFile.c_str()),
        channelsTM("input-emp.txt"), 
        channelsTDemux("input-emp-tdemux.txt.gz"),
        channelsVU9P("input-emp-vu9p.%03d.txt.gz"),
        channelsVU13P("input-emp-vu13p.%03d.txt.gz"),
        channelsTDemuxSplit("input-emp-tdemux.%03d.txt.gz"),
        channelsDecode("input-emp-decoded.txt.gz"), 
        channelsIn("input-emp-decoded-ref.txt.gz"),
        channelsReg("output-emp-regionized-ref.txt.gz"),
        channelsRegSplit("output-emp-regionized-ref.%03d.txt.gz"),
        channelsPuppi("output-emp-puppi-ref.txt.gz"),
        channelsPuppiSort("output-emp-puppisort-ref.txt.gz"),
        channelsPuppiSortStream("output-emp-puppisort-stream-ref.txt.gz",3),
        channelsPuppiSortStreamSplit("output-emp-puppisort-stream-ref.%03d.txt.gz",3),
        vu9p_links(nchann_vu9p, 0), // index is tmux link, value is VCU118 link
        vu13p_links(nchann_vu13p, 0), // index is tmux link, value is vu13p link
        decoded_validation_index(0),
        calo_tmuxer(NCALOSECTORS*NRAWCALOFIBERS), 
        pv_gttlatency(GTTLATENCY), // real delay from GTT latency (for input files)
        pv_delayer(TLEN*2+1),      // fake delay to realign with decoded inputs for validation
        mu_delayer(TLEN*2+1),
        regEmulator (/*nendcaps=*/2, REGIONIZERNCLOCKS, 2, 3, /*NTRACK*/ 0, NCALO, /*NEMCALO*/ 0, /*NMU*/ 0, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, /*atVertex=*/true),
        regEmulator2(/*nendcaps=*/2, REGIONIZERNCLOCKS, 2, 3, /*NTRACK*/ 0, NCALO, /*NEMCALO*/ 0, /*NMU*/ 0, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, /*atVertex=*/true),
        puEmulator(/*NTRACK=*/0,
                      NCALO,
                      NCALO,
                      NPV,
                      LINPUPPI_DR2MIN,
                      LINPUPPI_DR2MAX,
                      LINPUPPI_iptMax,
                      LINPUPPI_dzCut,
                      LINPUPPI_ptSlopeNe,
                      LINPUPPI_ptSlopePh,
                      LINPUPPI_ptZeroNe,
                      LINPUPPI_ptZeroPh,
                      LINPUPPI_alphaSlope,
                      LINPUPPI_alphaZero,
                      LINPUPPI_alphaCrop,
                      LINPUPPI_priorNe,
                      LINPUPPI_priorPh,
                      LINPUPPI_ptCut),
        hgcalDecoderEmulator(),
        regInit(false), 
        frame(0)
    { 
        initLinks();
        for (unsigned int isec = 0; isec < NCALOSECTORS; ++isec) {
            for (unsigned int itime = 0; itime < 3; ++itime) {
                calo_skipper[3*isec+itime] = HGCalHeaderSkipper(isec, itime*6, 30); // 30 clock cycles of towers
            }
        }
    }
        
    ~Tester() {
        printf("Processes %u events with %u tk, %u had, %u em, %u mu. Found %u pf, %u puppi, %u sorted puppi, %u e/g, %u ele\n",
                        cnt_.ev, cnt_.tk, cnt_.had, cnt_.em, cnt_.mu, cnt_.pf, cnt_.puppi, cnt_.puppisort, cnt_.eg, cnt_.ele);
    }

    static const unsigned int nchann_in =  3*NCALOSECTORS*NRAWCALOFIBERS, nchann_vu9p = 29*4, nchann_vu13p = 32*4;
    static const unsigned int nchann_demux = 3*NCALOSECTORS*NRAWCALOFIBERS;
    static const unsigned int nchann_decoded = NCALOSECTORS*NCALOFIBERS;
    static const unsigned int nchann_regionized =  NCALOOUT + 1;
    static const unsigned int nchann_pf =  NCALO , nchann_puppi =  NCALO, nchann_sort = NPUPPIFINALSORTED;
    static const unsigned int  calo_offs = 0;

    DumpFileReader inputs;
    Channels<nchann_in,64> channelsTM;
    Channels<nchann_demux,64> channelsTDemux, channelsTDemuxSplit;
    Channels<nchann_vu9p,64> channelsVU9P;
    Channels<nchann_vu13p,64> channelsVU13P;
    Channels<nchann_decoded,72> channelsDecode, channelsIn;
    Channels<nchann_regionized,72> channelsReg, channelsRegSplit;
    Channels<nchann_puppi,64> channelsPuppi;
    Channels<nchann_sort,64> channelsPuppiSort, channelsPuppiSortStream, channelsPuppiSortStreamSplit;

    std::vector<int> vu9p_links; // index is tmux link, value is VCU118 link
    std::vector<int> vu13p_links; // index is tmux link, value is VU13P link

    static const unsigned int decoded_validation_size = 3*TLEN+1;
    ap_uint<72> decoded_validation_data[decoded_validation_size][nchann_decoded];
    bool        decoded_validation_valid[decoded_validation_size][nchann_decoded];
    unsigned int decoded_validation_index;
    
    // TMux encoders
    TM18LinkMultiplet<ap_uint<64>,TLEN>  calo_tmuxer;
    HGCalHeaderSkipper calo_skipper[3*NCALOSECTORS];
    // TMux decoders, for testing
    TDemuxRef<256> calo_tdemuxer[NCALOSECTORS];
    // make a delay queue for the PV and the muons, to realign them to the first frame
    DelayQueue pv_gttlatency, pv_delayer, mu_delayer; // latency of the TDemuxRef (measured from first valid frame in to first valid frame out)

    l1ct::MultififoRegionizerEmulator regEmulator; // for clock-by-clock outputs
    l1ct::MultififoRegionizerEmulator regEmulator2; // run all in once
    l1ct::LinPuppiEmulator puEmulator;
    l1ct::HgcalClusterDecoderEmulator hgcalDecoderEmulator;
 
    bool regInit;
    unsigned int frame;

    struct Counters {
        unsigned int ev = 0, tk = 0, had = 0, em = 0, mu = 0, pf = 0, puppi = 0, puppisort = 0, eg = 0, ele = 0;
    } cnt_;
    template<typename T> static unsigned int count_nz(const T & c) { 
        unsigned int ret = 0;
        for (auto & o : c) { if (o.hwPt != 0) ret++; }
        return ret;
    }

    void initLinks();
    void decodeRawClusters(const std::vector<l1ct::DetectorSector<ap_uint<256>>> & raw, std::vector<l1ct::DetectorSector<l1ct::HadCaloObjEmu>> & in) ;
    void readTwoEndcaps(int itest, bool newOrbit, l1ct::RawInputs & raw, l1ct::RegionizerDecodedInputs & in, std::vector<l1ct::PFInputRegion> & allpfin) ;
    bool runTMuxAndDemux(int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RawInputs & raw)  ;
    void runRegionizer(const l1ct::RegionizerDecodedInputs & in, const std::vector<l1ct::PFInputRegion> & allpfin, unsigned int nclocks, int indexWithinTrain, bool tailOfTrain) ;
    bool runPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) ;

    bool run() ;
};

void Tester::initLinks() 
{
    // make a version for the VCU118 kit and VU9P with the proper link mapping
    // -- in the VU9P:
    //          regions 6-23 = 72 fibers mapped to HGCal
    for (int i =  0; i < 3*NCALOSECTORS*NRAWCALOFIBERS; ++i) vu9p_links[calo_offs+i] = i + 4*6;

    // make a version for the VU13 with the proper link mapping
    // -- in the VU13P:
    //          regions 6-23 = 72 fibers mapped to HGCal
    for (int i =  0; i < 3*NCALOSECTORS*NRAWCALOFIBERS; ++i) vu13p_links[calo_offs+i] = i + 4*6;

}

void Tester::decodeRawClusters(const std::vector<l1ct::DetectorSector<ap_uint<256>>> & raw, std::vector<l1ct::DetectorSector<l1ct::HadCaloObjEmu>> & in) {
    in.resize(raw.size());
    for (unsigned int isec = 0, nsec = raw.size(); isec < nsec; ++isec) {
        in[isec].region = raw[isec].region;
        in[isec].clear();
        for (const auto &w256 : raw[isec].obj) {
            if (w256) {
                in[isec].obj.push_back(hgcalDecoderEmulator.decode(w256));
                if (in[isec].obj.back().hwPt == 0) in[isec].obj.pop_back();
            }
        }
    }
}
void Tester::readTwoEndcaps(int itest,  bool newOrbit, l1ct::RawInputs & raw, l1ct::RegionizerDecodedInputs & in, std::vector<l1ct::PFInputRegion> & allpfin)
{
    raw.clear(); in.clear(); allpfin.clear();
    for (auto & sec : inputs.event().raw.hgcalcluster)  raw.hgcalcluster.push_back(sec);

    for (unsigned int i = 0, n = inputs.event().pfinputs.size(); i < n; ++i) {
        const auto & reg = inputs.event().pfinputs[i];
        allpfin.push_back(reg);
    }

    decodeRawClusters(raw.hgcalcluster, in.hadcalo);

    if (!regInit) { regEmulator.initSectorsAndRegions(in, allpfin); regInit = true; } // this will do clock-cycle emulation
    regEmulator2.run(in, allpfin); // this also but just internally, from the outside it will seem it took no time

    std::vector<l1ct::DetectorSector<ap_uint<256>>> hgcalFullPacket;
    for (unsigned int isec = 0; isec < raw.hgcalcluster.size(); ++isec) {
        hgcalFullPacket.emplace_back();
        auto & osec = hgcalFullPacket.back();
        osec.region = raw.hgcalcluster[isec].region;
        osec.obj.reserve(31+raw.hgcalcluster[isec].size());
        ap_uint<256> w256; ap_uint<64> head64;
        for (unsigned int w = 0; w < 4; ++w) {
            head64(63,48) = 0xABC0;       // Magic
            head64(47,38) = 0;            // Opaque
            head64(39,32) = (itest%3)*6;  // TM slice
            head64(31,24) = isec;         // Sector
            head64(23,16) = w;            // link
            head64(15,0)  = itest % 3564; // BX
            w256(64*w+63,64*w) = head64;
        }
        osec.obj.push_back(w256);
        for (unsigned int trow = 0; trow < 30; ++trow) {
            for (unsigned int w = 0; w < 4; ++w) {
                w256(64*w+63,64*w) = 4*trow + w;
            }
            osec.obj.push_back(w256);
        }
        for (const auto & c : raw.hgcalcluster[isec]) {
            osec.obj.push_back(c);
        }
    }
    calo_tmuxer.push_links_parallel<4>(itest, hgcalFullPacket, newOrbit, /*padWithValid=*/false);
}

bool Tester::runTMuxAndDemux(int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RawInputs & raw)
{
    bool ok = true;
    int ilink;
    for (int iclock = 0; iclock < nclocks; ++iclock, ++frame) {
        // pop out frames from the tmuxer for printing. for each sector, we put the 3 links for 3 set of events next to each other
        calo_tmuxer.pop_frame_parallel<4>(channelsTM, calo_offs, true, tailOfTrain);


        channelsTM.dump();

        // make also version with vcu118 link mapping
        for (ilink = 0; ilink < channelsTM.size();  ++ilink) {
            channelsVU9P.data[vu9p_links[ilink]] = channelsTM.data[ilink];
            channelsVU9P.orbit[vu9p_links[ilink]] = channelsTM.orbit[ilink];
            channelsVU9P.start[vu9p_links[ilink]] = channelsTM.start[ilink];
            channelsVU9P.end[vu9p_links[ilink]] = channelsTM.end[ilink];
            channelsVU9P.valid[vu9p_links[ilink]] = channelsTM.valid[ilink];
        }
        channelsVU9P.dump();
        // make also version with vu13p link mapping
        for (ilink = 0; ilink < channelsTM.size();  ++ilink) {
            channelsVU13P.data[vu13p_links[ilink]] = channelsTM.data[ilink];
            channelsVU13P.orbit[vu13p_links[ilink]] = channelsTM.orbit[ilink];
            channelsVU13P.start[vu13p_links[ilink]] = channelsTM.start[ilink];
            channelsVU13P.end[vu13p_links[ilink]] = channelsTM.end[ilink];
            channelsVU13P.valid[vu13p_links[ilink]] = channelsTM.valid[ilink];
        }
        channelsVU13P.dump();

        // now let's run the time demultiplexer
        for (int s = 0; s < NCALOSECTORS; ++s) {
            ap_uint<256+4> calo_tmux[3], calo_tdemux[3];
            for (unsigned int t = 0; t < 3; ++t) {
                ilink = calo_offs + (3*s+t)*NRAWCALOFIBERS;
                ok = ok && calo_skipper[3 * s + t](&channelsTM.data[ilink],
                                                   &channelsTM.orbit[ilink],
                                                   &channelsTM.start[ilink],
                                                   &channelsTM.end[ilink],
                                                   &channelsTM.valid[ilink],
                                                   calo_tmux[t]);
            }
            calo_tdemuxer[s](calo_tmux, calo_tdemux);
            assert(NCALOFIBERS == 3);
            for (int t = 0; t < 3; ++t) {
                ilink = calo_offs + (3*s+t)*NRAWCALOFIBERS;
                for (int f = 0; f < NRAWCALOFIBERS; ++f) {
                    channelsTDemux.data[ ilink + f] = calo_tdemux[t](64*f+63,64*f);
                    channelsTDemux.orbit[ilink + f] = calo_tdemux[t][256+0];
                    channelsTDemux.start[ilink + f] = calo_tdemux[t][256+1];
                    channelsTDemux.end[ilink + f] = calo_tdemux[t][256+2];
                    channelsTDemux.valid[ilink + f] = calo_tdemux[t][256+3];
                }
            }
        }


        channelsTDemux.dump();
        channelsTDemuxSplit.copy(channelsTDemux).dump();

        // and now we unpack and decode
        ilink = 0; unsigned int iout = 0;
        for (int s = 0; s < NCALOSECTORS; ++s) {
            assert(NRAWCALOFIBERS == 4);
            for (int c = 0; c < NCALOFIBERS; ++c) {
                ap_uint<256> hgcword = 0;
                bool valid = true, orbit = false, start = false, end = false;
                for (int b = 0; b < 4; ++b) {
                    hgcword(b*64+63,b*64) = channelsTDemux.data[ilink  + 4*c + b];
                    valid = valid &&        channelsTDemux.valid[ilink + 4*c + b];
                    orbit = orbit ||        channelsTDemux.orbit[ilink + 4*c + b];
                    start = start ||        channelsTDemux.start[ilink + 4*c + b];
                    end   = end   ||        channelsTDemux.end[ilink + 4*c + b];
                }
                channelsDecode.data[iout]  = hgcalDecoderEmulator.decode(hgcword).pack();
                channelsDecode.valid[iout] = valid;
                channelsDecode.orbit[iout] = orbit;
                channelsDecode.start[iout] = start;
                channelsDecode.end[iout]   = end;
                iout++;
            }
            ilink += 3*NRAWCALOFIBERS;
        }
        channelsDecode.dump();

        // VALIDATION
        if (indexWithinTrain >= 2 && (!tailOfTrain ? iclock > 0 : iclock <= 2*TLEN)) {
            bool validation_ok = true;
            int ishift = (!tailOfTrain) ? -3*TLEN-1 : -2*TLEN-1;
            unsigned int dvi = (decoded_validation_index + iclock + ishift + decoded_validation_size) % (decoded_validation_size);
            for (unsigned int i = 0; i < iout; ++i) {
                if (channelsDecode.data[i]  != decoded_validation_data[dvi][i] ||
                    channelsDecode.valid[i] != decoded_validation_valid[dvi][i]) {
                    if (validation_ok == true) { // print this only once
                        std::cout << "error in decoded validation at itest " << itest << " (within train " << indexWithinTrain << ", tail? " << tailOfTrain << ")" <<
                                        ", iclock " << iclock << " (tail? " << tailOfTrain <<") vs index " << dvi << ", decoded_validation_index " << decoded_validation_index << std::endl;
                        validation_ok = false;
                    }
                    std::cout << "channel " << i << " : " <<
                                 "expected " << decoded_validation_data[dvi][i].to_string(16) << " (valid? " << decoded_validation_valid[dvi][i] << "), " <<
                                 "found " << channelsDecode.data[i].to_string(16) << " (valid? " << channelsDecode.valid[i] << ")" << std::endl;
                    ok = false;
                }
            }
            if (!ok) break;
        }

    }
    return ok;

}

void Tester::runRegionizer(const l1ct::RegionizerDecodedInputs & in, const std::vector<l1ct::PFInputRegion> & allpfin, unsigned int nclocks, int indexWithinTrain, bool tailOfTrain) 
{
    int ilink;
    for (int iclock = 0; iclock < nclocks; ++iclock) {
        // emulate regionizer
        std::vector<l1ct::TkObjEmu> tk_links_in, tk_out; // not used but needed by interface
        std::vector<l1ct::EmCaloObjEmu> em_links_in, em_out;  // not used but needed by interface
        std::vector<l1ct::HadCaloObjEmu> calo_links_in, calo_out;
        std::vector<l1ct::MuObjEmu> mu_links_in, mu_out;  // not used but needed by interface
        std::vector<bool> tk_links_valid, calo_links_valid, mu_links_valid;

        if (!tailOfTrain) {
            //regEmulator.fillLinks(iclock, in, tk_links_in);
            // for HGCal, we have first 11 clock cycles of null words while header & towers are transmitted
            if (iclock >= 11) {
                regEmulator.fillLinks(iclock-11, in, calo_links_in, calo_links_valid);
            } else {
                calo_links_in.resize(NCALOSECTORS * NCALOFIBERS); // null data
                calo_links_valid.resize(NCALOSECTORS * NCALOFIBERS);  // null data
                for (auto & c : calo_links_in) c.clear();
                std::fill(calo_links_valid.begin(), calo_links_valid.end(), iclock != 0);
            }
            cnt_.had += count_nz(calo_links_in);

            ilink = 0; channelsIn.init(indexWithinTrain == 0 && iclock == 1, iclock == 0, false, (iclock < TLEN-1));
            for (int isec = 0, icalo = 0; isec < NCALOSECTORS; ++isec) {
                for (int ifiber = 0; ifiber < NCALOFIBERS; ++ifiber, ++icalo, ++ilink) {
                    channelsIn.valid[ilink] = calo_links_valid[icalo];
                    channelsIn.data[ilink] = calo_links_in[icalo].pack();
                }
            }
            channelsIn.dump();

            // put good decoded data for validation in the ring buffer
            for (unsigned int i = 0; i < nchann_decoded; ++i) {
                decoded_validation_data[decoded_validation_index][i] = channelsIn.data[i];
                decoded_validation_valid[decoded_validation_index][i] = channelsIn.valid[i];
            }
            decoded_validation_index = (decoded_validation_index + 1) % (decoded_validation_size);
        } else {
            calo_links_in.resize(NCALOSECTORS*NCALOFIBERS);
            for (auto & c : calo_links_in) c.clear();
        }

        bool newevt_ref = (iclock == 0);
        regEmulator.step(newevt_ref,
                         tk_links_in, calo_links_in, em_links_in, mu_links_in,
                         tk_out, calo_out, em_out, mu_out,
                         ROUTER_ISMUX);

        unsigned int ireg = iclock / (REGOUTII + REGPAUSEII);
        bool is_pause = (iclock % (REGOUTII + REGPAUSEII) >= REGOUTII);
        bool region_valid = ireg < allpfin.size();
        ilink = 0;
        bool orbit = (indexWithinTrain == 1) && iclock == 0;
        bool start = (iclock % (REGOUTII + REGPAUSEII)) == 0;
        bool end = (iclock % (REGOUTII + REGPAUSEII)) == REGOUTII - 1;
        channelsReg.init(orbit, start, end, region_valid && !is_pause); 
        for (int i = 0; i < NCALOOUT; ++i) 
            channelsReg.data[ilink++] = calo_out[i].pack_slim(); 
        channelsReg.data[ilink++] = region_valid ? allpfin[ireg].region.pack() : ap_uint<l1ct::PFRegion::BITWIDTH>(0);

        if (indexWithinTrain > 0) {
            channelsReg.dump();
            channelsRegSplit.copy(channelsReg).dump();
        }
    }
}

bool Tester::runPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) 
{

    l1ct::OutputRegion pfout;
    int ilink;
    cnt_.ev++;
    bool ok = true;
  std::vector<l1ct::PuppiObjEmu> outallne_nocut, outallne, outselne, outsort;

  for (int ireg = 0, nreg = allpfin.size(); ireg < nreg; ++ireg) {
    if (debug)
      printf("Will run forward puppi for event %d, region %d\n", itest, ireg);
    puEmulator.setDebug(debug);
    puEmulator.fwdlinpuppi_ref(
        allpfin[ireg].region, allpfin[ireg].hadcalo, outallne_nocut, outallne, outselne);
    cnt_.puppi += count_nz(outallne);
    ilink = 0;
    channelsPuppi.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg - 1, true);
    for (auto &pup : outallne)
      channelsPuppi.data[ilink++] = pup.pack();
    outallne.resize(NCALO);

    outsort.resize(NPUPPIFINALSORTED);
    for (auto &p : outsort)
      p.clear();
    hybrid_bitonic_sort_and_crop_ref(
        NCALO, std::min<unsigned int>(NCALO, NPUPPIFINALSORTED), &outallne[0], &outsort[0]);
    ilink = 0;
    channelsPuppiSort.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg - 1, true);
    for (auto &pup : outsort)
      channelsPuppiSort.data[ilink++] = pup.pack();
    cnt_.puppisort += count_nz(outsort);

#ifndef NO_CHECK_CMSSW_REF
    const auto &cmsswPuppi = inputs.event().out[ireg].puppi;
    bool match = count_nz(outsort) == count_nz(cmsswPuppi);
    for (int i = 0, n = std::min<int>(outallne.size(), cmsswPuppi.size()); i < n; ++i) {
      if (!puppi_equals(cmsswPuppi[i], outsort[i], "PuppiFwd", i))
        match = false;
    }
    if (debug || !match) {
      printf("Done comparison between this puppi (%u) and the CMSSW one (%u). %s\n",
             count_nz(outsort),
             count_nz(cmsswPuppi),
             match ? "ok" : "FAILED");
      // if (!match) ok = false; // FIXME
    }
#endif

    const unsigned int OUTII360 = REGOUTII + REGPAUSEII;
    channelsPuppi.dumpNCopies(OUTII360);
    channelsPuppiSort.dumpNCopies(OUTII360);
    channelsPuppiSortStream.copy(channelsPuppiSort).dump();
    channelsPuppiSortStreamSplit.copy(channelsPuppiSort).dump();

    if (!ok) break;
  }
    for (int ireg = 0; ireg < NPFREGIONS; ++ireg) {
        if (debug) printf("Will run Puppi FWD event %d, region %d\n", itest, ireg);
    }



    return ok;
}



bool Tester::run() {
    unsigned int events_per_chunk = 12;
    unsigned int frame = 0, ilink; 
    bool ok = true, firstOfTrain = true;

    if (NTEST == 0) {
        for (int itest = 0; inputs.nextEvent(); ++itest) {
            printf("Processing event %d run:lumi:event %u:%u:%lu -> ",
                    itest, inputs.event().run, inputs.event().lumi, inputs.event().event);
            bool stop = false;
            for (int ireg = 0; ireg < 18; ++ireg) {
                int npuppi = count_nz(inputs.event().out[ireg].puppi);
                if (npuppi > 0) printf("reg%d: %d, ", ireg, npuppi);
                if (npuppi > 0 && ireg == 0) stop = true;
            }
            printf("\n");
            if (stop) break;
        }
        return true;
    }
    if (NTEST == 48) { // skip to a test with some candidate in region 0
        for (int i = 0; i < 10; ++i) {
            if (!inputs.nextEvent()) return false;
        }
    }
    for (int itest = 0, trainStart = 0; itest < NTEST; ++itest) {
        if (!inputs.nextEvent()) break;
        printf("Processing event %d run:lumi:event %u:%u:%lu (train Start %d, index in train %d)\n",
                itest, inputs.event().run, inputs.event().lumi, inputs.event().event, trainStart, itest-trainStart);

        // now we make a single endcap setup
        l1ct::RawInputs raw; l1ct::RegionizerDecodedInputs in; std::vector<l1ct::PFInputRegion> allpfin;
        readTwoEndcaps(itest, itest - trainStart <= 2, raw, in, allpfin);
        if (itest == 0) {
            printf("Calo eta: center %d --> center %d, halfsize %d , extra %d \n", in.hadcalo.front().region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
        }

        runRegionizer(in, allpfin, TLEN, itest - trainStart, /*tail=*/false);
        ok = runPuppi(itest, itest == trainStart, allpfin, itest <= 1) && ok;
        
        ok = runTMuxAndDemux(itest, itest - trainStart, TLEN, /*tail=*/false, raw) && ok;

        #ifdef PAUSES
        if (!ok) break;
        if (itest - trainStart == (events_per_chunk-1)) { // put a pause after N events
            unsigned int pause_length = 300;
            assert(pause_length > GTTLATENCY);
            // flush data: regionizer
            runRegionizer(in, allpfin, TLEN, itest - trainStart, /*tail=*/true);
            // flush data and add pause; tdemux and inputs
            ok = runTMuxAndDemux(itest + 1, itest + 1 - trainStart, pause_length, /*tail=*/true, raw) && ok;
            // add pause in the expected outputs
            channelsIn.dumpNulls(pause_length);
            channelsReg.dumpNulls(pause_length);
            channelsPuppi.dumpNulls(pause_length);
            channelsPuppiSort.dumpNulls(pause_length);
            channelsPuppiSortStream.dumpNulls(pause_length/3);
            // re-init muxers
            calo_tmuxer.init(); 
            for (unsigned int isec = 0; isec < NCALOSECTORS; ++isec) {
                for (unsigned int itime = 0; itime < 3; ++itime) {
                    calo_skipper[3*isec+itime].reset();
                }
            }
            decoded_validation_index = 0; frame = 0;
            trainStart = itest + 1;
        }
        #endif
        if (((itest % events_per_chunk) == (events_per_chunk-1)) && (itest != NTEST-1)) { 
            channelsVU9P.newFile(); 
            channelsVU13P.newFile(); 
            channelsTDemuxSplit.newFile();
            channelsRegSplit.newFile();
            channelsPuppiSortStreamSplit.newFile();
        }
        if (!ok) break;
    } 

    return ok;
}

int main(int argc, char **argv) {
    Tester test(argv[1]);
    return test.run() ? 0 : 1;
}

#ifndef LAYER1_TMUX18
#define LAYER1_TMUX18
#endif
#ifndef REG_HGCal
#define REG_HGCal
#endif
#define ROUTER_STREAM
#define ROUTER_ISMUX 1
#define ROUTER_ISSTREAM 1
#ifndef PFFREQ
#define PFFREQ 240
#define REGOUTII 6
#define REGPAUSEII 3
#endif

#include <typeinfo>
#include "../correlator-common/egamma/l1-input/firmware/pfeginput.h"
#include "../correlator-common/l1-converters/muons/firmware/muonGmtToL1ct.h"
#include "../correlator-common/l1-converters/muons/firmware/muoninput.h"
#include "../correlator-common/l1-converters/muons/ref/muoninput_ref.h"
#include "../correlator-common/l1-converters/pv/firmware/vertex.h"
#include "../correlator-common/l1-converters/tracks/firmware/tkinput_hgcal.h"
#include "../correlator-common/l1-regionizer/multififo/firmware/regionizer.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/firmware/tdemux.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/tdemux_ref.h"
#include "../correlator-common/l1-regionizer/multififo/utils/dummy_obj_packers.h"
#include "../correlator-common/pf/firmware/pfalgo2hgc.h"
#include "../correlator-common/puppi/firmware/linpuppi.h"
#include "../correlator-common/utils/DumpFileReader.h"
#include "../correlator-common/utils/pattern_serializer.h"
#include "../correlator-common/utils/test_utils.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface//egamma/pfeginput_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/common/bitonic_hybrid_sort_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/egamma/pftkegalgo_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/egamma/pftkegsorter_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/hgcalinput_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/muonGmtToL1ct_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/tkinput_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/pf/pfalgo2hgc_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/puppi/linpuppi_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/regionizer/buffered_folded_multififo_regionizer_ref.h"
#include "utils/channels.h"
#include "utils/hgcal_header_skipper.h"
#include "utils/tmux18_utils.h"

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <memory>
#include <vector>

#define TLEN REGIONIZERNCLOCKS
#define NRAWCALOFIBERS 4
#define NCALOFWD 12

#ifndef GTTLATENCY
#define GTTLATENCY TLEN + 120
#endif

#ifndef NTEST
#define NTEST 48
#endif

#define PAUSES

#if defined(EGCOMPOSITEID)
    bool doComposite = true;
    // FIXME: check what is in the CMSSW config
    float trkQualityPtMin = 1.;
#else
    bool doComposite = false;
    float trkQualityPtMin = 10.;
#endif

struct Tester {
  Tester(const std::string &inputSample)
      : inputs((inputSample + "_HGCal.dump").c_str()),
        fwdinputs((inputSample + "_HGCalNoTK.dump").c_str()),
        channelsTM("input-emp.txt"),
        channelsVU9P("input-emp-vu9p.%03d.txt.gz"),
        channelsVU13P("input-emp-vu13p.%03d.txt.gz"),
        channelsDecode("input-emp-decoded.txt.gz"),
        channelsIn("input-emp-decoded-ref.txt.gz"),
        channelsReg("output-emp-regionized-ref.txt.gz"),
        channelsRegSplit("output-emp-regionized-ref.%03d.txt.gz"),
        channelsRegFwd("output-emp-regionized-fwd-ref.txt.gz"),
        channelsRegFwdSplit("output-emp-regionized-fwd-ref.%03d.txt.gz"),
        channelsPf("output-emp-pf-ref.txt.gz"),
        channelsPuppi("output-emp-puppi-ref.txt.gz"),
        channelsPuppiSort("output-emp-puppisort-ref.txt.gz"),
        channelsPuppiSortStream("output-emp-puppisort-stream-ref.txt.gz", REGOUTII + REGPAUSEII),
        channelsPuppiSortStreamSplit("output-emp-puppisort-stream-ref.%03d.txt.gz", REGOUTII + REGPAUSEII),
        channelsPuppiFwd("output-emp-puppi-fwd-ref.txt.gz"),
        channelsPuppiFwdSort("output-emp-puppisort-fwd-ref.txt.gz"),
        channelsPuppiFwdSortStream("output-emp-puppisort-fwd-stream-ref.txt.gz", REGOUTII + REGPAUSEII),
        channelsPuppiFwdSortStreamSplit("output-emp-puppisort-fwd-stream-ref.%03d.txt.gz", REGOUTII + REGPAUSEII),
        channelsEg("output-emp-eg-ref.txt.gz"),
        channelsEgTkIso("output-emp-egtkiso-ref.txt.gz"),
        channelsEgTkIsoStream("output-emp-egtkiso-ref.%03d.txt.gz"),
        channelsEgSorted("output-emp-egsorted-ref.txt.gz"),
        channelsEgSortedStream("output-emp-egsorted-ref.%03d.txt.gz"),
        vu9p_links(nchann_vu9p,
                   0),                 // index is tmux link, value is vu9p link
        vu13p_links(nchann_vu13p, 0),  // index is tmux link, value is vu13p link
        pv_gttlatency(GTTLATENCY),     // real delay from GTT latency (for input files)
        pv_gttlatency_short(TLEN+5),   // fake delay for tests on the board injecting a single channel 
        regEmulator(
            REGIONIZERNCLOCKS, NTRACK, NCALO, NEMCALO, NMU, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, /*atVertex=*/true),
        regEmulatorFwd(REGIONIZERNCLOCKS, 0, NCALOFWD, 0, 0, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, /*atVertex=*/true),
        regEmulator2(
            REGIONIZERNCLOCKS, NTRACK, NCALOFWD, NEMCALO, NMU, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, /*atVertex=*/true),
        regEmulator2Fwd(REGIONIZERNCLOCKS, 0, NCALOFWD, 0, 0, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, /*atVertex=*/true),
        pfEmulator(NTRACK,
                   NCALO,
                   NMU,
                   NCALO,
                   PFALGO_DR2MAX_TK_MU,
                   PFALGO_DR2MAX_TK_CALO,
                   PFALGO_TK_MAXINVPT_LOOSE,
                   PFALGO_TK_MAXINVPT_TIGHT),
        puEmulator(NTRACK,
                   NALLNEUTRALS,
                   NALLNEUTRALS,
                   NPV,
                   LINPUPPI_DR2MIN,
                   LINPUPPI_DR2MAX,
                   LINPUPPI_iptMax,
                   LINPUPPI_dzCut,
                   l1ct::Scales::makeGlbEta(LINPUPPI_etaCut),
                   LINPUPPI_ptSlopeNe,
                   LINPUPPI_ptSlopeNe_1,
                   LINPUPPI_ptSlopePh,
                   LINPUPPI_ptSlopePh_1,
                   LINPUPPI_ptZeroNe,
                   LINPUPPI_ptZeroNe_1,
                   LINPUPPI_ptZeroPh,
                   LINPUPPI_ptZeroPh_1,
                   LINPUPPI_alphaSlope,
                   LINPUPPI_alphaSlope_1,
                   LINPUPPI_alphaZero,
                   LINPUPPI_alphaZero_1,
                   LINPUPPI_alphaCrop,
                   LINPUPPI_alphaCrop_1,
                   LINPUPPI_priorNe,
                   LINPUPPI_priorNe_1,
                   LINPUPPI_priorPh,
                   LINPUPPI_priorPh_1,
                   LINPUPPI_ptCut,
                   LINPUPPI_ptCut_1),
        puEmulatorFwd(/*NTRACK=*/0,
                      NCALOFWD,
                      NCALOFWD,
                      NPV,
                      LINPUPPI_FWD_DR2MIN,
                      LINPUPPI_FWD_DR2MAX,
                      LINPUPPI_FWD_iptMax,
                      LINPUPPI_FWD_dzCut,
                      LINPUPPI_FWD_ptSlopeNe,
                      LINPUPPI_FWD_ptSlopePh,
                      LINPUPPI_FWD_ptZeroNe,
                      LINPUPPI_FWD_ptZeroPh,
                      LINPUPPI_FWD_alphaSlope,
                      LINPUPPI_FWD_alphaZero,
                      LINPUPPI_FWD_alphaCrop,
                      LINPUPPI_FWD_priorNe,
                      LINPUPPI_FWD_priorPh,
                      LINPUPPI_FWD_ptCut),
        egEmulator(l1ct::PFTkEGAlgoEmuConfig(NTRACK,
                                             NTRACK_EGIN,
                                             NEMCALO_EGIN,
                                             NEM_EGOUT,
                                             /*filterHwQuality=*/false,
                                             /*doBremRecovery=*/true,
                                             /*writeBeforeBremRecovery=*/false,
                                             /*caloHwQual=*/1,
                                             /*doEndcapQuality=*/true,
                                             /* emClusterPtMin= */0.,
                                             /*dEtaMaxBrem=*/0.02,
                                             /*dPhiMaxBrem=*/0.1,
                                             /*absEtaBoundaries=*/{0.0, 1.5},
                                             /*dEtaValues=*/{0.015, 0.01},
                                             /*dPhiValues=*/{0.07, 0.07},
                                             /*trkQualityPtMin=*/trkQualityPtMin,
                                             /*doComposite=*/doComposite,
                                             /*NTRACK_PER_EMCALO_EGCOMP=*/NTRACK_PER_EMCALO_EGCOMP,
                                             /* writeEgSta = */ false,
                                             /*tkIsoParams_tkEle = */ {2., 0.6, 0.03, 0.2},
                                             /*tkIsoParams_tkEm =  */ {2., 0.6, 0.07, 0.3},
                                             /*pfIsoParams_tkEle = */ {1., 0.6, 0.03, 0.2},
                                             /*pfIsoParams_tkEm =  */ {1., 0.6, 0.07, 0.3},
                                             /* doTkIso = */ true,
                                             /* doPfIso = */ false,
                                             /* hwIsoTypeTkEle = */ l1ct::EGIsoEleObjEmu::IsoType::TkIso,
                                             /* hwIsoTypeTkEm =  */ l1ct::EGIsoObjEmu::IsoType::TkIsoPV,
                                             /* compIDparams = */ {-0.732422, 0.214844, "compositeID.json"},
                                             /* debug = */ 0)),
        egSorterEmulator(NEM_EGOUT, NL1_EGOUT),
        tkDecoderEmulator(l1ct::TrackInputEmulator::Region::Endcap, l1ct::TrackInputEmulator::Encoding::Biased, /*bitwise=*/true, /*slim=*/false),
        muDecoderEmulator(GMT_Z0_SCALE, GMT_DXY_SCALE),
        hgcalDecoderEmulator(),
        regInit(false),
        frame(0) {
    initLinks();
    const float ptErr_edges[PTERR_BINS] = PTERR_EDGES;
    const float ptErr_offss[PTERR_BINS] = PTERR_OFFS;
    const float ptErr_scales[PTERR_BINS] = PTERR_SCALE;
    pfEmulator.loadPtErrBins(PTERR_BINS, ptErr_edges, ptErr_scales, ptErr_offss);
    l1ct::EGInputSelectorEmuConfig emSelCfg(EGINPUT_BITMASK, NCALO, NEMCALO, false);
    regEmulator.setEgInterceptMode(/*afterFifo=*/true, emSelCfg);
    regEmulator2.setEgInterceptMode(/*afterFifo=*/true, emSelCfg);
    tkDecoderEmulator.configPhi(phi_BITS);
    tkDecoderEmulator.configZ0(z0_BITS);
    tkDecoderEmulator.configDEtaHGCal(
        calcDEtaHGCal_BITS, calcDEtaHGCal_Z0S, calcDEtaHGCal_RS, calcDEtaHGCal_LUTBITS, calcDEtaHGCal_LUTSHIFT);
    tkDecoderEmulator.configDPhiHGCal(calcDPhiHGCal_BITS,
                                      calcDPhiHGCal_Z0S1,
                                      calcDPhiHGCal_Z0S2,
                                      calcDPhiHGCal_RINVSHIFT,
                                      calcDPhiHGCal_TANLSHIFT,
                                      calcDPhiHGCal_LUTBITS,
                                      0.0);
    tkDecoderEmulator.configEta(tanlLUT_BITS, 0, 15 - tanlLUT_BITS, tanlLUT_POSTOFFS, true, true);
    tkDecoderEmulator.configPt(ptLUT_BITS);

    for (unsigned int isec = 0; isec < 2 * NCALOSECTORS; ++isec) {
      calo_skipper[isec] = HGCalHeaderSkipper(isec, 0, 30);  // 30 clock cycles of towers
    }
  }

  ~Tester() {
    printf(
        "Processes %u events with %u tk, %u had, %u em, %u mu. Found %u pf, "
        "%u puppi, %u sorted puppi, %u fwd puppi, %u fwd sorted puppi, %u e/g, %u ele\n",
        cnt_.ev,
        cnt_.tk,
        cnt_.had,
        cnt_.em,
        cnt_.mu,
        cnt_.pf,
        cnt_.puppi,
        cnt_.puppisort,
        cnt_.puppifwd,
        cnt_.puppisortfwd,
        cnt_.eg,
        cnt_.ele);
  }

  static const unsigned int nchann_in = 2 * NTKSECTORS + 2 * NCALOSECTORS * NRAWCALOFIBERS + 1 + 1,
                            nchann_vu9p = 30 * 4, nchann_vu13p = 32 * 4;
  static const unsigned int nchann_decoded = 2 * NTKSECTORS + 2 * NCALOSECTORS + NMUFIBERS + 1;
  static const unsigned int nchann_regionized = NTKOUT + NCALOOUT + NEMOUT + NMUOUT + 1;
  static const unsigned int nchann_regionized_fwd = NCALOFWDOUT + 1;
  static const unsigned int nchann_pf = NTRACK + NCALO + NMU, nchann_puppi = NTRACK + NCALO,
                            nchann_sort = NPUPPIFINALSORTED;
  static const unsigned int nchann_puppi_fwd = NCALOFWD, nchann_sort_fwd = NPUPPIFINALSORTED;
  static const unsigned int nchann_eg = NEM_EGOUT + NEM_EGOUT, nchann_eg64 = NEM_EGOUT + 2 * NEM_EGOUT,
                            nchann_egsort = 1;
  static const unsigned int tk_offs = 0, calo_offs = 2 * NTKSECTORS;
  static const unsigned int mu_offs_raw = calo_offs + 2 * NCALOSECTORS * NRAWCALOFIBERS,
                            mu_offs_decoded = calo_offs + 2 * NCALOSECTORS;
  static const unsigned int vtx_offs_raw = mu_offs_raw + 1, vtx_offs_decoded = mu_offs_decoded + 1;

  DumpFileReader inputs, fwdinputs;
  Channels<nchann_in, 64> channelsTM;
  Channels<nchann_vu9p, 64> channelsVU9P;
  Channels<nchann_vu13p, 64> channelsVU13P;
  Channels<nchann_decoded, 128> channelsDecode, channelsIn;
  Channels<nchann_regionized, 128> channelsReg, channelsRegSplit;
  Channels<nchann_regionized_fwd, 128> channelsRegFwd, channelsRegFwdSplit;
  Channels<nchann_pf, 72> channelsPf;
  Channels<nchann_eg, 76> channelsEg, channelsEgTkIso;
  Channels<(nchann_eg64 + REGOUTII - 1) / REGOUTII, 64> channelsEgTkIsoStream;
  Channels<nchann_egsort, 64> channelsEgSorted, channelsEgSortedStream;
  Channels<nchann_puppi, 64> channelsPuppi;
  Channels<nchann_sort, 64> channelsPuppiSort, channelsPuppiSortStream, channelsPuppiSortStreamSplit;
  Channels<nchann_puppi_fwd, 64> channelsPuppiFwd;
  Channels<nchann_sort_fwd, 64> channelsPuppiFwdSort, channelsPuppiFwdSortStream, channelsPuppiFwdSortStreamSplit;

  std::vector<int> vu9p_links;   // index is tmux link, value is VU9P link
  std::vector<int> vu13p_links;  // index is tmux link, value is VU13P link

  ap_uint<128> decoded_validation_data[TLEN][nchann_decoded];
  bool decoded_validation_valid[TLEN][nchann_decoded];

  HGCalHeaderSkipper calo_skipper[2 * NCALOSECTORS];
  DelayQueue pv_gttlatency, pv_gttlatency_short;  // simulate GTT latency

  l1ct::BufferedFoldedMultififoRegionizerEmulator regEmulator, regEmulatorFwd;    // for clock-by-clock outputs
  l1ct::BufferedFoldedMultififoRegionizerEmulator regEmulator2, regEmulator2Fwd;  // run all in once
  l1ct::PFAlgo2HGCEmulator pfEmulator;
  l1ct::LinPuppiEmulator puEmulator;
  l1ct::LinPuppiEmulator puEmulatorFwd;
  l1ct::PFTkEGAlgoEmulator egEmulator;
  l1ct::PFTkEGSorterEmulator egSorterEmulator;
  l1ct::TrackInputEmulator tkDecoderEmulator;
  l1ct::GMTMuonDecoderEmulator muDecoderEmulator;
  l1ct::HgcalClusterDecoderEmulator hgcalDecoderEmulator;

  bool regInit;
  unsigned int frame;

  struct Counters {
    unsigned int ev = 0, tk = 0, had = 0, em = 0, mu = 0, pf = 0, puppi = 0, puppisort = 0, puppifwd = 0,
                 puppisortfwd = 0, eg = 0, ele = 0;
  } cnt_;
  template <typename T>
  static unsigned int count_nz(const T &c) {
    unsigned int ret = 0;
    for (auto &o : c) {
      if (o.hwPt != 0)
        ret++;
    }
    return ret;
  }

  void initLinks();
  void decodeRawTracks(const std::vector<l1ct::DetectorSector<ap_uint<96>>> &raw,
                       std::vector<l1ct::DetectorSector<l1ct::TkObjEmu>> &in,
                       bool debug);
  void decodeRawMuons(const l1ct::DetectorSector<ap_uint<64>> &raw,
                      l1ct::DetectorSector<l1ct::MuObjEmu> &in,
                      bool debug);
  void decodeRawClusters(const std::vector<l1ct::DetectorSector<ap_uint<256>>> &raw,
                         std::vector<l1ct::DetectorSector<l1ct::HadCaloObjEmu>> &in,
                         bool debug);
  void readEndcaps(int itest,
                   l1ct::RawInputs &raw,
                   l1ct::RegionizerDecodedInputs &in,
                   l1ct::RegionizerDecodedInputs &infwd,
                   std::vector<l1ct::PFInputRegion> &allpfin,
                   std::vector<l1ct::PFInputRegion> &allpfinfwd,
                   bool debug);
  bool runTMuxAndDemux(
      int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RawInputs &raw, bool debug);
  void runRegionizer(const l1ct::RegionizerDecodedInputs &in,
                     const l1ct::RegionizerDecodedInputs &infwd,
                     const std::vector<l1ct::PFInputRegion> &allpfin,
                     const std::vector<l1ct::PFInputRegion> &allpfinfwd,
                     unsigned int nclocks,
                     unsigned int indexWithinTrain,
                     bool tailOfTrain);
  bool runPFPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> &allpfin, bool debug);
  bool runPuppiFwd(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> &allpfinfwd, bool debug);
  bool runTkEg(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> &allpfin, bool debug);

  bool run();
};

void Tester::initLinks() {
  // make a version for the VU9P with the proper link mapping
  // -- in the tmux vector, we have the first NTKSECTORS*3 mapped to TF, then 3*NCALOSECTORS*NRAWCALOFIBERS, then 3 muons and 1 PV
  // -- in the VU9P:
  //          region 27 fibers 1 mapped to muons
  //          region 27 fiber 4 mapped to PV
  //          regions 0-2 + 28-29 = 18 fibers mapped to tracker (2x9 fibers)
  //          regions 12-17  = 24 fibers mapped to HGCal
  vu9p_links[mu_offs_raw] = 4 * 27 + 0;
  vu9p_links[vtx_offs_raw] = 4 * 27 + 3;
  for (int i = 0; i < 2 * NTKSECTORS; ++i)
    vu9p_links[tk_offs + i] = (i <= 11) ? (i + 4 * 0) : ((i - 12) + 4 * 28);
  for (int i = 0, ic = 0; i < 2 * NCALOSECTORS * NRAWCALOFIBERS; ++i) {
    vu9p_links[calo_offs + i] = 4 * 12 + i;
  }
  for (int i =  0; i <= vtx_offs_raw; ++i) {
      printf("Input link %3d mapped to VU9P channel %3d\n", i, vu9p_links[i]);
  }

  // make a version for the VU13 with the proper link mapping
  // -- in the tmux vector, we have the first NTKSECTORS*3 mapped to TF, then 3*NCALOSECTORS*NRAWCALOFIBERS, then 3 muons and 1 PV
  // -- in the VU13P:
  //          region 27 fibers 1 mapped to muons
  //          region 27 fiber 4 mapped to PV
  //          regions 0-2 + 30-31 = 18 fibers mapped to tracker (2x9 fibers)
  //          regions 13-18  = 24 fibers mapped to HGCal
  vu13p_links[mu_offs_raw] = 4 * 27 + 0;
  vu13p_links[vtx_offs_raw] = 4 * 27 + 3;
  for (int i = 0; i < 2 * NTKSECTORS; ++i)
    vu13p_links[tk_offs + i] = (i <= 11) ? (i + 4 * 0) : ((i - 12) + 4 * 30);
  for (int i = 0, ic = 0; i < 2 * NCALOSECTORS * NRAWCALOFIBERS; ++i) {
    vu13p_links[calo_offs + i] = 4 * 13 + i;
  }
  for (int i = 0; i <= vtx_offs_raw; ++i) {
    printf("Input link %3d mapped to VU13P channel %3d\n", i, vu13p_links[i]);
  }
}

void Tester::decodeRawTracks(const std::vector<l1ct::DetectorSector<ap_uint<96>>> &raw,
                             std::vector<l1ct::DetectorSector<l1ct::TkObjEmu>> &in,
                             bool debug) {
  in.resize(raw.size());
  for (unsigned int isec = 0, nsec = raw.size(); isec < nsec; ++isec) {
    in[isec].region = raw[isec].region;
    in[isec].clear();
    for (const auto &w96 : raw[isec].obj) {
      auto out = tkDecoderEmulator.decodeTrack(w96, raw[isec].region, true);
      if (debug && w96 != 0)
        std::cout << "Ideal decoder, sec " << isec << ", track word " << w96.to_string(16) << ", ok?" << out.second
                  << ", tkobj " << out.first.pack().to_string(16) << " (region "
                  << raw[isec].region.pack().to_string(16) << ")" << std::endl;
      if (!out.second)
        out.first.clear();
      in[isec].obj.push_back(out.first);
    }
  }
}
void Tester::decodeRawMuons(const l1ct::DetectorSector<ap_uint<64>> &raw,
                            l1ct::DetectorSector<l1ct::MuObjEmu> &in,
                            bool debug) {
  in.clear();
  in.region = raw.region;
  for (const auto &w64 : raw.obj) {
    in.obj.push_back(muDecoderEmulator.decode(w64));
    if (debug && w64 != 0)
      std::cout << "Ideal decoder, muon word " << w64.to_string(16) << ", muobj " << in.obj.back().pack().to_string(16)
                << std::endl;
  }
}
void Tester::decodeRawClusters(const std::vector<l1ct::DetectorSector<ap_uint<256>>> &raw,
                               std::vector<l1ct::DetectorSector<l1ct::HadCaloObjEmu>> &in,
                               bool debug) {
  in.resize(raw.size());
  for (unsigned int isec = 0, nsec = raw.size(); isec < nsec; ++isec) {
    in[isec].region = raw[isec].region;
    in[isec].clear();
    for (const auto &w256 : raw[isec].obj) {
      if (w256) {
        in[isec].obj.push_back(hgcalDecoderEmulator.decode(w256));
        if (debug && w256 != 0)
          std::cout << "Ideal decoder, sec " << isec << ", cluster word " << w256.to_string(16) << ", ok?"
                    << (in[isec].obj.back().hwPt != 0) << ", calo_obj " << in[isec].obj.back().pack().to_string(16)
                    << std::endl;
        if (in[isec].obj.back().hwPt == 0)
          in[isec].obj.pop_back();
      }
    }
  }
}
void Tester::readEndcaps(int itest,
                         l1ct::RawInputs &raw,
                         l1ct::RegionizerDecodedInputs &in,
                         l1ct::RegionizerDecodedInputs &infwd,
                         std::vector<l1ct::PFInputRegion> &allpfin,
                         std::vector<l1ct::PFInputRegion> &allpfinfwd,
                         bool debug) {
  raw.clear();
  in.clear();
  allpfin.clear();
  allpfinfwd.clear();
  raw.track = inputs.event().raw.track;
  raw.hgcalcluster = inputs.event().raw.hgcalcluster;
  raw.muon = inputs.event().raw.muon;
  raw.muon.obj.resize(NMUON_PROMPT, ap_uint<64>(0));

  in.emcalo = inputs.event().decoded.emcalo;
  allpfin = inputs.event().pfinputs;
  allpfinfwd = fwdinputs.event().pfinputs;

  decodeRawTracks(raw.track, in.track, debug);
  decodeRawMuons(raw.muon, in.muon, debug);
  decodeRawClusters(raw.hgcalcluster, in.hadcalo, debug);

  infwd = in;
  infwd.clear();
  infwd.hadcalo = in.hadcalo;

  if (!regInit) {
    regEmulator.initSectorsAndRegions(in, allpfin);
    regEmulatorFwd.initSectorsAndRegions(infwd, allpfinfwd);
    regInit = true;
  }                               // this will do clock-cycle emulation
  regEmulator2.run(in, allpfin);  // this also but just internally, from the
                                  // outside it will seem it took no time
  for (unsigned int i = 0; i < 2 * NTKSECTORS; ++i) {
    if (raw.track[i].obj.empty()) {
      assert(in.track[i].obj.empty());
      raw.track[i].obj.emplace_back(0);
      in.track[i].obj.emplace_back();
      in.track[i].obj.back().clear();
    }
  }
}

bool Tester::runTMuxAndDemux(
    int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RawInputs &raw, bool debug) {
  bool ok = true;
  assert(raw.track.size() == 2 * NTKSECTORS);
  assert(raw.hgcalcluster.size() == 2 * NCALOSECTORS);
  std::vector<std::vector<ap_uint<64>>> packed_tracks(2 * NTKSECTORS);
  for (unsigned int isec = 0; isec < 2 * NTKSECTORS; ++isec) {
    unsigned int ilink = 2 * (isec % NTKSECTORS) + (isec / NTKSECTORS);
    packed_tracks[ilink] = pack_w96(raw.track[isec]);
  }
  std::vector<std::vector<ap_uint<64>>> packed_hgcal(2 * NCALOSECTORS * NRAWCALOFIBERS);
  assert(NRAWCALOFIBERS == 4);
  for (unsigned int isec = 0; isec < 2 * NCALOSECTORS; ++isec) {
    for (unsigned int w = 0; w < 4; ++w) {
      unsigned int ilink = (2 * (isec % NCALOSECTORS) + (isec / NCALOSECTORS));
      std::vector<ap_uint<64>> &link = packed_hgcal[ilink * NRAWCALOFIBERS + w];
      link.reserve(31 + raw.hgcalcluster[isec].size());
      ap_uint<64> head64;
      head64(63, 48) = 0xABC0;       // Magic
      head64(47, 38) = 0;            // Opaque
      head64(39, 32) = 0;            // TM slice
      head64(31, 24) = isec;         // Sector
      head64(23, 16) = w;            // link
      head64(15, 0) = itest % 3564;  // BX
      link.push_back(head64);
      // fake towers
      for (unsigned int trow = 0; trow < 30; ++trow) {
        link.emplace_back(4 * trow + w);
      }
      // custers
      for (const auto &c : raw.hgcalcluster[isec]) {
        link.emplace_back(c(64 * w + 63, 64 * w));
      }
    }
  }

  ap_uint<64> tk_reminder[2 * NTKSECTORS];
  for (int iclock = 0; iclock < nclocks; ++iclock, ++frame) {
    bool orbit_tag = (indexWithinTrain == 0) && (iclock == 0);
    // put in tracker links
    for (unsigned int i = 0, ilink = tk_offs; i < 2 * NTKSECTORS; ++i, ++ilink) {
      channelsTM.getOrNull(ilink, packed_tracks[i], iclock, orbit_tag);
    }
    // hgcal links
    for (unsigned int i = 0, ilink = calo_offs; i < 2 * NCALOSECTORS * NRAWCALOFIBERS; ++i, ++ilink) {
      channelsTM.getOrNull(ilink, packed_hgcal[i], iclock, orbit_tag);
    }
    // muons
    channelsTM.getOrNull(mu_offs_raw, raw.muon.obj, iclock, orbit_tag);
    // if this is not a real event, clear up
    if (tailOfTrain) {
      channelsTM.clear(false);
    }
    // PV, delayed by GTT latency
    ap_uint<64> pvword_gtt = !tailOfTrain ? inputs.event().pv_emu(iclock) : ap_uint<64>(0);
    bool pvword_gtt_orbit =  !tailOfTrain ? orbit_tag : false;
    bool pvword_gtt_start =  !tailOfTrain ? (iclock == 0)                 : false;
    bool pvword_gtt_end   =  !tailOfTrain ? (iclock == 9)                 : false;
    bool pvword_gtt_valid =  !tailOfTrain ? (iclock < 10)                 : false;
    pv_gttlatency(pvword_gtt, 
                  pvword_gtt_orbit, 
                  pvword_gtt_start, 
                  pvword_gtt_end, 
                  pvword_gtt_valid, 
                  channelsTM.data[vtx_offs_raw], 
                  channelsTM.orbit[vtx_offs_raw], 
                  channelsTM.start[vtx_offs_raw], 
                  channelsTM.end[vtx_offs_raw], 
                  channelsTM.valid[vtx_offs_raw]);
    channelsTM.dump();


    // make a version with vu9p link mapping and lower GTT latency
    pv_gttlatency_short(pvword_gtt, 
              pvword_gtt_orbit, 
              pvword_gtt_start, 
              pvword_gtt_end, 
              pvword_gtt_valid, 
              channelsVU9P.data[vu9p_links[vtx_offs_raw]], 
              channelsVU9P.orbit[vu9p_links[vtx_offs_raw]], 
              channelsVU9P.start[vu9p_links[vtx_offs_raw]], 
              channelsVU9P.end[vu9p_links[vtx_offs_raw]], 
              channelsVU9P.valid[vu9p_links[vtx_offs_raw]]);
    for (unsigned int ilink = 0; ilink < vtx_offs_raw; ++ilink) {
      channelsVU9P.copy(channelsTM, ilink, vu9p_links[ilink]);
    }
    channelsVU9P.dump(); 

    // make a version with vu13p link mapping
    for (unsigned int ilink = 0; ilink < vtx_offs_raw; ++ilink) {
      channelsVU13P.copy(channelsTM, ilink, vu13p_links[ilink]);
    }
    channelsVU13P.copy(channelsVU9P, vu9p_links[vtx_offs_raw], vu13p_links[vtx_offs_raw]);
    channelsVU13P.dump();

    // and now we unpack and decode
    for (int s = 0; s < 2 * NTKSECTORS; ++s) {
      unsigned int ilink = 2 * (s % NTKSECTORS) + (s / NTKSECTORS) + tk_offs;
      unsigned int iout = ilink;
      // for 1 link, the sequence is
      //   | 0  | 63-0  | track 0 bits 63-0 |
      //   | 1  | 31-0  | track 0 bits 96-64 |
      //   | 1  | 63-32 | track 1 bits 31-0 |
      //   | 2  | 63-0  | track 1 bits 96-32 |
      //   | 3  | 63-0  | track 2 bits 63-0 |
      //   | 4  | 31-0  | track 2 bits 96-64 |
      //   | 4  | 63-32 | track 3 bits 31-0 |
      //   | 5  | 63-0  | track 3 bits 96-32 |
      ap_uint<96> tkword = 0;
      switch (iclock % 3) {
        case 0:
          tk_reminder[ilink] = channelsTM.data[ilink];
          tkword = 0;
          break;
        case 1:
          tkword(63, 0) = tk_reminder[ilink];
          tkword(95, 64) = channelsTM.data[ilink](31, 0);
          tk_reminder[ilink](31, 0) = channelsTM.data[ilink](63, 32);
          break;
        case 2:
          tkword(31, 0) = tk_reminder[ilink](31, 0);
          tkword(95, 32) = channelsTM.data[ilink];
          break;
      }
      auto out = tkDecoderEmulator.decodeTrack(tkword, raw.track[s].region, true);
      if (!out.second)
        out.first.clear();
      channelsDecode.data[iout] = out.first.pack();
      channelsDecode.orbit[iout] = channelsTM.orbit[ilink];
      channelsDecode.start[iout] = channelsTM.start[ilink];
      channelsDecode.end[iout] = channelsTM.end[ilink];
      channelsDecode.valid[iout] = channelsTM.valid[ilink];
      assert(ilink < calo_offs && iout < calo_offs);
      if (debug && tkword != 0) {
        std::cout << "At iclock " << iclock << ", sector " << s << "(link " << ilink << "), track word "
                  << tkword.to_string(16) << " -> iout " << iout << ", tkobj " << out.first.pack().to_string(16)
                  << " (region " << raw.track[s].region.pack().to_string(16) << ")"
                  << ", ok? " << out.second << ", valid ? " << channelsDecode.valid[iout] << std::endl;
      }
    }

    for (int s = 0; s < 2 * NCALOSECTORS; ++s) {
      assert(NRAWCALOFIBERS == 4);
      unsigned int ilink = (2 * (s % NCALOSECTORS) + (s / NCALOSECTORS)) * NRAWCALOFIBERS + calo_offs;
      unsigned int iout = 2 * (s % NCALOSECTORS) + (s / NCALOSECTORS) + calo_offs;
      ap_uint<256> hgcword = 0;
      ok = ok && calo_skipper[s](&channelsTM.data[ilink],
                                 &channelsTM.orbit[ilink],
                                 &channelsTM.start[ilink],
                                 &channelsTM.end[ilink],
                                 &channelsTM.valid[ilink],
                                 hgcword,
                                 channelsDecode.orbit[iout],
                                 channelsDecode.start[iout],
                                 channelsDecode.end[iout],
                                 channelsDecode.valid[iout]);
      channelsDecode.data[iout] = hgcalDecoderEmulator.decode(hgcword).pack();
      assert(ilink >= calo_offs && ilink < mu_offs_raw);
      assert(iout >= calo_offs && iout < mu_offs_decoded);
      if (debug && (hgcword != 0 || iclock == 0)) {
        std::cout << "At iclock " << iclock << ", calo sector " << s << "(link " << ilink << "), cluster word "
                  << hgcword.to_string(16) << " -> iout " << iout << ", calo_obj "
                  << channelsDecode.data[iout].to_string(16) << ", ok? " << channelsDecode.valid[iout] << std::endl;
      }
    }
    // decode muons (only 1)
    assert(NMUFIBERS == 1);
    channelsDecode.data[mu_offs_decoded] = muDecoderEmulator.decode(channelsTM.data[mu_offs_raw]).pack();
    channelsDecode.orbit[mu_offs_decoded] = channelsTM.orbit[mu_offs_raw];
    channelsDecode.start[mu_offs_decoded] = channelsTM.start[mu_offs_raw];
    channelsDecode.end[mu_offs_decoded] = channelsTM.end[mu_offs_raw];
    channelsDecode.valid[mu_offs_decoded] = channelsTM.valid[mu_offs_raw];
    if (debug && channelsTM.data[mu_offs_raw] != 0)
      std::cout << "At iclock " << iclock << ", muon word " << channelsTM.data[mu_offs_raw].to_string(16) << ", muobj "
                << channelsDecode.data[mu_offs_decoded].to_string(16) << std::endl;
    // the vertex is trivial
    ap_uint<l1ct::PVObj::BITWIDTH> pv_decoded;
    packed_l1vertex_input_convert(channelsTM.data[vtx_offs_raw],
                                  channelsTM.valid[vtx_offs_raw],
                                  pv_decoded,
                                  channelsDecode.valid[vtx_offs_decoded]);
    channelsDecode.data[vtx_offs_decoded] = pv_decoded;
    channelsDecode.orbit[vtx_offs_decoded] = channelsTM.orbit[vtx_offs_raw];
    channelsDecode.start[vtx_offs_decoded] = channelsTM.start[vtx_offs_raw];
    channelsDecode.end[vtx_offs_decoded] = channelsTM.end[vtx_offs_raw];
    channelsDecode.dump();

    // VALIDATION
    if (!tailOfTrain) {
      bool validation_ok = true;
      for (unsigned int i = 0; i < vtx_offs_decoded; ++i) {  // VTX can't be validate this way
        if (channelsDecode.data[i] != decoded_validation_data[iclock][i] ||
            channelsDecode.valid[i] != decoded_validation_valid[iclock][i]) {
          if (validation_ok == true) {  // print this only once
            std::cout << "error in decoded validation at itest " << itest << " (within train " << indexWithinTrain
                      << ", tail? " << tailOfTrain << ")"
                      << ", iclock " << iclock << " (tail? " << tailOfTrain << ")" << std::endl;
            validation_ok = false;
          }
          std::cout << "channel " << i << " : "
                    << "expected " << decoded_validation_data[iclock][i].to_string(16) << " (valid? "
                    << decoded_validation_valid[iclock][i] << "), "
                    << "found " << channelsDecode.data[i].to_string(16) << " (valid? " << channelsDecode.valid[i] << ")"
                    << std::endl;
          ok = false;
        }
      }
      if (!ok)
        break;
    }
  }
  return ok;
}

void Tester::runRegionizer(const l1ct::RegionizerDecodedInputs &in,
                           const l1ct::RegionizerDecodedInputs &infwd,
                           const std::vector<l1ct::PFInputRegion> &allpfin,
                           const std::vector<l1ct::PFInputRegion> &allpfinfwd,
                           unsigned int nclocks,
                           unsigned int indexWithinTrain,
                           bool tailOfTrain) {
  int ilink;
  if (!tailOfTrain) {
    regEmulator.fillEvent(in);
    regEmulatorFwd.fillEvent(infwd);
  }

  // emulate regionizer
  std::vector<l1ct::TkObjEmu> tk_links_in(2 * NTKSECTORS), tk_out, fwdtk_out;
  std::vector<l1ct::EmCaloObjEmu> em_links_in, em_out, fwdem_out;
  std::vector<l1ct::HadCaloObjEmu> calo_links_in(2 * NCALOSECTORS), calo_out, fwdcalo_out;
  std::vector<l1ct::MuObjEmu> mu_links_in(1), mu_out, fwdmu_out;
  std::vector<bool> tk_links_valid(2 * NTKSECTORS), calo_links_valid(2 * NCALOSECTORS), mu_links_valid(1);
  for (int iclock = 0; iclock < nclocks; ++iclock) {
    if (!tailOfTrain) {
      // we can't just do
      //     regEmulator.fillLinks(iclock, tk_links_in);
      // since the links are 64 bit and the tracks 96 bits
      int itkclock = 2 * (iclock / 3) + (iclock % 3) - 1;
      if (iclock % 3 == 0) {
        for (int isec = 0; isec < 2 * NTKSECTORS; ++isec) {
          ilink = 2 * (isec % NTKSECTORS) + (isec / NTKSECTORS);
          unsigned int ntracks = in.track[isec].size();
          unsigned int nw64 = (ntracks * 3 + 1) / 2;
          tk_links_in[ilink].clear();
          tk_links_valid[ilink] = (iclock == 0) || (iclock < nw64);
        }
      } else {
        regEmulator.fillLinks(itkclock, tk_links_in, tk_links_valid);
      }
      // for HGCal, we have first 31 clock cycles of null words while header &
      // towers are transmitted
      if (iclock >= 31) {
        int ihgclock = iclock - 31;
        regEmulator.fillLinks(ihgclock, calo_links_in, calo_links_valid);
      } else {
        calo_links_in.resize(NCALOSECTORS * NCALOFIBERS);     // null data
        calo_links_valid.resize(NCALOSECTORS * NCALOFIBERS);  // null data
        for (auto &c : calo_links_in)
          c.clear();
        std::fill(calo_links_valid.begin(), calo_links_valid.end(), iclock != 0);
      }
      regEmulator.fillLinks(iclock, mu_links_in, mu_links_valid);
      cnt_.tk += count_nz(tk_links_in);
      cnt_.had += count_nz(calo_links_in);
      cnt_.mu += count_nz(mu_links_in);

      ilink = 0;
      bool lastframe = (iclock == REGIONIZERNCLOCKS - 1);
      channelsIn.init(indexWithinTrain == 1, iclock == 0, false, !lastframe);
      for (int isec = 0, itk = 0; isec < 2 * NTKSECTORS; ++isec, ++itk, ++ilink) {
        channelsIn.valid[ilink] = tk_links_valid[itk];
        channelsIn.data[ilink] = tk_links_in[itk].pack();
      }
      for (int isec = 0, icalo = 0; isec < 2 * NCALOSECTORS; ++isec, ++icalo, ++ilink) {
        channelsIn.valid[ilink] = calo_links_valid[icalo];
        channelsIn.data[ilink] = calo_links_in[icalo].pack();
      }
      for (int imu = 0; imu < NMUFIBERS; ++imu, ++ilink) {
        channelsIn.valid[ilink] = iclock < NMUON_PROMPT;  //mu_links_valid[imu];
        channelsIn.data[ilink] = mu_links_in[imu].pack();
      }
      channelsIn.valid[ilink] = iclock == 0 || iclock < inputs.event().pvs.size();
      channelsIn.data[ilink++] = inputs.event().pv(iclock).pack();
      channelsIn.dump();

      // put good decoded data for validation in the ring buffer
      for (unsigned int i = 0; i < nchann_decoded; ++i) {
        decoded_validation_data[iclock][i] = channelsIn.data[i];
        decoded_validation_valid[iclock][i] = channelsIn.valid[i];
      }
    } else {
      tk_links_in.resize(NTKSECTORS * NTKFIBERS);
      calo_links_in.resize(NCALOSECTORS * NCALOFIBERS);
      mu_links_in.resize(NMUFIBERS);
      for (auto &t : tk_links_in)
        t.clear();
      for (auto &c : calo_links_in)
        c.clear();
      for (auto &m : mu_links_in)
        m.clear();
    }

    bool newevt_ref = (iclock == 0);
    regEmulator.step(
        newevt_ref, tk_links_in, calo_links_in, em_links_in, mu_links_in, tk_out, calo_out, em_out, mu_out, ROUTER_ISMUX);
    regEmulatorFwd.step(newevt_ref,
                        tk_links_in,
                        calo_links_in,
                        em_links_in,
                        mu_links_in,
                        fwdtk_out,
                        fwdcalo_out,
                        fwdem_out,
                        fwdmu_out,
                        ROUTER_ISMUX);
    cnt_.em += count_nz(em_out);

    unsigned int ireg = iclock / (REGOUTII + REGPAUSEII);
    bool is_pause = (iclock % (REGOUTII + REGPAUSEII) >= REGOUTII);
    bool region_valid = ireg < allpfin.size();
    ilink = 0;
    bool orbit = (indexWithinTrain == 1) && iclock == 0;
    bool start = (iclock % (REGOUTII + REGPAUSEII)) == 0;
    bool end = (iclock  % (REGOUTII + REGPAUSEII)) == REGOUTII-1;
    channelsReg.init(orbit, start, end, region_valid && !is_pause);
    for (int i = 0; i < NTKOUT; ++i)
      channelsReg.data[ilink++] = tk_out[i].pack();
    for (int i = 0; i < NCALOOUT; ++i)
      channelsReg.data[ilink++] = calo_out[i].pack();
    for (int i = 0; i < NEMOUT; ++i)
      channelsReg.data[ilink++] = em_out[i].pack();
    for (int i = 0; i < NMUOUT; ++i)
      channelsReg.data[ilink++] = mu_out[i].pack();
    channelsReg.data[ilink++] = region_valid ? allpfin[ireg].region.pack() : ap_uint<l1ct::PFRegion::BITWIDTH>(0);
    ilink = 0;
    channelsRegFwd.init(orbit, start, end, region_valid && !is_pause);
    for (int i = 0; i < NCALOFWDOUT; ++i)
      channelsRegFwd.data[ilink++] = fwdcalo_out[i].pack_slim();
    channelsRegFwd.data[ilink++] = region_valid ? allpfinfwd[ireg].region.pack() : ap_uint<l1ct::PFRegion::BITWIDTH>(0);

    if (indexWithinTrain > 0) {
      channelsReg.dump();
      channelsRegFwd.dump();
      channelsRegSplit.copy(channelsReg).dump();
      channelsRegFwdSplit.copy(channelsRegFwd).dump();
    }
  }
}

bool Tester::runPFPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> &allpfin, bool debug) {
  l1ct::OutputRegion pfout;
  int ilink;
  cnt_.ev++;
  bool ok = true;

  for (int ireg = 0, nreg = allpfin.size(); ireg < nreg; ++ireg) {
    if (debug)
      printf("Will run PF event %d, region %d\n", itest, ireg);
    bool orb_tag = newOrbit && ireg == 0;
    bool start_tag = (ireg == 0);
    bool end_tag = (ireg == nreg - 1);
    pfEmulator.setDebug(debug);
    pfEmulator.run(allpfin[ireg], pfout);
    pfEmulator.mergeNeutrals(pfout);
    cnt_.pf += count_nz(pfout.pfcharged);
    cnt_.pf += count_nz(pfout.pfneutral);

#ifndef NO_CHECK_CMSSW_REF
    const auto &cmsswPFC = inputs.event().out[ireg].pfcharged;
    const auto &cmsswPFN = inputs.event().out[ireg].pfneutral;
    bool match = count_nz(pfout.pfcharged) == count_nz(cmsswPFC) && count_nz(pfout.pfneutral) == count_nz(cmsswPFN);
    for (int i = 0, n = std::min<int>(pfout.pfcharged.size(), cmsswPFC.size()); i < n; ++i) {
      if (!pf_equals(cmsswPFC[i], pfout.pfcharged[i], "PFcharged", i))
        match = false;
    }
    for (int i = 0, n = std::min<int>(pfout.pfneutral.size(), cmsswPFC.size()); i < n; ++i) {
      if (!pf_equals(cmsswPFN[i], pfout.pfneutral[i], "PFneutral", i))
        match = false;
    }
    if (debug || !match) {
      printf("Done comparison between this PF (%u/%u) and the CMSSW one (%u/%u). %s\n",
             count_nz(pfout.pfcharged),
             count_nz(pfout.pfneutral),
             count_nz(cmsswPFC),
             count_nz(cmsswPFN),
             match ? "ok" : "FAILED");
      //if (!match) ok = false; // FIXME
    }
#endif

    channelsPf.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg-1, true);
    for (int i = 0, n = pfout.pfcharged.size(), ilink = 0; i < n; ++i, ++ilink)
      channelsPf.data[ilink] = pfout.pfcharged[i].pack();
    for (int i = 0, n = pfout.pfneutral.size(), ilink = NTRACK; i < n; ++i, ++ilink)
      channelsPf.data[ilink] = pfout.pfneutral[i].pack();
    for (int i = 0, n = pfout.pfmuon.size(), ilink = NTRACK + NCALO; i < n; ++i, ++ilink)
      channelsPf.data[ilink] = pfout.pfmuon[i].pack();

    // Puppi objects
    if (debug)
      printf("Will run Puppi with z0 = %d in event %d, region %d\n", inputs.event().pv().hwZ0.to_int(), itest, ireg);
    puEmulator.setDebug(debug);

    std::vector<l1ct::PVObjEmu> pvs(NPV, inputs.event().pv());

    std::vector<l1ct::PuppiObjEmu> outallch, outallne_nocut, outallne, outselne;
    puEmulator.linpuppi_chs_ref(allpfin[ireg].region, pvs, pfout.pfcharged, outallch);
    puEmulator.linpuppi_ref(
        allpfin[ireg].region, allpfin[ireg].track, pvs, pfout.pfneutral, outallne_nocut, outallne, outselne);

    outallch.resize(NTRACK);
    outallne.resize(NCALO);
    outallch.insert(outallch.end(), outallne.begin(), outallne.end());
    ilink = 0;
    channelsPuppi.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg-1, true);
    for (auto &pup : outallch)
      channelsPuppi.data[ilink++] = pup.pack();
    cnt_.puppi += count_nz(outallch);

    pfout.puppi.resize(NPUPPIFINALSORTED);
    folded_hybrid_bitonic_sort_and_crop_ref(NTRACK + NCALO, NPUPPIFINALSORTED, &outallch[0], &pfout.puppi[0]);
    cnt_.puppisort += count_nz(pfout.puppi);

    ilink = 0;
    channelsPuppiSort.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg-1, true);
    for (auto &pup : pfout.puppi)
      channelsPuppiSort.data[ilink++] = pup.pack();

#ifndef NO_CHECK_CMSSW_REF
    const auto &cmsswPuppi = inputs.event().out[ireg].puppi;
    match = count_nz(pfout.puppi) == count_nz(cmsswPuppi);
    for (int i = 0, n = std::min<int>(pfout.puppi.size(), cmsswPuppi.size()); i < n; ++i) {
      if (!puppi_equals(cmsswPuppi[i], pfout.puppi[i], "Puppi", i))
        match = false;
    }
    if (debug || !match) {
      printf("Done comparison between this puppi (%u) and the CMSSW one (%u). %s\n",
             count_nz(pfout.puppi),
             count_nz(cmsswPuppi),
             match ? "ok" : "FAILED");
      // if (!match) ok = false; // FIXME
    }
#endif

    const unsigned int OUTII360 = REGOUTII + REGPAUSEII;
    channelsPf.dumpNCopies(OUTII360);
    channelsPuppi.dumpNCopies(OUTII360);
    channelsPuppiSort.dumpNCopies(OUTII360);
    channelsPuppiSortStream.copy(channelsPuppiSort).dump();
    channelsPuppiSortStreamSplit.copy(channelsPuppiSort).dump();

    if (!ok)
      break;
  }
  return ok;
}

bool Tester::runPuppiFwd(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> &allpfinfwd, bool debug) {
  int ilink;
  bool ok = true;
  std::vector<l1ct::PuppiObjEmu> outallne_nocut, outallne, outselne, outsort;

  for (int ireg = 0, nreg = allpfinfwd.size(); ireg < nreg; ++ireg) {
    if (debug)
      printf("Will run forward puppi for event %d, region %d\n", itest, ireg);
    puEmulatorFwd.setDebug(debug);
    puEmulatorFwd.fwdlinpuppi_ref(
        allpfinfwd[ireg].region, allpfinfwd[ireg].hadcalo, outallne_nocut, outallne, outselne);
    cnt_.puppifwd += count_nz(outallne);
    ilink = 0;
    channelsPuppiFwd.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg - 1, true);
    for (auto &pup : outallne)
      channelsPuppiFwd.data[ilink++] = pup.pack();
    outallne.resize(NCALOFWD);

    outsort.resize(NPUPPIFINALSORTED);
    for (auto &p : outsort)
      p.clear();
    folded_hybrid_bitonic_sort_and_crop_ref(
        NCALOFWD, std::min<unsigned int>(NCALOFWD, NPUPPIFINALSORTED), &outallne[0], &outsort[0]);
    ilink = 0;
    channelsPuppiFwdSort.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg - 1, true);
    for (auto &pup : outsort)
      channelsPuppiFwdSort.data[ilink++] = pup.pack();
    cnt_.puppisortfwd += count_nz(outsort);

#ifndef NO_CHECK_CMSSW_REF
    const auto &cmsswPuppi = fwdinputs.event().out[ireg].puppi;
    bool match = count_nz(outsort) == count_nz(cmsswPuppi);
    for (int i = 0, n = std::min<int>(outallne.size(), cmsswPuppi.size()); i < n; ++i) {
      if (!puppi_equals(cmsswPuppi[i], outsort[i], "PuppiFwd", i))
        match = false;
    }
    if (debug || !match) {
      printf("Done comparison between this puppi (%u) and the CMSSW one (%u). %s\n",
             count_nz(outsort),
             count_nz(cmsswPuppi),
             match ? "ok" : "FAILED");
      // if (!match) ok = false; // FIXME
    }
#endif

    const unsigned int OUTII360 = REGOUTII + REGPAUSEII;
    channelsPuppiFwd.dumpNCopies(OUTII360);
    channelsPuppiFwdSort.dumpNCopies(OUTII360);
    channelsPuppiFwdSortStream.copy(channelsPuppiFwdSort).dump();
    channelsPuppiFwdSortStreamSplit.copy(channelsPuppiFwdSort).dump();

    if (!ok)
      break;
  }
  return ok;
}

bool Tester::runTkEg(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> &allpfin, bool debug) {
  std::vector<l1ct::OutputRegion> egouts(allpfin.size());
  std::vector<unsigned int> indices[2];
  int ilink;
  bool ok = true;
  for (int ireg = 0, nreg = allpfin.size(); ireg < nreg; ++ireg) {
    unsigned int iendcap = (allpfin[ireg].region.floatEtaCenter() > 0);
    indices[iendcap].push_back(ireg);
    l1ct::OutputRegion &egout = egouts[ireg];
    if (debug)
      printf("Will run TkEG event %d, region %d\n", itest, ireg);
    if (debug)
      egEmulator.setDebug(3);
    else
      egEmulator.setDebug(0);
    egEmulator.run(allpfin[ireg], egout);

    channelsEg.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg-1, true);
    for (int i = 0, n = egout.egphoton.size(), ilink = 0; i < n; ++i, ++ilink) {
      channelsEg.data[ilink] = egout.egphoton[i].pack();
    }
    for (int i = 0, n = egout.egelectron.size(), ilink = NEM_EGOUT; i < n; ++i, ++ilink) {
      channelsEg.data[ilink] = egout.egelectron[i].pack();
    }
    if (debug) {
      for (const auto &o : egout.egphoton) {
        if (o.hwPt)
          printf("Event %2d region %d:     photon   with pT %6.2f, iso %7d  packed %s\n",
                 itest,
                 ireg,
                 o.floatPt(),
                 o.intIso(),
                 o.pack().to_string(16).c_str());
      }
      for (const auto &o : egout.egelectron) {
        if (o.hwPt)
          printf("Event %2d region %d:     electron with pT %6.2f, iso %7d  packed %s\n",
                 itest,
                 ireg,
                 o.floatPt(),
                 o.intIso(),
                 o.pack().to_string(16).c_str());
      }
    }
    egEmulator.runIso(allpfin[ireg], inputs.event().pvs, egout);
    cnt_.eg += count_nz(egout.egphoton);
    cnt_.ele += count_nz(egout.egelectron);

    int nphotons = 0, neles = 0;
    channelsEgTkIso.init(newOrbit && ireg == 0, ireg == 0, ireg == nreg-1, true);
    for (int i = 0, n = egout.egphoton.size(), ilink = 0; i < n; ++i, ++ilink) {
      if (egout.egphoton[i].hwPt > 0)
        nphotons++;
      channelsEgTkIso.data[ilink] = egout.egphoton[i].pack();
    }
    for (int i = 0, n = egout.egelectron.size(), ilink = NEM_EGOUT; i < n; ++i, ++ilink) {
      channelsEgTkIso.data[ilink] = egout.egelectron[i].pack();
    }
    if (debug) {
      std::cout << "# photons: " << nphotons << " # eles: " << neles << std::endl;
      for (const auto &o : egout.egphoton) {
        if (o.hwPt)
          printf("Event %2d region %d: iso photon   with pT %6.2f, iso %7d  packed %s\n",
                 itest,
                 ireg,
                 o.floatPt(),
                 o.intIso(),
                 o.pack().to_string(16).c_str());
      }
      for (const auto &o : egout.egelectron) {
        if (o.hwPt)
          printf("Event %2d region %d: iso electron with pT %6.2f, iso %7d  packed %s\n",
                 itest,
                 ireg,
                 o.floatPt(),
                 o.intIso(),
                 o.pack().to_string(16).c_str());
      }
    }

#ifndef NO_CHECK_CMSSW_REF
    const std::vector<l1ct::EGIsoObjEmu> &cmsswPho = inputs.event().out[ireg].egphoton;
    const std::vector<l1ct::EGIsoEleObjEmu> &cmsswEle = inputs.event().out[ireg].egelectron;
    bool match = count_nz(egout.egphoton) == count_nz(cmsswPho) && count_nz(egout.egelectron) == count_nz(cmsswEle);
    if (!match) {
      HumanReadablePatternSerializer debugHR(
          "-", /*zerosuppress=*/true);  // this will print on stdout, we'll use it for errors
      debugHR.dump_tkeg("Standalone Photon", egout.egphoton);
      debugHR.dump_tkeg("CMSSW      Photon", cmsswPho);
      debugHR.dump_tkele("Standalone Electron", egout.egelectron);
      debugHR.dump_tkele("CMSSW      Electron", cmsswEle);
    }
    for (int i = 0, n = std::min<int>(egout.egphoton.size(), cmsswPho.size()); i < n; ++i) {
      if (!egiso_equals(cmsswPho[i], egout.egphoton[i], "EG Photon", i))
        match = false;
    }
    for (int i = 0, n = std::min<int>(egout.egelectron.size(), cmsswEle.size()); i < n; ++i) {
      if (!egisoele_equals(cmsswEle[i], egout.egelectron[i], "EG Electron", i))
        match = false;
    }
    if (debug || !match) {
      printf("Done comparison between this EG Photons & Electrons (%u, %u) and the CMSSW ones (%u, %u). %s\n",
             count_nz(egout.egphoton),
             count_nz(egout.egelectron),
             count_nz(cmsswPho),
             count_nz(cmsswEle),
             match ? "ok" : "FAILED");
      //if (!match) ok = false;
    }
#endif

    const unsigned int OUTII360 = REGOUTII + REGPAUSEII;
    channelsEg.dumpNCopies(OUTII360);
    channelsEgTkIso.dumpNCopies(OUTII360);

    // now make more 64-bit friendly versions
    ap_uint<64> egiso64[nchann_eg64];
    assert(l1ct::EGIsoObj::BITWIDTH <= 64);  // photons are <= 64 bits
    assert(64 < l1ct::EGIsoEleObj::BITWIDTH &&
           l1ct::EGIsoEleObj::BITWIDTH <= 128);  // electrons are > 64 bits, but within 128
    for (int i = 0; i < NEM_EGOUT; ++i) {
      egiso64[i] = channelsEgTkIso.data[i];
    }
    for (int ii = NEM_EGOUT, io = NEM_EGOUT; ii < 2 * NEM_EGOUT; ++ii, io += 2) {
      egiso64[io + 0] = channelsEgTkIso.data[ii](63, 0);
      egiso64[io + 1] = channelsEgTkIso.data[ii](l1ct::EGIsoEleObj::BITWIDTH - 1, 64);
    }

    for (int j = 0; j < OUTII360; ++j) {
      bool orbit = newOrbit && ireg == 0 && j == 0;
      bool start = ireg == 0 && j == 0;
      bool end = ireg == nreg - 1 && j == OUTII360-1;
      channelsEgTkIsoStream.init(orbit, start, end, j < NEM_EGOUT);
      if (j < NEM_EGOUT) {
        channelsEgTkIsoStream.data[0] = egiso64[j];
        channelsEgTkIsoStream.data[1] = egiso64[2 * j + 0 + NEM_EGOUT];
        channelsEgTkIsoStream.data[2] = egiso64[2 * j + 1 + NEM_EGOUT];
      }
      channelsEgTkIsoStream.dump();
    }

    if (!ok)
      break;
  }

  // final sorter
  egSorterEmulator.setDebug(debug);
  for (int iendcap = 0; iendcap <= 1; ++iendcap) {
    std::vector<l1ct::EGIsoObjEmu> perBoardPho;
    std::vector<l1ct::EGIsoEleObjEmu> perBoardEle;
    egSorterEmulator.run<l1ct::EGIsoObjEmu>(allpfin, egouts, indices[iendcap], perBoardPho);
    egSorterEmulator.run<l1ct::EGIsoEleObjEmu>(allpfin, egouts, indices[iendcap], perBoardEle);

#ifndef NO_CHECK_CMSSW_REF
    const std::vector<l1ct::EGIsoObjEmu> &cmsswPho = inputs.event().board_out[iendcap].egphoton;
    const std::vector<l1ct::EGIsoEleObjEmu> &cmsswEle = inputs.event().board_out[iendcap].egelectron;
    bool match = count_nz(perBoardPho) == count_nz(cmsswPho) && count_nz(perBoardEle) == count_nz(cmsswEle);
    if (!match) {
      HumanReadablePatternSerializer debugHR(
          "-", /*zerosuppress=*/true);  // this will print on stdout, we'll use it for errors
      debugHR.dump_tkeg("Sorted Standalone Photon", perBoardPho);
      debugHR.dump_tkeg("Sorted CMSSW      Photon", cmsswPho);
      debugHR.dump_tkele("Sorted Standalone Electron", perBoardEle);
      debugHR.dump_tkele("Sorted CMSSW      Electron", cmsswEle);
    }
    for (int i = 0, n = std::min<int>(perBoardPho.size(), cmsswPho.size()); i < n; ++i) {
      if (!egiso_equals(cmsswPho[i], perBoardPho[i], "EG Photon", i))
        match = false;
    }
    for (int i = 0, n = std::min<int>(perBoardEle.size(), cmsswEle.size()); i < n; ++i) {
      if (!egisoele_equals(cmsswEle[i], perBoardEle[i], "EG Electron", i))
        match = false;
    }
    if (debug || !match) {
      printf("Done comparison between this sorted EG Photons & Electrons (%u, %u) and the CMSSW ones (%u, %u). %s\n",
             count_nz(perBoardPho),
             count_nz(perBoardEle),
             count_nz(cmsswPho),
             count_nz(cmsswEle),
             match ? "ok" : "FAILED");
      //if (!match) ok = false;
    }
#endif

    unsigned int i64 = 0;
    for (int i = 0; i < NL1_EGOUT; ++i, ++i64) {
      channelsEgSorted.init(newOrbit && i == 0 && iendcap == 0, i == 0 && iendcap == 0, false, true);
      channelsEgSorted.data[0] =
          i < perBoardPho.size() ? perBoardPho[i].pack() : ap_uint<l1ct::EGIsoObjEmu::BITWIDTH>(0);
      channelsEgSorted.dump();
      channelsEgSortedStream.copy(channelsEgSorted).dump();
    }
    for (int i = 0; i < NL1_EGOUT; ++i) {
      ap_uint<128> ele128 = i < perBoardEle.size() ? perBoardEle[i].pack() : ap_uint<l1ct::EGIsoEleObjEmu::BITWIDTH>(0);
      for (int j = 0; j < 128; j += 64, ++i64) {
        channelsEgSorted.init(false, false, i == NL1_EGOUT - 1 && j != 0 && iendcap == 1, true);
        channelsEgSorted.data[0] = ele128(j + 63, j);
        channelsEgSorted.dump();
        channelsEgSortedStream.copy(channelsEgSorted).dump();
      }
    }
    for (; i64 < TLEN / 2; ++i64) {
      channelsEgSorted.init(false, false, false, iendcap == 0);
      channelsEgSorted.dump();
      channelsEgSortedStream.copy(channelsEgSorted).dump();
    }
  }
  return ok;
}

bool Tester::run() {
  unsigned int events_per_chunk = 6;
  unsigned int frame = 0, ilink;
  bool ok = true, firstOfTrain = true;
  l1ct::PVObjEmu pv_prev;  // we have 1 event of delay in the reference regionizer, so we
                           // need to use the PV from 54 clocks before

  if (NTEST == 48) {
    // skip the first 27 events, to start with an event that has an electron in the negative endcap (easier debugging)
    for (int i = 0; i < 27; ++i) {
      inputs.nextEvent();
      fwdinputs.nextEvent();
    }
  }
  for (int itest = 0, trainStart = 0; itest < NTEST; ++itest) {
    if (!inputs.nextEvent() || !fwdinputs.nextEvent())
      break;
    assert(inputs.event().run == fwdinputs.event().run && inputs.event().lumi == fwdinputs.event().lumi &&
           inputs.event().event == fwdinputs.event().event);
    printf(
        "Processing event %d run:lumi:event %u:%u:%lu (train Start %d, "
        "index in train %d)\n",
        itest,
        inputs.event().run,
        inputs.event().lumi,
        inputs.event().event,
        trainStart,
        itest - trainStart);

    bool debug = itest <= 10;

    l1ct::RawInputs raw;
    l1ct::RegionizerDecodedInputs in, infwd;
    std::vector<l1ct::PFInputRegion> allpfin, allpfinfwd;
    readEndcaps(itest, raw, in, infwd, allpfin, allpfinfwd, debug);
    if (itest == 0) {
      printf("Tk   eta: center %d --> center %d, halfsize %d , extra %d \n",
             in.track.front().region.intEtaCenter(),
             allpfin.front().region.intEtaCenter(),
             allpfin.front().region.hwEtaHalfWidth.to_int(),
             allpfin.front().region.hwEtaExtra.to_int());
      printf("Calo eta: center %d --> center %d, halfsize %d , extra %d \n",
             in.hadcalo.front().region.intEtaCenter(),
             allpfin.front().region.intEtaCenter(),
             allpfin.front().region.hwEtaHalfWidth.to_int(),
             allpfin.front().region.hwEtaExtra.to_int());
      printf("Mu   eta: center %d --> center %d, halfsize %d , extra %d \n",
             in.muon.region.intEtaCenter(),
             allpfin.front().region.intEtaCenter(),
             allpfin.front().region.hwEtaHalfWidth.to_int(),
             allpfin.front().region.hwEtaExtra.to_int());
    }

    runRegionizer(in, infwd, allpfin, allpfinfwd, TLEN, itest - trainStart, /*tail=*/false);
    ok = runPFPuppi(itest, itest == trainStart, allpfin, debug) && ok;
    ok = runPuppiFwd(itest, itest == trainStart, allpfinfwd, debug) && ok;
    ok = runTkEg(itest, itest == trainStart, allpfin, debug) && ok;

    ok = runTMuxAndDemux(itest, itest - trainStart, TLEN, /*tail=*/false, raw, debug) && ok;

    if (!ok)
      break;
    if (itest - trainStart == (events_per_chunk - 1)) {  // put a pause after 2*N events
      unsigned int pause_length = 360;
      assert(pause_length > GTTLATENCY);
      // flush data: regionizer
      runRegionizer(in, infwd, allpfin, allpfinfwd, TLEN, itest - trainStart, /*tail=*/true);
      runTMuxAndDemux(itest + 1, itest + 1 - trainStart, pause_length, /*tail=*/true, raw, debug);
      // add pause in the expected outputs
      channelsIn.dumpNulls(pause_length);
      channelsReg.dumpNulls(pause_length);
      channelsRegFwd.dumpNulls(pause_length);
      channelsPf.dumpNulls(pause_length);
      channelsEg.dumpNulls(pause_length);
      channelsEgTkIso.dumpNulls(pause_length);
      channelsEgSorted.dumpNulls(pause_length);
      channelsPuppi.dumpNulls(pause_length);
      channelsPuppiSort.dumpNulls(pause_length);
      channelsPuppiSortStream.dumpNulls(pause_length/(REGOUTII+REGPAUSEII));
      channelsPuppiFwd.dumpNulls(pause_length);
      channelsPuppiFwdSort.dumpNulls(pause_length);
      channelsPuppiFwdSortStream.dumpNulls(pause_length/(REGOUTII+REGPAUSEII));
      // re-init stuff
      for (unsigned int isec = 0; isec < 2 * NCALOSECTORS; ++isec) {
        calo_skipper[isec].reset();
      }
      frame = 0;
      trainStart = itest + 1;
    }
    if (((itest % events_per_chunk) == (events_per_chunk - 1)) && (itest != NTEST - 1)) {
      channelsVU9P.newFile();
      channelsVU13P.newFile();
      channelsRegSplit.newFile();
      channelsRegFwdSplit.newFile();
      channelsPuppiSortStreamSplit.newFile();
      channelsPuppiFwdSortStreamSplit.newFile();
      channelsEgTkIsoStream.newFile();
      channelsEgSortedStream.newFile();
    }
    if (!ok)
      break;
  }

  return ok;
}

int main(int argc, char **argv) {
  Tester test(argv[1]);
  return test.run() ? 0 : 1;
}

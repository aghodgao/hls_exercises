if { [ info exists env(pfBoard) ] } { set pfBoard $env(pfBoard) } { set pfBoard "VCU118" }
if { [ info exists env(pfReg) ] } { set pfReg $env(pfReg) } { set pfReg "Barrel" }
if { [ info exists env(nTest) ] } { set nTest $env(nTest) } { set nTest "48" }
if { [ info exists env(nBarrelReg) ] } { set nBarrelReg $env(nBarrelReg); } { set nBarrelReg "18";  }
if { [ info exists env(pfFreq) ] } { set pfFreq $env(pfFreq); } { if { $nBarrelReg == 9 } { set pfFreq "180"; } { set pfFreq "240"; } }

set sample TTbar_PU200
set postFreq "_${pfFreq}"
set pfReg "Barrel"
set regionizerCFlags ""
set outLabel "phi${nBarrelReg}"
if { $nBarrelReg == 9 } {
    set regionizerCFlags "${regionizerCFlags} -DBARREL9R";
    if { $pfFreq == 240 } {
        set regOutII 4;
    } else {
        set regOutII 6;
    }
    set regPauseII 0;
    set dataFileName "TTbar_PU200_${pfReg}9.dump"
} else {
    set regOutII 2;
    set regPauseII 1;
    set dataFileName "TTbar_PU200_${pfReg}.dump"
}

set CMSSW_BASE $env(CMSSW_BASE)
set emulators "${CMSSW_BASE}/src/L1Trigger/Phase2L1ParticleFlow/src/"
set regionizerCFlags "${regionizerCFlags} -DPFFREQ=${pfFreq} -DREGOUTII=${regOutII} -DREGPAUSEII=${regPauseII}";

if { [ info exists env(gttLatency) ] } {
    set gttLatency $env(gttLatency);
    set regionizerCFlags "${regionizerCFlags} -DGTTLATENCY=${gttLatency}"
    set postFreq "${postFreq}_gttLatency${gttLatency}"
}


set cflags "-std=c++14 -I${CMSSW_BASE}/src -DREG_${pfReg} -DBOARD_${pfBoard} ${regionizerCFlags} -DNTEST=${nTest} "
set cflags "${cflags} -DCHECK_CMSSW_REF"
open_project -reset "project_csim_layer1_${pfReg}_${outLabel}_tmux6${postFreq}"

add_files -tb layer1_full_barrel_tmux6_test.cpp -cflags "${cflags}"
add_files -tb utils/tmux18_utils.cpp -cflags "${cflags}"
add_files -tb ${CMSSW_BASE}/src/DataFormats/L1TParticleFlow/src/layer1_emulator.cpp -cflags "${cflags}"
add_files -tb ${emulators}/l1-converters/tkinput_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/l1-converters/muonGmtToL1ct_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/l1-converters/hgcalinputt_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/pf/pfalgo_common_ref.cpp   -cflags "${cflags}"
add_files -tb ${emulators}/pf/pfalgo3_ref.cpp   -cflags "${cflags}"
add_files -tb ${emulators}/puppi/linpuppi_ref.cpp   -cflags "${cflags}"
add_files -tb ${emulators}/regionizer/multififo_regionizer_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/regionizer/regionizer_base_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/egamma/pfeginput_ref.cpp -cflags "${cflags}"
add_files -tb ${emulators}/egamma/pftkegalgo_ref.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-converters/pv/firmware/vertex.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-converters/muons/ref/muoninput_ref.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-regionizer/multififo/tdemux/firmware/tdemux.cpp   -cflags "${cflags}"
add_files -tb ../correlator-common/l1-regionizer/multififo/utils/dummy_obj_packers.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/l1-regionizer/multififo/firmware/dummy_obj_unpackers.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/utils/pattern_serializer.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/utils/test_utils.cpp -cflags "${cflags}"
add_files -tb ../correlator-common/dumpfiles/${dataFileName}

open_solution -reset "solution"
set_part {xcvu9p-flga2104-2L-e}
create_clock -period 2.5

csim_design -argv ${dataFileName}

exit

#ifndef REG_HGCal
#define REG_HGCal
#endif
#define ROUTER_STREAM
#define ROUTER_ISMUX 1
#define ROUTER_ISSTREAM 1
#ifndef PFFREQ
#define PFFREQ 180
#define REGOUTII 6
#endif
#ifndef HGCAL_SIDE
#define HGCAL_SIDE 1
#endif
#include "../correlator-common/l1-regionizer/multififo/firmware/regionizer.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/regionizer/multififo_regionizer_ref.h"
#include "../correlator-common/utils/pattern_serializer.h"
#include "../correlator-common/utils/test_utils.h"
#include "../correlator-common/utils/DumpFileReader.h"
#include "../correlator-common/l1-regionizer/multififo/utils/dummy_obj_packers.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/tdemux_ref.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/firmware/tdemux.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/pf/pfalgo2hgc_ref.h"
#include "../correlator-common/pf/firmware/pfalgo2hgc.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/puppi/linpuppi_ref.h"
#include "../correlator-common/puppi/firmware/linpuppi.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/common/bitonic_hybrid_sort_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/egamma/pfeginput_ref.h"
#include "../correlator-common/egamma/l1-input/firmware/pfeginput.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/egamma/pftkegalgo_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/egamma/pftkegsorter_ref.h"
#include "../correlator-common/l1-converters/pv/firmware/vertex.h"
#include "../correlator-common/l1-converters/tracks/firmware/tkinput_hgcal.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/tkinput_ref.h"
#include "../correlator-common/l1-converters/muons/firmware/muonGmtToL1ct.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/muonGmtToL1ct_ref.h"
#include "../correlator-common/l1-converters/muons/firmware/muoninput.h"
#include "../correlator-common/l1-converters/muons/ref/muoninput_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/hgcalinput_ref.h"
#include "utils/tmux18_utils.h"
#include "utils/channels.h"
#include "utils/channels.h"
#include "utils/hgcal_header_skipper.h"

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <vector>
#include <memory>

#define TLEN REGIONIZERNCLOCKS 
#define NRAWCALOFIBERS 4

#ifndef GTTLATENCY
#if PFFREQ == 240
#define GTTLATENCY 3*(TLEN-2)+10
#else
#define GTTLATENCY 3*(TLEN-2)+120
#endif
#endif

#ifndef NTEST
#define NTEST 48
#endif

#define PAUSES

#if defined(EGCOMPOSITEID)
    bool doComposite = true;
    // FIXME: check what is in the CMSSW config
    float trkQualityPtMin = 1.;
#else
    bool doComposite = false;
    float trkQualityPtMin = 10.;
#endif

struct Tester {

    Tester(const std::string & inputFile) :
        inputs(inputFile.c_str()),
        channelsTM("input-emp.txt"), 
        channelsTDemux("input-emp-tdemux.txt.gz"),
        channelsVU9P("input-emp-vu9p.%03d.txt.gz"),
        channelsVU13P("input-emp-vu13p.%03d.txt.gz"),
        channelsTDemuxSplit("input-emp-tdemux.%03d.txt.gz"),
        channelsDecode("input-emp-decoded.txt.gz"), 
        channelsIn("input-emp-decoded-ref.txt.gz"),
        channelsReg("output-emp-regionized-ref.txt.gz"),
        channelsRegSplit("output-emp-regionized-ref.%03d.txt.gz"),
        channelsPf("output-emp-pf-ref.txt.gz"),
        channelsPuppi("output-emp-puppi-ref.txt.gz"),
        channelsPuppiStream("output-emp-puppi-stream-ref.%03d.txt.gz",6),
        channelsPuppiSort("output-emp-puppisort-ref.txt.gz"),
        channelsPuppiSortStream("output-emp-puppisort-stream-ref.txt.gz",6),
        channelsPuppiSortStreamSplit("output-emp-puppisort-stream-ref.%03d.txt.gz",6),
        channelsEg("output-emp-eg-ref.txt.gz"),
        channelsEgTkIso("output-emp-egtkiso-ref.txt.gz"),        
        channelsEgStream("output-emp-eg-ref.%03d.txt.gz"),
        channelsEgTkIsoStream("output-emp-egtkiso-ref.%03d.txt.gz"),
        channelsEgSorted("output-emp-egsorted-ref.txt.gz"),
        channelsEgSortedStream("output-emp-egsorted-ref.%03d.txt.gz"),
        vu9p_links(nchann_vu9p, 0), // index is tmux link, value is VCU118 link
        vu13p_links(nchann_vu13p, 0), // index is tmux link, value is vu13p link
        decoded_validation_index(0),
        tk_tmuxer(NTKSECTORS), 
        calo_tmuxer(NCALOSECTORS*NRAWCALOFIBERS), 
        mu_tmuxer(1,NMUON_PROMPT),        
        mu_merger(NMUON_PROMPT),
        pv_gttlatency(GTTLATENCY), // real delay from GTT latency (for input files)
        pv_delayer(TLEN*2+1),      // fake delay to realign with decoded inputs for validation
        mu_delayer(TLEN*2+1),
        regEmulator (/*nendcaps=*/1, REGIONIZERNCLOCKS, 2, 3, NTRACK, NCALO, NEMCALO, NMU, ROUTER_ISSTREAM, REGOUTII, 0, /*atVertex=*/true),
        regEmulator2(/*nendcaps=*/1, REGIONIZERNCLOCKS, 2, 3, NTRACK, NCALO, NEMCALO, NMU, ROUTER_ISSTREAM, REGOUTII, 0, /*atVertex=*/true),
        pfEmulator(NTRACK, NCALO, NMU, NCALO,
                        PFALGO_DR2MAX_TK_MU, PFALGO_DR2MAX_TK_CALO,
                        PFALGO_TK_MAXINVPT_LOOSE, PFALGO_TK_MAXINVPT_TIGHT),
        puEmulator(NTRACK, NALLNEUTRALS, NALLNEUTRALS, NPV,
                          LINPUPPI_DR2MIN, LINPUPPI_DR2MAX, LINPUPPI_iptMax, LINPUPPI_dzCut,
                          l1ct::Scales::makeGlbEta(LINPUPPI_etaCut), 
                          LINPUPPI_ptSlopeNe, LINPUPPI_ptSlopeNe_1, LINPUPPI_ptSlopePh, LINPUPPI_ptSlopePh_1, 
                          LINPUPPI_ptZeroNe, LINPUPPI_ptZeroNe_1, LINPUPPI_ptZeroPh, LINPUPPI_ptZeroPh_1, 
                          LINPUPPI_alphaSlope, LINPUPPI_alphaSlope_1, LINPUPPI_alphaZero, LINPUPPI_alphaZero_1, LINPUPPI_alphaCrop, LINPUPPI_alphaCrop_1, 
                          LINPUPPI_priorNe, LINPUPPI_priorNe_1, LINPUPPI_priorPh, LINPUPPI_priorPh_1,
                          LINPUPPI_ptCut, LINPUPPI_ptCut_1),
        egEmulator(l1ct::PFTkEGAlgoEmuConfig(NTRACK, NTRACK_EGIN, NEMCALO_EGIN, NEM_EGOUT,
                        /*filterHwQuality=*/false,
                        /*doBremRecovery=*/true,
                        /*writeBeforeBremRecovery=*/false,
                        /*caloHwQual=*/1,
                        /*doEndcapQuality=*/true,
                        /* emClusterPtMin= */0.,
                        /*dEtaMaxBrem=*/0.02,
                        /*dPhiMaxBrem=*/0.1,
                        /*absEtaBoundaries=*/{0.0, 1.5},
                        /*dEtaValues=*/{0.015, 0.01},
                        /*dPhiValues=*/{0.07, 0.07},
                        /*trkQualityPtMin=*/trkQualityPtMin,
                        /*doComposite=*/doComposite,
                        /*NTRACK_PER_EMCALO_EGCOMP=*/NTRACK_PER_EMCALO_EGCOMP,
                         /* writeEgSta = */ false,
                        /*tkIsoParams_tkEle = */ {2., 0.6, 0.03, 0.2},
                        /*tkIsoParams_tkEm =  */ {2., 0.6, 0.07, 0.3},
                        /*pfIsoParams_tkEle = */ {1., 0.6, 0.03, 0.2},
                        /*pfIsoParams_tkEm =  */ {1., 0.6, 0.07, 0.3},
                        /* doTkIso = */ true,
                        /* doPfIso = */ false,
                        /* hwIsoTypeTkEle = */ l1ct::EGIsoEleObjEmu::IsoType::TkIso,
                        /* hwIsoTypeTkEm =  */ l1ct::EGIsoObjEmu::IsoType::TkIsoPV,
                        /* compIDparams = */ {-0.732422, 0.214844, "compositeID.json"},
                        /* debug = */ 0)),
        egSorterEmulator(NEM_EGOUT, NL1_EGOUT),
        tkDecoderEmulator(l1ct::TrackInputEmulator::Region::Endcap, l1ct::TrackInputEmulator::Encoding::Biased, /*bitwise=*/true, /*slim=*/false),
        muDecoderEmulator(GMT_Z0_SCALE, GMT_DXY_SCALE),
        hgcalDecoderEmulator(),
        regInit(false), 
        frame(0)
    { 
        initLinks();
        const float ptErr_edges[PTERR_BINS]  = PTERR_EDGES;
        const float ptErr_offss[PTERR_BINS]  = PTERR_OFFS;
        const float ptErr_scales[PTERR_BINS] = PTERR_SCALE;
        pfEmulator.loadPtErrBins(PTERR_BINS, ptErr_edges, ptErr_scales, ptErr_offss);   
        l1ct::EGInputSelectorEmuConfig emSelCfg(EGINPUT_BITMASK, NCALO, NEMCALO, false);
        regEmulator.setEgInterceptMode(/*afterFifo=*/true, emSelCfg);
        regEmulator2.setEgInterceptMode(/*afterFifo=*/true, emSelCfg);
        tkDecoderEmulator.configPhi(phi_BITS);
        tkDecoderEmulator.configZ0(z0_BITS);
        tkDecoderEmulator.configDEtaHGCal(calcDEtaHGCal_BITS,calcDEtaHGCal_Z0S,calcDEtaHGCal_RS,calcDEtaHGCal_LUTBITS,calcDEtaHGCal_LUTSHIFT);
        tkDecoderEmulator.configDPhiHGCal(calcDPhiHGCal_BITS,calcDPhiHGCal_Z0S1,calcDPhiHGCal_Z0S2,calcDPhiHGCal_RINVSHIFT,calcDPhiHGCal_TANLSHIFT,calcDPhiHGCal_LUTBITS, 0.0);
        tkDecoderEmulator.configEta(tanlLUT_BITS,0,15-tanlLUT_BITS,tanlLUT_POSTOFFS,true,true);
        tkDecoderEmulator.configPt(ptLUT_BITS);

        for (unsigned int isec = 0; isec < NCALOSECTORS; ++isec) {
            for (unsigned int itime = 0; itime < 3; ++itime) {
                calo_skipper[3*isec+itime] = HGCalHeaderSkipper(((HGCAL_SIDE+1)/2)*3+isec, itime*6, 30); // 30 clock cycles of towers
            }
        }
    }
        
    ~Tester() {
        printf("Processes %u events with %u tk, %u had, %u em, %u mu. Found %u pf, %u puppi, %u sorted puppi, %u e/g, %u ele\n",
                        cnt_.ev, cnt_.tk, cnt_.had, cnt_.em, cnt_.mu, cnt_.pf, cnt_.puppi, cnt_.puppisort, cnt_.eg, cnt_.ele);
    }

    static const unsigned int nchann_in = NTKSECTORS*3 + 3*NCALOSECTORS*NRAWCALOFIBERS + 3 + 1, nchann_vu9p = 29*4, nchann_vu13p = 32*4;
    static const unsigned int nchann_demux = NTKSECTORS*3 + 3*NCALOSECTORS*NRAWCALOFIBERS + 1 + 1;
    static const unsigned int nchann_decoded = NTKSECTORS*NTKFIBERS + NCALOSECTORS*NCALOFIBERS + NMUFIBERS + 1;
    static const unsigned int nchann_regionized = NTKOUT + NCALOOUT + NEMOUT + NMUOUT + 1;
    static const unsigned int nchann_pf = NTRACK + NCALO + NMU, nchann_puppi = NTRACK + NCALO, nchann_sort = NPUPPIFINALSORTED;
    static const unsigned int nchann_eg = NEM_EGOUT + NEM_EGOUT, nchann_eg64 = NEM_EGOUT + 2*NEM_EGOUT, nchann_egsort = 1;
    static const unsigned int tk_offs = 0, calo_offs = NTKSECTORS*3;
    static const unsigned int mu_offs_raw = calo_offs + NCALOSECTORS * NRAWCALOFIBERS * 3, mu_offs_decoded = calo_offs + NCALOSECTORS * NCALOFIBERS * 3;
    static const unsigned int vtx_offs_raw = mu_offs_raw + 3, vtx_offs_demux = mu_offs_raw + 1, vtx_offs_decoded = mu_offs_decoded + 1;

    DumpFileReader inputs;
    std::vector<unsigned int> pfRegToRef;
    Channels<nchann_in,64> channelsTM;
    Channels<nchann_demux,64> channelsTDemux, channelsTDemuxSplit;
    Channels<nchann_vu9p,64> channelsVU9P;
    Channels<nchann_vu13p,64> channelsVU13P;
    Channels<nchann_decoded,128> channelsDecode, channelsIn;
    Channels<nchann_regionized,128> channelsReg, channelsRegSplit;
    Channels<nchann_pf,72> channelsPf;
    Channels<nchann_eg,76> channelsEg, channelsEgTkIso;
    Channels<(nchann_eg64+REGOUTII-1)/REGOUTII,64> channelsEgStream, channelsEgTkIsoStream;
    Channels<nchann_egsort,64> channelsEgSorted, channelsEgSortedStream;
    Channels<nchann_puppi,64> channelsPuppi, channelsPuppiStream;
    Channels<nchann_sort,64> channelsPuppiSort, channelsPuppiSortStream, channelsPuppiSortStreamSplit;

    std::vector<int> vu9p_links; // index is tmux link, value is VCU118 link
    std::vector<int> vu13p_links; // index is tmux link, value is VU13P link

    static const unsigned int decoded_validation_size = 3*TLEN+1;
    ap_uint<128> decoded_validation_data[decoded_validation_size][nchann_decoded];
    bool        decoded_validation_valid[decoded_validation_size][nchann_decoded];
    unsigned int decoded_validation_index;
    
    // TMux encoders
    TM18LinkMultiplet<ap_uint<64>,TLEN> tk_tmuxer, calo_tmuxer, mu_tmuxer;
    HGCalHeaderSkipper calo_skipper[3*NCALOSECTORS];
    // TMux decoders, for testing
    TDemuxRef<64> tk_tdemuxer[NTKSECTORS];
    TDemuxRef<256> calo_tdemuxer[NCALOSECTORS];
    MuonStreamMerger mu_merger;
    // make a delay queue for the PV and the muons, to realign them to the first frame
    DelayQueue pv_gttlatency, pv_delayer, mu_delayer; // latency of the TDemuxRef (measured from first valid frame in to first valid frame out)

    l1ct::MultififoRegionizerEmulator regEmulator; // for clock-by-clock outputs
    l1ct::MultififoRegionizerEmulator regEmulator2; // run all in once
    l1ct::PFAlgo2HGCEmulator pfEmulator;
    l1ct::LinPuppiEmulator puEmulator;
    l1ct::PFTkEGAlgoEmulator egEmulator;
    l1ct::PFTkEGSorterEmulator egSorterEmulator;
    l1ct::TrackInputEmulator tkDecoderEmulator;
    l1ct::GMTMuonDecoderEmulator muDecoderEmulator;
    l1ct::HgcalClusterDecoderEmulator hgcalDecoderEmulator;
 
    bool regInit;
    unsigned int frame;

    struct Counters {
        unsigned int ev = 0, tk = 0, had = 0, em = 0, mu = 0, pf = 0, puppi = 0, puppisort = 0, eg = 0, ele = 0;
    } cnt_;
    template<typename T> static unsigned int count_nz(const T & c) { 
        unsigned int ret = 0;
        for (auto & o : c) { if (o.hwPt != 0) ret++; }
        return ret;
    }

    void initLinks();
    void decodeRawTracks(const std::vector<l1ct::DetectorSector<ap_uint<96>>> & raw, std::vector<l1ct::DetectorSector<l1ct::TkObjEmu>> & in) ;
    void decodeRawMuons(const l1ct::DetectorSector<ap_uint<64>> & raw, l1ct::DetectorSector<l1ct::MuObjEmu> & in) ;
    void decodeRawClusters(const std::vector<l1ct::DetectorSector<ap_uint<256>>> & raw, std::vector<l1ct::DetectorSector<l1ct::HadCaloObjEmu>> & in) ;
    void readOneEndcap(int itest, bool newOrbit, l1ct::RawInputs & raw, l1ct::RegionizerDecodedInputs & in, std::vector<l1ct::PFInputRegion> & allpfin) ;
    bool runTMuxAndDemux(int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RawInputs & raw)  ;
    void runRegionizer(const l1ct::RegionizerDecodedInputs & in, const std::vector<l1ct::PFInputRegion> & allpfin, unsigned int nclocks, int indexWithinTrain, bool tailOfTrain) ;
    bool runPFPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) ;
    bool runTkEg(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) ;

    bool run() ;
};

void Tester::initLinks() 
{
    // make a version for the VU9P kit and VU13 with the proper link mapping
    // -- in the tmux vector, we have the first NTKSECTORS*3 mapped to TF, then 3*NCALOSECTORS*NRAWCALOFIBERS, then 3 muons and 1 PV
    // -- in the VU9P:
    //          region 1 fibers 1-3 mapped to muons 
    //          region 1 fiber 4 mapped to PV
    //          regions 2-4 + 25-28 = 28 fibers mapped to tracker (3x9 fibers)
    //          regions 11-19 = 36 fibers mapped to HGCal
    vu9p_links[vtx_offs_raw] = 4+3;
    for (int i =  0; i < 3; ++i) vu9p_links[mu_offs_raw+i] = 4+i;
    for (int i =  0; i < 3*NTKSECTORS; ++i) vu9p_links[tk_offs+i] = (i <= 11) ? (i + 4*2) : ((i-12) + 4*25);
    for (int i =  0; i < 3*NCALOSECTORS*NRAWCALOFIBERS; ++i) vu9p_links[calo_offs+i] = i + 4*11;
    //for (int i =  0; i <= vtx_offs_raw; ++i) {
    //    printf("Input link %3d mapped to VU9P channel %3d\n", i, vu9p_links[i]);
    //}

    // -- in the VU13P:
    //          region 27 fibers 1-3 mapped to muons 
    //          region 27 fiber 4 mapped to PV
    //          regions 0-3 + 28-30 = 28 fibers mapped to tracker (3x9 fibers)
    //          regions 12-14 + 17-22 = 36 fibers mapped to HGCal
    vu13p_links[vtx_offs_raw] = 4*27+3;
    for (int i = 0; i < 3; ++i) vu13p_links[mu_offs_raw+i] = 4*27+i;
    for (int i = 0; i < 3*NTKSECTORS; ++i) vu13p_links[tk_offs+i] = (i <= 14) ? (i + 4*0) : ((i-15) + 4*28);
    for (int i = 0, ic = 0; i < NCALOSECTORS; ++i) {
        unsigned int quad0 = (i == 0) ? 12 : (i == 1 ? 17 : 20);
        for (int j = 0; j < 3*NRAWCALOFIBERS; ++j, ++ic) {
            vu13p_links[calo_offs+ic] = 4*quad0 + j;
        }
    }
    for (int i =  0; i <= vtx_offs_raw; ++i) {
        printf("Input link %3d mapped to VU13P channel %3d\n", i, vu13p_links[i]);
    }
}

void Tester::decodeRawTracks(const std::vector<l1ct::DetectorSector<ap_uint<96>>> & raw, std::vector<l1ct::DetectorSector<l1ct::TkObjEmu>> & in) {
    in.resize(raw.size());
    for (unsigned int isec = 0, nsec = raw.size(); isec < nsec; ++isec) {
        in[isec].region = raw[isec].region;
        in[isec].clear();
        for (const auto &w96 : raw[isec].obj) {
            auto out = tkDecoderEmulator.decodeTrack(w96, raw[isec].region, true);
            if (!out.second) out.first.clear();
            in[isec].obj.push_back(out.first);
        }
    }
}
void Tester::decodeRawMuons(const l1ct::DetectorSector<ap_uint<64>> & raw, l1ct::DetectorSector<l1ct::MuObjEmu> & in) {
    in.clear();
    in.region = raw.region;
    for (const auto & w64 : raw.obj) {
        in.obj.push_back(muDecoderEmulator.decode(w64));
    }

}
void Tester::decodeRawClusters(const std::vector<l1ct::DetectorSector<ap_uint<256>>> & raw, std::vector<l1ct::DetectorSector<l1ct::HadCaloObjEmu>> & in) {
    in.resize(raw.size());
    for (unsigned int isec = 0, nsec = raw.size(); isec < nsec; ++isec) {
        in[isec].region = raw[isec].region;
        in[isec].clear();
        for (const auto &w256 : raw[isec].obj) {
            if (w256) {
                in[isec].obj.push_back(hgcalDecoderEmulator.decode(w256));
                if (in[isec].obj.back().hwPt == 0) in[isec].obj.pop_back();
            }
        }
    }
}
void Tester::readOneEndcap(int itest, bool newOrbit, l1ct::RawInputs & raw, l1ct::RegionizerDecodedInputs & in, std::vector<l1ct::PFInputRegion> & allpfin)
{
    raw.clear(); in.clear(); allpfin.clear();
    for (auto & sec : inputs.event().raw.track) if (sec.region.floatEtaCenter()*HGCAL_SIDE >= 0) raw.track.push_back(sec);
    for (auto & sec : inputs.event().raw.hgcalcluster) if (sec.region.floatEtaCenter()*HGCAL_SIDE >= 0) raw.hgcalcluster.push_back(sec);
    for (auto & sec : inputs.event().decoded.emcalo) if (sec.region.floatEtaCenter()*HGCAL_SIDE >= 0) {
        in.emcalo.push_back(sec); in.emcalo.back().obj.clear(); // put in the sector, but empty since we intercept clusters
    }
    raw.muon = inputs.event().raw.muon;
    raw.muon.obj.resize(NMUON_PROMPT, ap_uint<64>(0));

    for (unsigned int i = 0, n = inputs.event().pfinputs.size(); i < n; ++i) {
        const auto & reg = inputs.event().pfinputs[i];
        if (reg.region.floatEtaCenter()*HGCAL_SIDE >= 0) {
            allpfin.push_back(reg);
            pfRegToRef.push_back(i);
        }
    }

    decodeRawTracks(raw.track, in.track);
    decodeRawMuons(raw.muon, in.muon);
    decodeRawClusters(raw.hgcalcluster, in.hadcalo);

    if (!regInit) { regEmulator.initSectorsAndRegions(in, allpfin); regInit = true; } // this will do clock-cycle emulation
    regEmulator2.run(in, allpfin); // this also but just internally, from the outside it will seem it took no time

    for (auto & sec : raw.track) {
        if (sec.obj.empty()) sec.obj.emplace_back(0);
    }
    tk_tmuxer.push_links(itest, raw.track, pack_w96, newOrbit, /*padWithValid=*/false);

    std::vector<l1ct::DetectorSector<ap_uint<256>>> hgcalFullPacket;
    for (unsigned int isec = 0; isec < raw.hgcalcluster.size(); ++isec) {
        hgcalFullPacket.emplace_back();
        auto & osec = hgcalFullPacket.back();
        osec.region = raw.hgcalcluster[isec].region;
        osec.obj.reserve(31+raw.hgcalcluster[isec].size());
        ap_uint<256> w256; ap_uint<64> head64;
        for (unsigned int w = 0; w < 4; ++w) {
            head64(63,48) = 0xABC0;       // Magic
            head64(47,38) = 0;            // Opaque
            head64(39,32) = (itest%3)*6;  // TM slice
            head64(31,24) = ((HGCAL_SIDE+1)/2)*3+isec; 
            head64(23,16) = w;            // link
            head64(15,0)  = itest % 3564; // BX
            w256(64*w+63,64*w) = head64;
        }
        osec.obj.push_back(w256);
        for (unsigned int trow = 0; trow < 30; ++trow) {
            for (unsigned int w = 0; w < 4; ++w) {
                w256(64*w+63,64*w) = 4*trow + w;
            }
            osec.obj.push_back(w256);
        }
        for (const auto & c : raw.hgcalcluster[isec]) {
            osec.obj.push_back(c);
        }
    }
    calo_tmuxer.push_links_parallel<4>(itest, hgcalFullPacket, newOrbit, /*padWithValid=*/false);
    mu_tmuxer.push_link(itest, raw.muon, [](const l1ct::DetectorSector<ap_uint<64>> &v){return v.obj;}, newOrbit);
}

bool Tester::runTMuxAndDemux(int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RawInputs & raw)
{
    bool ok = true;
    int ilink;
    for (int iclock = 0; iclock < nclocks; ++iclock, ++frame) {
        // pop out frames from the tmuxer for printing. for each sector, we put the 3 links for 3 set of events next to each other
        tk_tmuxer.pop_frame(channelsTM, tk_offs, true, tailOfTrain);
        calo_tmuxer.pop_frame_parallel<4>(channelsTM, calo_offs, true, tailOfTrain);
        mu_tmuxer.pop_frame(channelsTM, mu_offs_raw, true, tailOfTrain);
        // the vertex is not TMUXed so we just add it at the end, but delayed by GTT latency
        ap_uint<64> pvword_gtt = !tailOfTrain ? inputs.event().pv_emu(iclock) : ap_uint<64>(0); 
        bool pvword_gtt_orbit =  !tailOfTrain ? (iclock == 0 && indexWithinTrain == 0) : false;
        bool pvword_gtt_start =  !tailOfTrain ? (iclock == 0)                 : false;
        bool pvword_gtt_end   =  !tailOfTrain ? (iclock == 9)                 : false;
        bool pvword_gtt_valid =  !tailOfTrain ? (iclock < 10)                 : false;
        pv_gttlatency(pvword_gtt, 
                      pvword_gtt_orbit, 
                      pvword_gtt_start, 
                      pvword_gtt_end, 
                      pvword_gtt_valid, 
                      channelsTM.data[vtx_offs_raw], 
                      channelsTM.orbit[vtx_offs_raw], 
                      channelsTM.start[vtx_offs_raw], 
                      channelsTM.end[vtx_offs_raw], 
                      channelsTM.valid[vtx_offs_raw]);
        channelsTM.dump();

        // make also version with vcu118 link mapping
        for (ilink = 0; ilink <= vtx_offs_raw; ++ilink) {
            channelsVU9P.data[vu9p_links[ilink]] = channelsTM.data[ilink];
            channelsVU9P.orbit[vu9p_links[ilink]] = channelsTM.orbit[ilink];
            channelsVU9P.start[vu9p_links[ilink]] = channelsTM.start[ilink];
            channelsVU9P.end[vu9p_links[ilink]] = channelsTM.end[ilink];
            channelsVU9P.valid[vu9p_links[ilink]] = channelsTM.valid[ilink];
        }
        channelsVU9P.dump();
        // make also version with vu13p link mapping
        for (ilink = 0; ilink <= vtx_offs_raw; ++ilink) {
            channelsVU13P.data[vu13p_links[ilink]] = channelsTM.data[ilink];
            channelsVU13P.orbit[vu13p_links[ilink]] = channelsTM.orbit[ilink];
            channelsVU13P.start[vu13p_links[ilink]] = channelsTM.start[ilink];
            channelsVU13P.end[vu13p_links[ilink]] = channelsTM.end[ilink];
            channelsVU13P.valid[vu13p_links[ilink]] = channelsTM.valid[ilink];
        }
        channelsVU13P.dump();

        // now let's run the time demultiplexer
        for (int s = 0; s < NTKSECTORS; ++s) {
          tk_tdemuxer[s](&channelsTM.data[3 * s],
                         &channelsTM.orbit[3 * s],
                         &channelsTM.start[3 * s],
                         &channelsTM.end[3 * s],
                         &channelsTM.valid[3 * s],
                         &channelsTDemux.data[3 * s],
                         &channelsTDemux.orbit[3 * s],
                         &channelsTDemux.start[3 * s],
                         &channelsTDemux.end[3 * s],
                         &channelsTDemux.valid[3 * s]);
        }
        for (int s = 0; s < NCALOSECTORS; ++s) {
            ap_uint<256+4> calo_tmux[3], calo_tdemux[3];
            for (unsigned int t = 0; t < 3; ++t) {
                ilink = calo_offs + (3*s+t)*NRAWCALOFIBERS;
                ok = ok && calo_skipper[3 * s + t](&channelsTM.data[ilink],
                                                   &channelsTM.orbit[ilink],
                                                   &channelsTM.start[ilink],
                                                   &channelsTM.end[ilink],
                                                   &channelsTM.valid[ilink],
                                                   calo_tmux[t]);
            }
            calo_tdemuxer[s](calo_tmux, calo_tdemux);
            assert(NCALOFIBERS == 3);
            for (int t = 0; t < 3; ++t) {
                ilink = calo_offs + (3*s+t)*NRAWCALOFIBERS;
                for (int f = 0; f < NRAWCALOFIBERS; ++f) {
                    channelsTDemux.data[ ilink + f] = calo_tdemux[t](64*f+63,64*f);
                    channelsTDemux.orbit[ilink + f] = calo_tdemux[t][256+0];
                    channelsTDemux.start[ilink + f] = calo_tdemux[t][256+1];
                    channelsTDemux.end[ilink + f] = calo_tdemux[t][256+2];
                    channelsTDemux.valid[ilink + f] = calo_tdemux[t][256+3];
                }
            }
        }

        // muons are merged and delayed, not demultiplexed
        ap_uint<68> mu_merged;
        mu_merger.merge(channelsTM.wext(mu_offs_raw+0), 
                        channelsTM.wext(mu_offs_raw+1),
                        channelsTM.wext(mu_offs_raw+2),
                        mu_merged);
        channelsTDemux.setExt(mu_offs_raw, mu_delayer(mu_merged));
;
        // note: the PV is delayed to realign it to the other demuxed channels
        pv_delayer(pvword_gtt, 
                      pvword_gtt_orbit, 
                      pvword_gtt_start, 
                      pvword_gtt_end, 
                      pvword_gtt_valid, 
                      channelsTDemux.data[vtx_offs_demux], 
                      channelsTDemux.orbit[vtx_offs_demux], 
                      channelsTDemux.start[vtx_offs_demux], 
                      channelsTDemux.end[vtx_offs_demux], 
                      channelsTDemux.valid[vtx_offs_demux]);


        channelsTDemux.dump();
        channelsTDemuxSplit.copy(channelsTDemux).dump();

        // and now we unpack and decode
        ilink = 0; unsigned int iout = 0;
        for (int s = 0; s < NTKSECTORS; ++s, ilink += 3) {
            ap_uint<96> tkword[2];
            tkword[0](63, 0) = channelsTDemux.data[ilink+0](63, 0);
            tkword[0](95,64) = channelsTDemux.data[ilink+1](31, 0);
            tkword[1](31, 0) = channelsTDemux.data[ilink+1](63,32);
            tkword[1](95,32) = channelsTDemux.data[ilink+2](63, 0);
            channelsDecode.valid[iout+0] = channelsTDemux.valid[ilink+0] && channelsTDemux.valid[ilink+1];
            channelsDecode.valid[iout+1] = channelsTDemux.valid[ilink+1] && channelsTDemux.valid[ilink+2];
            channelsDecode.orbit[iout+0] = (channelsTDemux.orbit[ilink+0] || channelsTDemux.orbit[ilink+1]) && channelsDecode.valid[iout+0];
            channelsDecode.orbit[iout+1] = (channelsTDemux.orbit[ilink+1] || channelsTDemux.orbit[ilink+2]) && channelsDecode.valid[iout+1];
            channelsDecode.start[iout+0] = (channelsTDemux.start[ilink+0] || channelsTDemux.start[ilink+1]) && channelsDecode.valid[iout+0];
            channelsDecode.start[iout+1] = (channelsTDemux.start[ilink+1] || channelsTDemux.start[ilink+2]) && channelsDecode.valid[iout+1];
            for (int t = 0; t < 2; ++t, ++iout) {
                auto out = tkDecoderEmulator.decodeTrack(tkword[t], raw.track[s].region, true);
                if (!out.second) out.first.clear();
                channelsDecode.data[iout] = out.first.pack();
            }
        }
        for (int s = 0; s < NCALOSECTORS; ++s) {
            assert(NRAWCALOFIBERS == 4);
            for (int c = 0; c < NCALOFIBERS; ++c) {
                ap_uint<256> hgcword = 0;
                bool valid = true, orbit = false, start = false, end = false;
                for (int b = 0; b < 4; ++b) {
                    hgcword(b*64+63,b*64) = channelsTDemux.data[ilink  + 4*c + b];
                    valid = valid &&        channelsTDemux.valid[ilink + 4*c + b];
                    orbit = orbit ||        channelsTDemux.orbit[ilink + 4*c + b];
                    start = start ||        channelsTDemux.start[ilink + 4*c + b];
                    end   = end   ||        channelsTDemux.end[ilink + 4*c + b];
                }
                channelsDecode.data[iout]  = hgcalDecoderEmulator.decode(hgcword).pack();
                channelsDecode.valid[iout] = valid;
                channelsDecode.orbit[iout] = orbit;
                channelsDecode.start[iout] = start;
                channelsDecode.end[iout]   = end;

                iout++;
            }
            ilink += 3*NRAWCALOFIBERS;
        }
        // decode muons (only 1)
        assert(NMUFIBERS==1);
        channelsDecode.data[iout] = muDecoderEmulator.decode(channelsTDemux.data[ilink]).pack();
        channelsDecode.orbit[iout] = channelsTDemux.orbit[ilink];
        channelsDecode.start[iout] = channelsTDemux.start[ilink];
        channelsDecode.end[iout]   = channelsTDemux.end[ilink];
        channelsDecode.valid[iout] = channelsTDemux.valid[ilink];
        ilink++; iout++;
        // the vertex is trivial
        ap_uint<l1ct::PVObj::BITWIDTH> pv_decoded; 
        packed_l1vertex_input_convert(channelsTDemux.data [ilink], 
                                      channelsTDemux.valid[ilink], 
                                      pv_decoded,  
                                      channelsDecode.valid[iout]);
        channelsDecode.data[iout]  = pv_decoded;
        channelsDecode.orbit[iout] = channelsTDemux.orbit[ilink];
        channelsDecode.start[iout] = channelsTDemux.start[ilink];
        channelsDecode.end[iout]   = channelsTDemux.end[ilink];
        channelsDecode.dump();

        // VALIDATION
        if (indexWithinTrain >= 2 && (!tailOfTrain ? iclock > 0 : iclock <= 2*TLEN)) {
            bool validation_ok = true;
            int ishift = (!tailOfTrain) ? -3*TLEN-1 : -2*TLEN-1;
            unsigned int dvi = (decoded_validation_index + iclock + ishift + decoded_validation_size) % (decoded_validation_size);
            for (unsigned int i = 0; i <= iout; ++i) {
                if (channelsDecode.data[i]  != decoded_validation_data[dvi][i] ||
                    channelsDecode.valid[i] != decoded_validation_valid[dvi][i]) {
                    if (validation_ok == true) { // print this only once
                        std::cout << "error in decoded validation at itest " << itest << " (within train " << indexWithinTrain << ", tail? " << tailOfTrain << ")" <<
                                        ", iclock " << iclock << " (tail? " << tailOfTrain <<") vs index " << dvi << ", decoded_validation_index " << decoded_validation_index << std::endl;
                        validation_ok = false;
                    }
                    std::cout << "channel " << i << " : " <<
                                 "expected " << decoded_validation_data[dvi][i].to_string(16) << " (valid? " << decoded_validation_valid[dvi][i] << "), " <<
                                 "found " << channelsDecode.data[i].to_string(16) << " (valid? " << channelsDecode.valid[i] << ")" << std::endl;
                    ok = false;
                }
            }
            if (!ok) break;
        }

    }
    return ok;

}

void Tester::runRegionizer(const l1ct::RegionizerDecodedInputs & in, const std::vector<l1ct::PFInputRegion> & allpfin, unsigned int nclocks, int indexWithinTrain, bool tailOfTrain) 
{
    int ilink;
    for (int iclock = 0; iclock < nclocks; ++iclock) {
        // emulate regionizer
        std::vector<l1ct::TkObjEmu> tk_links_in, tk_out;
        std::vector<l1ct::EmCaloObjEmu> em_links_in, em_out; // not used but needed by interface
        std::vector<l1ct::HadCaloObjEmu> calo_links_in, calo_out;
        std::vector<l1ct::MuObjEmu> mu_links_in, mu_out;
        std::vector<bool> tk_links_valid, calo_links_valid, mu_links_valid;

        if (!tailOfTrain) {
            regEmulator.fillLinks(iclock, in, tk_links_in, tk_links_valid);
            // for HGCal, we have first 11 clock cycles of null words while header & towers are transmitted
            if (iclock >= 11) {
                regEmulator.fillLinks(iclock-11, in, calo_links_in, calo_links_valid);
            } else {
                calo_links_in.resize(NCALOSECTORS*NCALOFIBERS); // null data
                calo_links_valid.resize(NCALOSECTORS * NCALOFIBERS);  // null data
                for (auto & c : calo_links_in) c.clear();
                std::fill(calo_links_valid.begin(), calo_links_valid.end(), iclock != 0);
            }
            regEmulator.fillLinks(iclock, in, mu_links_in, mu_links_valid);
            cnt_.tk += count_nz(tk_links_in);
            cnt_.had += count_nz(calo_links_in);
            cnt_.mu += count_nz(mu_links_in);

            ilink = 0; channelsIn.init(indexWithinTrain == 1, iclock == 0, false, (iclock < TLEN-1));
            for (int isec = 0, itk = 0; isec < NTKSECTORS; ++isec)  {
                for (int ifiber = 0; ifiber < NTKFIBERS; ++ifiber, ++itk, ++ilink) {
                    channelsIn.valid[ilink] = tk_links_valid[itk] || (iclock == 0 && ifiber == 0);
                    channelsIn.data[ilink] = tk_links_in[itk].pack();
                }
            }
            for (int isec = 0, icalo = 0; isec < NCALOSECTORS; ++isec) {
                for (int ifiber = 0; ifiber < NCALOFIBERS; ++ifiber, ++icalo, ++ilink) {
                    channelsIn.valid[ilink] = calo_links_valid[icalo];
                    channelsIn.data[ilink] = calo_links_in[icalo].pack();
                }
            }
            for (int imu = 0; imu < NMUFIBERS; ++imu, ++ilink) {
                //channelsIn.valid[ilink] = mu_links_valid[imu];
                channelsIn.valid[ilink] = iclock < NMUON_PROMPT;
                channelsIn.data[ilink] = mu_links_in[imu].pack();
            }
            channelsIn.valid[ilink] = inputs.event().pvs.size() > unsigned(iclock);
            channelsIn.data[ilink++] = inputs.event().pv(iclock).pack();
            channelsIn.dump();

            // put good decoded data for validation in the ring buffer
            for (unsigned int i = 0; i < nchann_decoded; ++i) {
                decoded_validation_data[decoded_validation_index][i] = channelsIn.data[i];
                decoded_validation_valid[decoded_validation_index][i] = channelsIn.valid[i];
            }
            decoded_validation_index = (decoded_validation_index + 1) % (decoded_validation_size);
        } else {
            tk_links_in.resize(NTKSECTORS*NTKFIBERS);
            calo_links_in.resize(NCALOSECTORS*NCALOFIBERS);
            mu_links_in.resize(NMUFIBERS);
            for (auto & t : tk_links_in) t.clear();
            for (auto & c : calo_links_in) c.clear();
            for (auto & m : mu_links_in) m.clear();
        }

        bool newevt_ref = (iclock == 0);
        regEmulator.step(newevt_ref,
                         tk_links_in, calo_links_in, em_links_in, mu_links_in,
                         tk_out, calo_out, em_out, mu_out,
                         ROUTER_ISMUX);
        cnt_.em += count_nz(em_out);

        unsigned int ireg = iclock/REGOUTII; 
        bool region_valid = ireg < allpfin.size();
        bool orbit = (indexWithinTrain == 1) && iclock == 0;
        bool start = iclock == 0;
        bool end = iclock == (allpfin.size()*REGOUTII-1);
        ilink = 0; channelsReg.init(orbit, start, end, region_valid);
        for (int i = 0; i < NTKOUT; ++i) 
            channelsReg.data[ilink++] = tk_out[i].pack(); 
        for (int i = 0; i < NCALOOUT; ++i) 
            channelsReg.data[ilink++] = calo_out[i].pack(); 
        for (int i = 0; i < NEMOUT; ++i)
            channelsReg.data[ilink++] = em_out[i].pack();
        for (int i = 0; i < NMUOUT; ++i) 
            channelsReg.data[ilink++] = mu_out[i].pack();
        channelsReg.data[ilink++] = region_valid ? allpfin[ireg].region.pack() : ap_uint<l1ct::PFRegion::BITWIDTH>(0);

        if (indexWithinTrain > 0) {
            channelsReg.dump();
            channelsRegSplit.copy(channelsReg).dump();
        }
    }
}

bool Tester::runPFPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) 
{

    l1ct::OutputRegion pfout;
    int ilink;
    cnt_.ev++;
    bool ok = true;

    for (int ireg = 0; ireg < NPFREGIONS; ++ireg) {
        if (debug) printf("Will run PF event %d, region %d\n", itest, ireg);
        bool orb_tag = newOrbit && ireg == 0;
        bool start_tag = (ireg == 0);
        bool end_tag = (ireg == NPFREGIONS-1);
        pfEmulator.setDebug(debug);
        pfEmulator.run(allpfin[ireg], pfout);
        pfEmulator.mergeNeutrals(pfout);
        cnt_.pf += count_nz(pfout.pfcharged);
        cnt_.pf += count_nz(pfout.pfneutral);

#ifndef NO_CHECK_CMSSW_REF
        const auto & cmsswPFC = inputs.event().out[pfRegToRef[ireg]].pfcharged;
        const auto & cmsswPFN = inputs.event().out[pfRegToRef[ireg]].pfneutral;
        bool match = count_nz(pfout.pfcharged) == count_nz(cmsswPFC) && count_nz(pfout.pfneutral) == count_nz(cmsswPFN);
        for (int i = 0, n = std::min<int>(pfout.pfcharged.size(), cmsswPFC.size()); i < n; ++i) {
            if (!pf_equals(cmsswPFC[i], pfout.pfcharged[i], "PFcharged", i)) match = false;
        }
        for (int i = 0, n = std::min<int>(pfout.pfneutral.size(), cmsswPFC.size()); i < n; ++i) {
            if (!pf_equals(cmsswPFN[i], pfout.pfneutral[i], "PFneutral", i)) match = false;
        }
        if (debug || !match) {
            printf("Done comparison between this PF (%u/%u) and the CMSSW one (%u/%u). %s\n", 
                        count_nz(pfout.pfcharged), count_nz(pfout.pfneutral), 
                        count_nz(cmsswPFC), count_nz(cmsswPFN),
                        match ? "ok" : "FAILED");
            if (!match) ok = false;
        }
#endif

        channelsPf.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (int i = 0, n = pfout.pfcharged.size(), ilink = 0; i < n; ++i, ++ilink)
            channelsPf.data[ilink] = pfout.pfcharged[i].pack();
        for (int i = 0, n = pfout.pfneutral.size(), ilink = NTRACK; i < n; ++i, ++ilink)
            channelsPf.data[ilink] = pfout.pfneutral[i].pack();
        for (int i = 0, n = pfout.pfmuon.size(), ilink = NTRACK+NCALO; i < n; ++i, ++ilink)
            channelsPf.data[ilink] = pfout.pfmuon[i].pack();

        // Puppi objects
        if (debug) printf("Will run Puppi with z0 = %d in event %d, region %d\n", inputs.event().pv().hwZ0.to_int(), itest, ireg);
        puEmulator.setDebug(debug);

        std::vector<l1ct::PVObjEmu> pvs(NPV, inputs.event().pv());

        std::vector<l1ct::PuppiObjEmu> outallch, outallne_nocut, outallne, outselne;
        puEmulator.linpuppi_chs_ref(allpfin[ireg].region, pvs, pfout.pfcharged, outallch);
        puEmulator.linpuppi_ref(allpfin[ireg].region, allpfin[ireg].track, pvs, pfout.pfneutral, outallne_nocut, outallne, outselne);

        outallch.resize(NTRACK);
        outallne.resize(NCALO);
        outallch.insert(outallch.end(), outallne.begin(), outallne.end());
        ilink = 0; channelsPuppi.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (auto & pup : outallch) channelsPuppi.data[ilink++] = pup.pack();
        cnt_.puppi += count_nz(outallch);

        pfout.puppi.resize(NPUPPIFINALSORTED);
        folded_hybrid_bitonic_sort_and_crop_ref(NTRACK+NCALO, NPUPPIFINALSORTED, &outallch[0], &pfout.puppi[0]);
        cnt_.puppisort += count_nz(pfout.puppi);

        ilink = 0;
        channelsPuppiSort.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS - 1, true);
        for (auto & pup : pfout.puppi) channelsPuppiSort.data[ilink++] = pup.pack();

#ifndef NO_CHECK_CMSSW_REF
        const auto & cmsswPuppi = inputs.event().out[pfRegToRef[ireg]].puppi;
        match = count_nz(pfout.puppi) == count_nz(cmsswPuppi);
        for (int i = 0, n = std::min<int>(pfout.puppi.size(), cmsswPuppi.size()); i < n; ++i) {
            if (!puppi_equals(cmsswPuppi[i], pfout.puppi[i], "Puppi", i)) match = false;
        }
        if (debug || !match) {
            printf("Done comparison between this puppi (%u) and the CMSSW one (%u). %s\n", 
                    count_nz(pfout.puppi), count_nz(cmsswPuppi),
                    match ? "ok" : "FAILED");
            if (!match) ok = false;
        }
#endif

        channelsPf.dumpNCopies(6);
        channelsPuppi.dumpNCopies(6);
        channelsPuppiSort.dumpNCopies(6);
        channelsPuppiStream.copy(channelsPuppi).dump();
        channelsPuppiSortStream.copy(channelsPuppiSort).dump();
        channelsPuppiSortStreamSplit.copy(channelsPuppiSort).dump();

        if (!ok) break;
    }

    return ok;
}


bool Tester::runTkEg(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) 
{

    std::vector<l1ct::OutputRegion> egouts(NPFREGIONS);
    std::vector<unsigned int> indices(NPFREGIONS);
    int ilink;
    bool ok = true;

    for (int ireg = 0; ireg < NPFREGIONS; ++ireg) {
        l1ct::OutputRegion & egout = egouts[ireg]; indices[ireg] = ireg;
        if (debug) printf("Will run TkEG event %d, region %d\n", itest, ireg);
        if (debug) egEmulator.setDebug(3);
        else egEmulator.setDebug(0);
        egEmulator.run(allpfin[ireg], egout);

        channelsEg.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (int i = 0, n = egout.egphoton.size(), ilink = 0; i < n; ++i, ++ilink){
            channelsEg.data[ilink] = egout.egphoton[i].pack();
        }
        for (int i = 0, n = egout.egelectron.size(), ilink = NEM_EGOUT; i < n; ++i, ++ilink){
            channelsEg.data[ilink] = egout.egelectron[i].pack();
        }
        if(debug) {
          for (const auto & o : egout.egphoton) { 
              if (o.hwPt) printf("Event %2d region %d:     photon   with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str());
          }
          for (const auto & o : egout.egelectron) { 
              if (o.hwPt) printf("Event %2d region %d:     electron with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str()); 
          }
        }
        egEmulator.runIso(allpfin[ireg], inputs.event().pvs, egout);
        cnt_.eg += count_nz(egout.egphoton);
        cnt_.ele += count_nz(egout.egelectron);

        int nphotons = 0, neles = 0;
        channelsEgTkIso.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (int i = 0, n = egout.egphoton.size(), ilink = 0; i < n; ++i, ++ilink){
            if(egout.egphoton[i].hwPt > 0) nphotons++;
            channelsEgTkIso.data[ilink] = egout.egphoton[i].pack();
        }
        for (int i = 0, n = egout.egelectron.size(), ilink = NEM_EGOUT; i < n; ++i, ++ilink){
            if(egout.egelectron[i].hwPt > 0) neles++;
            channelsEgTkIso.data[ilink] = egout.egelectron[i].pack();
        }        
        if(debug) {
          std::cout << "# photons: " << nphotons << " # eles: " << neles << std::endl;
          for (const auto & o : egout.egphoton) { 
              if (o.hwPt) printf("Event %2d region %d: iso photon   with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str());
          }
          for (const auto & o : egout.egelectron) { 
              if (o.hwPt) printf("Event %2d region %d: iso electron with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str()); 
          }
        }

        
#ifndef NO_CHECK_CMSSW_REF
        const std::vector<l1ct::EGIsoObjEmu> & cmsswPho = inputs.event().out[pfRegToRef[ireg]].egphoton;
        const std::vector<l1ct::EGIsoEleObjEmu> & cmsswEle = inputs.event().out[pfRegToRef[ireg]].egelectron;
        bool match = count_nz(egout.egphoton) == count_nz(cmsswPho) && count_nz(egout.egelectron) == count_nz(cmsswEle);
        if (!match) {
            HumanReadablePatternSerializer debugHR("-", /*zerosuppress=*/true); // this will print on stdout, we'll use it for errors
            debugHR.dump_tkeg("Standalone Photon", egout.egphoton);
            debugHR.dump_tkeg("CMSSW      Photon", cmsswPho);
            debugHR.dump_tkele("Standalone Electron", egout.egelectron);
            debugHR.dump_tkele("CMSSW      Electron", cmsswEle);
        }
        for (int i = 0, n = std::min<int>(egout.egphoton.size(), cmsswPho.size()); i < n; ++i) {
            if (!egiso_equals(cmsswPho[i], egout.egphoton[i],   "EG Photon", i)) match = false;
        }
        for (int i = 0, n = std::min<int>(egout.egelectron.size(), cmsswEle.size()); i < n; ++i) {
            if (!egisoele_equals(cmsswEle[i], egout.egelectron[i], "EG Electron", i)) match = false;
        }
        if (debug || !match) {
            printf("Done comparison between this EG Photons & Electrons (%u, %u) and the CMSSW ones (%u, %u). %s\n", 
                    count_nz(egout.egphoton), 
                    count_nz(egout.egelectron), 
                    count_nz(cmsswPho),
                    count_nz(cmsswEle),
                    match ? "ok" : "FAILED");
            if (!match) ok = false;
        }
#endif


        channelsEg.dumpNCopies(6);
        channelsEgTkIso.dumpNCopies(6);

        // now make more 64-bit friendly versions
        ap_uint<64> eg64[nchann_eg64], egiso64[nchann_eg64];
        assert(l1ct::EGIsoObj::BITWIDTH <= 64);  // photons are <= 64 bits
        assert(64 < l1ct::EGIsoEleObj::BITWIDTH && l1ct::EGIsoEleObj::BITWIDTH <= 128);  // electrons are > 64 bits, but within 128
        for (int i = 0; i < NEM_EGOUT; ++i) {
            eg64[i] = channelsEg.data[i];
            egiso64[i] = channelsEgTkIso.data[i];
        }
        for (int ii = NEM_EGOUT, io = NEM_EGOUT; ii < 2*NEM_EGOUT; ++ii, io += 2) {
            eg64[io+0] = channelsEg.data[ii](63,0);
            eg64[io+1] = channelsEg.data[ii](l1ct::EGIsoEleObj::BITWIDTH-1,64);
            egiso64[io+0] = channelsEgTkIso.data[ii](63,0);
            egiso64[io+1] = channelsEgTkIso.data[ii](l1ct::EGIsoEleObj::BITWIDTH-1,64);
        }

        for (int j = 0; j < 6; ++j) {
            bool orbit = newOrbit && ireg == 0 && j == 0; 
            bool start = ireg == 0 && j == 0;
            bool end = ireg == NPFREGIONS-1 && j == 5;
            // general case
            channelsEgStream.init(orbit, start, end, j < REGOUTII); 
            channelsEgTkIsoStream.init(orbit, start, end, j < REGOUTII);
            if (j < REGOUTII) {
                for (unsigned int i = j, k = 0; i < nchann_eg64; i += REGOUTII, ++k) {
                    channelsEgStream.data[k] = eg64[i];
                    channelsEgTkIsoStream.data[k] = egiso64[i];
                } 
            }
            // override for iso egamma at 360 MHz
            if (REGOUTII == 6 && NEM_EGOUT <= 6) {
                channelsEgTkIsoStream.init(orbit, start, end, true);
                if (j < NEM_EGOUT) {
                    channelsEgTkIsoStream.data[0] = egiso64[j];
                    channelsEgTkIsoStream.data[1] = egiso64[2*j+0+NEM_EGOUT];
                    channelsEgTkIsoStream.data[2] = egiso64[2*j+1+NEM_EGOUT];
                }
            }
            channelsEgStream.dump();
            channelsEgTkIsoStream.dump();
        }

        if (!ok) break;
    }

    // final sorter
    egSorterEmulator.setDebug(debug);
    std::vector<l1ct::EGIsoObjEmu> perBoardPho;
    std::vector<l1ct::EGIsoEleObjEmu> perBoardEle;
    egSorterEmulator.run<l1ct::EGIsoObjEmu>(allpfin, egouts, indices, perBoardPho);
    egSorterEmulator.run<l1ct::EGIsoEleObjEmu>(allpfin, egouts, indices, perBoardEle);

#ifndef NO_CHECK_CMSSW_REF
    int board_out = HGCAL_SIDE > 0 ? 1 : 0;
    const std::vector<l1ct::EGIsoObjEmu> & cmsswPho = inputs.event().board_out[board_out].egphoton;
    const std::vector<l1ct::EGIsoEleObjEmu> & cmsswEle = inputs.event().board_out[board_out].egelectron;
    bool match = count_nz(perBoardPho) == count_nz(cmsswPho) && count_nz(perBoardEle) == count_nz(cmsswEle);
    if (!match) {
        HumanReadablePatternSerializer debugHR("-", /*zerosuppress=*/true); // this will print on stdout, we'll use it for errors
        debugHR.dump_tkeg("Sorted Standalone Photon", perBoardPho);
        debugHR.dump_tkeg("Sorted CMSSW      Photon", cmsswPho);
        debugHR.dump_tkele("Sorted Standalone Electron", perBoardEle);
        debugHR.dump_tkele("Sorted CMSSW      Electron", cmsswEle);
    }
    for (int i = 0, n = std::min<int>(perBoardPho.size(), cmsswPho.size()); i < n; ++i) {
        if (!egiso_equals(cmsswPho[i], perBoardPho[i],   "EG Photon", i)) match = false;
    }
    for (int i = 0, n = std::min<int>(perBoardEle.size(), cmsswEle.size()); i < n; ++i) {
        if (!egisoele_equals(cmsswEle[i], perBoardEle[i], "EG Electron", i)) match = false;
    }
    if (debug || !match) {
        printf("Done comparison between this sorted EG Photons & Electrons (%u, %u) and the CMSSW ones (%u, %u). %s\n",
                count_nz(perBoardPho),
                count_nz(perBoardEle),
                count_nz(cmsswPho),
                count_nz(cmsswEle),
                match ? "ok" : "FAILED");
        if (!match) ok = false;
    }
#endif

    unsigned int i64 = 0;
    for (int i = 0; i < NL1_EGOUT ; ++i, ++i64) {
        channelsEgSorted.init(newOrbit && i == 0, i == 0, false, true);
        channelsEgSorted.data[0]  = i < perBoardPho.size() ? perBoardPho[i].pack() : ap_uint<l1ct::EGIsoObjEmu::BITWIDTH>(0);
        channelsEgSorted.dump(); 
        channelsEgSortedStream.copy(channelsEgSorted).dump();
    }
    for (int i = 0; i < NL1_EGOUT ; ++i) {
        ap_uint<128> ele128 = i < perBoardEle.size() ? perBoardEle[i].pack() : ap_uint<l1ct::EGIsoEleObjEmu::BITWIDTH>(0);
        for (int j = 0; j < 128; j += 64, ++i64) {
            channelsEgSorted.init(false, false, i == NL1_EGOUT - 1 && j != 0, true);
            channelsEgSorted.data[0]  = ele128(j+63,j);
            channelsEgSorted.dump(); 
            channelsEgSortedStream.copy(channelsEgSorted).dump();
        }
    }
    for (; i64 < TLEN; ++i64) {
        channelsEgSorted.init(false, false, false, false);
        channelsEgSorted.dump(); 
        channelsEgSortedStream.copy(channelsEgSorted).dump();
    }

    return ok;
}



bool Tester::run() {
    unsigned int events_per_chunk = 12;
    unsigned int frame = 0, ilink; 
    bool ok = true, firstOfTrain = true;
    l1ct::PVObjEmu pv_prev; // we have 1 event of delay in the reference regionizer, so we need to use the PV from 54 clocks before

    if (NTEST == 48) { // skip to a test with some valid e/gamma objects
        for (int i = 0; i < 17; ++i) {
            if (!inputs.nextEvent()) return false;
        }
    }
    for (int itest = 0, trainStart = 0; itest < NTEST; ++itest) {
        if (!inputs.nextEvent()) break;
        printf("Processing event %d run:lumi:event %u:%u:%lu (train Start %d, index in train %d)\n",
                itest, inputs.event().run, inputs.event().lumi, inputs.event().event, trainStart, itest-trainStart);

        // now we make a single endcap setup
        l1ct::RawInputs raw; l1ct::RegionizerDecodedInputs in; std::vector<l1ct::PFInputRegion> allpfin;
        readOneEndcap(itest, itest - trainStart <= 2, raw, in, allpfin);
        if (itest == 0) {
            printf("Tk   eta: center %d --> center %d, halfsize %d , extra %d \n", in.track.front().region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
            printf("Calo eta: center %d --> center %d, halfsize %d , extra %d \n", in.hadcalo.front().region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
            printf("Mu   eta: center %d --> center %d, halfsize %d , extra %d \n", in.muon.region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
        }

        runRegionizer(in, allpfin, TLEN, itest - trainStart, /*tail=*/false);
        ok = runPFPuppi(itest, itest == trainStart, allpfin, itest <= 1) && ok;
        ok = runTkEg(itest, itest == trainStart, allpfin, itest <= 1) && ok;
        
        ok = runTMuxAndDemux(itest, itest - trainStart, TLEN, /*tail=*/false, raw) && ok;

        #ifdef PAUSES
        if (!ok) break;
        if (itest - trainStart == (events_per_chunk-1)) { // put a pause after N events
            unsigned int pause_length = 300;
            assert(pause_length > GTTLATENCY);
            // flush data: regionizer
            runRegionizer(in, allpfin, TLEN, itest - trainStart, /*tail=*/true);
            // flush data and add pause; tdemux and inputs
            ok = runTMuxAndDemux(itest + 1, itest + 1 - trainStart, pause_length, /*tail=*/true, raw) && ok;
            // add pause in the expected outputs
            channelsIn.dumpNulls(pause_length);
            channelsReg.dumpNulls(pause_length);
            channelsPf.dumpNulls(pause_length);
            channelsEg.dumpNulls(pause_length);
            channelsEgTkIso.dumpNulls(pause_length);
            channelsEgSorted.dumpNulls(pause_length);
            channelsPuppi.dumpNulls(pause_length);
            channelsPuppiSort.dumpNulls(pause_length);
            channelsPuppiSortStream.dumpNulls(pause_length/6);
            // re-init muxers
            tk_tmuxer.init(); calo_tmuxer.init(); mu_tmuxer.init(); 
            for (unsigned int isec = 0; isec < NCALOSECTORS; ++isec) {
                for (unsigned int itime = 0; itime < 3; ++itime) {
                    calo_skipper[3*isec+itime].reset();
                }
            }
            decoded_validation_index = 0; frame = 0;
            trainStart = itest + 1;
        }
        #endif
        if (((itest % events_per_chunk) == (events_per_chunk-1)) && (itest != NTEST-1)) { 
            channelsVU9P.newFile(); 
            channelsVU13P.newFile(); 
            channelsTDemuxSplit.newFile();
            channelsRegSplit.newFile();
            channelsPuppiStream.newFile();
            channelsPuppiSortStreamSplit.newFile();
            channelsEgStream.newFile();
            channelsEgTkIsoStream.newFile();
            channelsEgSortedStream.newFile();
        }
        if (!ok) break;
    } 

    return ok;
}

int main(int argc, char **argv) {
    Tester test(argv[1]);
    return test.run() ? 0 : 1;
}

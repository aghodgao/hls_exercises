#ifndef REG_Barrel
#define REG_Barrel
#endif
#define ROUTER_STREAM
#define ROUTER_ISMUX 1
#define ROUTER_ISSTREAM 1
#ifndef PFFREQ
#define PFFREQ 240
#define REGOUTII 2
#define REGPAUSEII 1
#endif
#include "../correlator-common/l1-regionizer/multififo/firmware/regionizer.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/regionizer/multififo_regionizer_ref.h"
#include "../correlator-common/utils/pattern_serializer.h"
#include "../correlator-common/utils/test_utils.h"
#include "../correlator-common/utils/DumpFileReader.h"
#include "../correlator-common/l1-regionizer/multififo/firmware/dummy_obj_unpackers.h"
#include "../correlator-common/l1-regionizer/multififo/utils/dummy_obj_packers.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/tdemux_ref.h"
#include "../correlator-common/l1-regionizer/multififo/tdemux/firmware/tdemux.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/pf/pfalgo3_ref.h"
#include "../correlator-common/pf/firmware/pfalgo3.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/puppi/linpuppi_ref.h"
#include "../correlator-common/puppi/firmware/linpuppi.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/common/bitonic_hybrid_sort_ref.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/egamma/pftkegalgo_ref.h"
#include "../correlator-common/l1-converters/pv/firmware/vertex.h"
#include "../correlator-common/l1-converters/tracks/firmware/tkinput_barrel.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/tkinput_ref.h"
#include "../correlator-common/l1-converters/muons/firmware/muonGmtToL1ct.h"
#include "L1Trigger/Phase2L1ParticleFlow/interface/l1-converters/muonGmtToL1ct_ref.h"
#include "../correlator-common/l1-converters/muons/firmware/muoninput.h"
#include "../correlator-common/l1-converters/muons/ref/muoninput_ref.h"
#include "utils/tmux18_utils.h"
#include "utils/channels.h"

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <vector>
#include <memory>

#define TLEN REGIONIZERNCLOCKS 
#define NRAWCALOFIBERS (NCALOFIBERS+NEMFIBERS) 

#ifndef GTTLATENCY
#if PFFREQ == 240
#define GTTLATENCY 3*(TLEN-2)+10
#else
#define GTTLATENCY 3*(TLEN-2)+120
#endif
#endif

#ifndef NTEST
#define NTEST 48
#endif

#define PAUSES

namespace reco {
  template <typename T>
  inline T reduceRange(T x) {
    T o2pi = 1. / (2. * M_PI);
    if (std::abs(x) <= T(M_PI))
      return x;
    T n = std::round(x * o2pi);
    return x - n * T(2. * M_PI);
  }
  inline double deltaPhi(double phi1, double phi2) { return reduceRange(phi1 - phi2); }
}  // namespace reco



struct Tester {

    Tester(const std::string & inputFile) :
        inputs(inputFile.c_str()),
        channelsTM("input-emp.txt"), 
        channelsTDemux("input-emp-tdemux.txt.gz"),
        channelsVU9P("input-emp-vu9p.%03d.txt.gz"),
        channelsDecode("input-emp-decoded.txt.gz"), 
        channelsIn("input-emp-decoded-ref.txt.gz"),
        channelsReg("output-emp-regionized-ref.txt.gz"),
        channelsRegSplit("output-emp-regionized-ref.%03d.txt.gz"),
        channelsPf("output-emp-pf-ref.txt.gz"),
        channelsPuppi("output-emp-puppi-ref.txt.gz"),
        channelsPuppiSort("output-emp-puppisort-ref.txt.gz"),
        channelsPuppiSortStream("output-emp-puppisort-stream-ref.txt.gz",REGOUTII+REGPAUSEII),
        channelsPuppiSortStreamSplit("output-emp-puppisort-stream-ref.%03d.txt.gz",REGOUTII+REGPAUSEII),
        channelsEg("output-emp-eg-ref.txt.gz"),
        channelsEgTkIso("output-emp-egtkiso-ref.txt.gz"),        
        channelsEgTkIsoStream("output-emp-egtkiso-stream-ref.txt.gz"),
        channelsEgTkIsoStreamSplit("output-emp-egtkiso-stream-ref.%03d.txt.gz"),
        vu9p_links(vtx_offs_raw+1, 0), // index is tmux link, value is VCU118 link
        decoded_validation_index(0),
        tk_tmuxer(NTKSECTORS), mu_tmuxer(1,NMUON_PROMPT),
        mu_merger(NMUON_PROMPT),
        pv_gttlatency(GTTLATENCY), // real delay from GTT latency (for input files)
        pv_delayer(TLEN*2+1),      // fake delay to realign with decoded inputs for validation
        mu_delayer(TLEN*2+1),
        calo_delayer(NCALOSECTORS*(NCALOFIBERS+NEMFIBERS), DelayQueue(TLEN*2+1)),
#ifdef BARREL9R
        barrelSetup(l1ct::MultififoRegionizerEmulator::BarrelSetup::Phi9),
#else
        barrelSetup(l1ct::MultififoRegionizerEmulator::BarrelSetup::Phi18),
#endif // BARREL9R
        regEmulator(barrelSetup, NCALOFIBERS, NEMFIBERS, REGIONIZERNCLOCKS, NTRACK, NCALO, NEMCALO, NMU, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, true),
        regEmulator2(barrelSetup, NCALOFIBERS, NEMFIBERS, REGIONIZERNCLOCKS, NTRACK, NCALO, NEMCALO, NMU, ROUTER_ISSTREAM, REGOUTII, REGPAUSEII, true),
        pfEmulator(NTRACK,NEMCALO,NCALO,NMU, 
                       NPHOTON,NSELCALO,NALLNEUTRALS,
                       PFALGO_DR2MAX_TK_MU, PFALGO_DR2MAX_TK_EM, PFALGO_DR2MAX_EM_CALO, PFALGO_DR2MAX_TK_CALO,
                       PFALGO_TK_MAXINVPT_LOOSE, PFALGO_TK_MAXINVPT_TIGHT),
        puEmulator(NTRACK, NALLNEUTRALS, NNEUTRALS, NPV,
                          LINPUPPI_DR2MIN, LINPUPPI_DR2MAX, LINPUPPI_iptMax, LINPUPPI_dzCut,
                          LINPUPPI_ptSlopeNe, LINPUPPI_ptSlopePh, LINPUPPI_ptZeroNe, LINPUPPI_ptZeroPh, 
                          LINPUPPI_alphaSlope, LINPUPPI_alphaZero, LINPUPPI_alphaCrop, 
                          LINPUPPI_priorNe, LINPUPPI_priorPh,
                          LINPUPPI_ptCut),
        egEmulator(l1ct::PFTkEGAlgoEmuConfig(NTRACK, NTRACK_EGIN, NEMCALO_EGIN, NEM_EGOUT, 
                                            /*filterHwQuality=*/false,
                                            /*doBremRecovery=*/false,
                                            /*writeBeforeBremRecovery=*/false,
                                            /*caloHwQual=*/1,
                                            /*doEndcapQuality=*/false,
                                            /*emClusterPtMin=*/0.0)),
        tkDecoderEmulator(l1ct::TrackInputEmulator::Region::Barrel, l1ct::TrackInputEmulator::Encoding::Biased, /*bitwise=*/true, /*slim=*/true),
        muDecoderEmulator(GMT_Z0_SCALE, GMT_DXY_SCALE),
        regInit(false), 
        frame(0)
    { 
        initLinks();
        const float ptErr_edges[PTERR_BINS]  = PTERR_EDGES;
        const float ptErr_offss[PTERR_BINS]  = PTERR_OFFS;
        const float ptErr_scales[PTERR_BINS] = PTERR_SCALE;
        pfEmulator.loadPtErrBins(PTERR_BINS, ptErr_edges, ptErr_scales, ptErr_offss);   
        tkDecoderEmulator.configPhi(phi_BITS);
        tkDecoderEmulator.configZ0(z0_BITS);
        tkDecoderEmulator.configDEtaBarrel(calcDEtaBarrel_BITS,calcDEtaBarrel_Z0PRESHIFT,calcDEtaBarrel_Z0POSTSHIFT, 0.0);
        tkDecoderEmulator.configDPhiBarrel(calcDPhiBarrel_BITS,calcDPhiBarrel_RINVPRESHIFT,calcDPhiBarrel_RINVPOSTSHIFT, 0.0);
        tkDecoderEmulator.configEta(tanlLUT_BITS,0,15-tanlLUT_BITS,tanlLUT_POSTOFFS,true,false);
        tkDecoderEmulator.configPt(ptLUT_BITS);
    }
        
    ~Tester() {
        printf("Processes %u events with %u tk, %u had, %u em, %u mu. Found %u pf, %u puppi, %u sorted puppi, %u e/g, %u ele\n",
                        cnt_.ev, cnt_.tk, cnt_.had, cnt_.em, cnt_.mu, cnt_.pf, cnt_.puppi, cnt_.puppisort, cnt_.eg, cnt_.ele);
    }

    static const unsigned int nchann_in = NTKSECTORS*3 + NCALOSECTORS*NRAWCALOFIBERS + 3 + 1, nchann_vu9p = 30*4;
    static const unsigned int nchann_demux = NTKSECTORS*3 + NCALOSECTORS*NRAWCALOFIBERS + 1 + 1;
    static const unsigned int nchann_decoded = NTKSECTORS*NTKFIBERS + NCALOSECTORS*NRAWCALOFIBERS + NMUFIBERS + 1;
    static const unsigned int nchann_regionized = NTKOUT + NCALOOUT + NEMOUT + NMUOUT + 1;
    static const unsigned int nchann_pf = NTRACK + NPHOTON + NSELCALO + NMU, nchann_puppi = NTRACK + NALLNEUTRALS, nchann_sort = NPUPPIFINALSORTED;
    static const unsigned int nchann_eg = NEM_EGOUT + NEM_EGOUT, nchann_eg64 = NEM_EGOUT + 2*NEM_EGOUT;
    static const unsigned int nchann_eg_pho_stream = (NEM_EGOUT +REGOUTII+REGPAUSEII-1)/(REGOUTII+REGPAUSEII);
    static const unsigned int nchann_eg_ele_stream = (NEM_EGOUT +REGOUTII+REGPAUSEII-1)/(REGOUTII+REGPAUSEII);
    static const unsigned int nchann_eg_stream64 = nchann_eg_pho_stream + 2*nchann_eg_ele_stream;
    static const unsigned int tk_offs = 0, calo_offs = NTKSECTORS*3;
    static const unsigned int mu_offs_raw = calo_offs + NCALOSECTORS*NRAWCALOFIBERS, mu_offs_decoded = calo_offs + NCALOSECTORS * NRAWCALOFIBERS;
    static const unsigned int vtx_offs_raw = mu_offs_raw + 3, vtx_offs_demux = mu_offs_raw + 1, vtx_offs_decoded = mu_offs_decoded + 1;

    DumpFileReader inputs;
    std::vector<unsigned int> pfRegToRef;
    Channels<nchann_in,64> channelsTM;
    Channels<nchann_demux,64> channelsTDemux;
    Channels<nchann_vu9p,64> channelsVU9P;
    Channels<nchann_decoded,72> channelsDecode, channelsIn;
    Channels<nchann_regionized,72> channelsReg, channelsRegSplit;
    Channels<nchann_pf,72> channelsPf;
    Channels<nchann_puppi,64> channelsPuppi;
    Channels<nchann_sort,64> channelsPuppiSort, channelsPuppiSortStream, channelsPuppiSortStreamSplit;
    Channels<nchann_eg,76> channelsEg, channelsEgTkIso;
    Channels<nchann_eg_stream64,64> channelsEgTkIsoStream, channelsEgTkIsoStreamSplit;
    std::vector<int> vu9p_links; // index is tmux link, value is VCU118 link

    static const unsigned int decoded_validation_size = 3*TLEN+1;
    ap_uint<72> decoded_validation_data[decoded_validation_size][nchann_decoded];
    bool        decoded_validation_valid[decoded_validation_size][nchann_decoded];
    unsigned int decoded_validation_index;
    
    // TMux encoders
    TM18LinkMultiplet<ap_uint<64>,TLEN> tk_tmuxer, mu_tmuxer;
    // TMux decoders, for testing
    TDemuxRef<64> tk_tdemuxer[NTKSECTORS];
    MuonStreamMerger mu_merger;
    // make a delay queue for the PV and the muons, to realign them to the first frame
    DelayQueue pv_gttlatency, pv_delayer, mu_delayer; // latency of the TDemuxRef (measured from first valid frame in to first valid frame out)
    std::vector<DelayQueue> calo_delayer;

    l1ct::MultififoRegionizerEmulator::BarrelSetup barrelSetup;
    l1ct::MultififoRegionizerEmulator regEmulator; // for clock-by-clock outputs
    l1ct::MultififoRegionizerEmulator regEmulator2; // run all in once
    l1ct::PFAlgo3Emulator pfEmulator;
    l1ct::LinPuppiEmulator puEmulator;
    l1ct::PFTkEGAlgoEmulator egEmulator;
    l1ct::TrackInputEmulator tkDecoderEmulator;
    l1ct::GMTMuonDecoderEmulator muDecoderEmulator;
 
    bool regInit;
    unsigned int frame;

    struct Counters {
        unsigned int ev = 0, tk = 0, had = 0, em = 0, mu = 0, pf = 0, puppi = 0, puppisort = 0, eg = 0, ele = 0;
    } cnt_;
    template<typename T> static unsigned int count_nz(const T & c) { 
        unsigned int ret = 0;
        for (auto & o : c) { if (o.hwPt != 0) ret++; }
        return ret;
    }


    template<typename T>
    bool hasSector(const l1ct::DetectorSector<T> & reg) {
        if (barrelSetup == l1ct::MultififoRegionizerEmulator::BarrelSetup::Phi9 || barrelSetup == l1ct::MultififoRegionizerEmulator::BarrelSetup::Phi18) {
            return std::abs(reco::deltaPhi(reg.region.floatPhiCenter(), 2*M_PI/9)) <= 5*M_PI/9;
        }
        return true;
    }
    bool hasRegion(const l1ct::PFInputRegion & reg) {
        if (barrelSetup == l1ct::MultififoRegionizerEmulator::BarrelSetup::Central9 || barrelSetup == l1ct::MultififoRegionizerEmulator::BarrelSetup::Central18) {
            return (std::abs(reg.region.floatEtaCenter()) <= 0.5);
        } else if (barrelSetup == l1ct::MultififoRegionizerEmulator::BarrelSetup::Phi9 || barrelSetup == l1ct::MultififoRegionizerEmulator::BarrelSetup::Phi18) {
            return std::abs(reco::deltaPhi(reg.region.floatPhiCenter(), 2*M_PI/9)) <= M_PI/3;
        }
        return true;
    }


    void initLinks();
    void decodeRawTracks(const std::vector<l1ct::DetectorSector<ap_uint<96>>> & raw, std::vector<l1ct::DetectorSector<l1ct::TkObjEmu>> & in) ;
    void decodeRawMuons(const l1ct::DetectorSector<ap_uint<64>> & raw, l1ct::DetectorSector<l1ct::MuObjEmu> & in) ;
    void readInputs(int itest, bool newOrbit, l1ct::RawInputs & raw, l1ct::RegionizerDecodedInputs & in, std::vector<l1ct::PFInputRegion> & allpfin) ;
    bool runTMuxAndDemux(int itest,
                         int indexWithinTrain,
                         int nclocks,
                         bool tailOfTrain,
                         const l1ct::RegionizerDecodedInputs &in,
                         const l1ct::RawInputs &raw);
    void runRegionizer(const l1ct::RegionizerDecodedInputs &in,
                       const std::vector<l1ct::PFInputRegion> &allpfin,
                       unsigned int nclocks,
                       unsigned int indexWithinTrainn,
                       bool tailOfTrain);
    bool runPFPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) ;
    bool runTkEg(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) ;

    bool run() ;
};

void Tester::initLinks() 
{
    // make a version for the VCU118 kit and VU9P with the proper link mapping
    // -- in the tmux vector, we have the first NTKSECTORS*3 mapped to TF, then 3*NCALOSECTORS*NRAWCALOFIBERS, then 3 muons and 1 PV
    // -- in the VCU118:
    //          region 1 fibers 1-3 mapped to muons 
    //          region 1 fiber 4 mapped to PV
    //          regions 82-4 + 25-28 = 28 fibers mapped to tracker (3x9 fibers)
    //          regions 11-19 = 36 fibers mapped to HGCal
    printf("tk offs %u, calo offs %u, mu offs raw %u, vtx offs raw %u\n", tk_offs, calo_offs, mu_offs_raw, vtx_offs_raw);

    vu9p_links[vtx_offs_raw] = 4*28+3;
    for (int i =  0; i < 3; ++i) vu9p_links[mu_offs_raw+i] = 4*17+i;
    for (int i =  0; i < 15;           ++i) vu9p_links[tk_offs+i] =    i   + 4*1;
    for (int i = 15; i < 3*NTKSECTORS; ++i) vu9p_links[tk_offs+i] = (i-15) + 4*25;
    for (int i =  0; i < NCALOSECTORS*NRAWCALOFIBERS; ++i) vu9p_links[calo_offs+i] = i + 4*18;
    std::vector<int> check(nchann_vu9p,-1);
    bool errors = false;
    for (int i = 0; i <= vtx_offs_raw; ++i) {
        if (check[vu9p_links[i]] != -1) { 
            printf("ERROR: links %d and %d both go to VCU118 link %d\n", i, check[vu9p_links[i]], vu9p_links[i]);
            errors = true;
        }
        check[vu9p_links[i]] = i;
    }
    for (int i =  0; i <= vtx_offs_raw; ++i) {
        printf("Input link %3d mapped to VCU channel %3d\n", i, vu9p_links[i]);
    }
    fflush(stdout);
    assert(!errors);
}

void Tester::decodeRawTracks(const std::vector<l1ct::DetectorSector<ap_uint<96>>> & raw, std::vector<l1ct::DetectorSector<l1ct::TkObjEmu>> & in) {
    in.resize(raw.size());
    for (unsigned int isec = 0, nsec = raw.size(); isec < nsec; ++isec) {
        in[isec].region = raw[isec].region;
        in[isec].clear();
        for (const auto &w96 : raw[isec].obj) {
            auto out = tkDecoderEmulator.decodeTrack(w96, raw[isec].region, true);
            if (!out.second) out.first.clear();
            in[isec].obj.push_back(out.first);
        }
    }
}
void Tester::decodeRawMuons(const l1ct::DetectorSector<ap_uint<64>> & raw, l1ct::DetectorSector<l1ct::MuObjEmu> & in) {
    in.clear();
    in.region = raw.region;
    for (const auto & w64 : raw.obj) {
        in.obj.push_back(muDecoderEmulator.decode(w64));
    }
}
void Tester::readInputs(int itest, bool newOrbit, l1ct::RawInputs & raw, l1ct::RegionizerDecodedInputs & in, std::vector<l1ct::PFInputRegion> & allpfin)
{
    raw.clear(); in.clear(); allpfin.clear();
    for (int i = 0; i < 18; i += 9) {
        raw.track.push_back(inputs.event().raw.track[8+i]);
        for (int j = 0; j < 4; ++j) {
            raw.track.push_back(inputs.event().raw.track[j+i]);
        }
    }
    for (auto & sec : inputs.event().decoded.hadcalo) { 
        if (hasSector(sec)) in.hadcalo.push_back(sec);
    }
    for (auto & sec : inputs.event().decoded.emcalo) {
        if (hasSector(sec)) in.emcalo.push_back(sec);
    }
    raw.muon = inputs.event().raw.muon;
    raw.muon.obj.resize(NMUON_PROMPT, ap_uint<64>(0));

    for (unsigned int i = 0, n = inputs.event().pfinputs.size(); i < n; ++i) {
        const auto & reg = inputs.event().pfinputs[i];
        if (hasRegion(reg)) {
            allpfin.push_back(reg);
            pfRegToRef.push_back(i);
        }
    }

    decodeRawTracks(raw.track, in.track);
    decodeRawMuons(raw.muon, in.muon);

    if (!regInit) { regEmulator.initSectorsAndRegions(in, allpfin); regInit = true; } // this will do clock-cycle emulation
    regEmulator2.run(in, allpfin); // this also but just internally, from the outside it will seem it took no time

    for (auto & sec : raw.track) {
        if (sec.obj.empty()) sec.obj.emplace_back(0);
    }
    tk_tmuxer.push_links(itest, raw.track, pack_w96, newOrbit, /*padWithValid=*/false);
    mu_tmuxer.push_link(itest, raw.muon, [](const l1ct::DetectorSector<ap_uint<64>> &v){return v.obj;}, newOrbit);
}

bool Tester::runTMuxAndDemux(int itest, int indexWithinTrain, int nclocks, bool tailOfTrain, const l1ct::RegionizerDecodedInputs & in, const l1ct::RawInputs & raw)
{
    bool ok = true;
    int ilink;
    for (int iclock = 0; iclock < nclocks; ++iclock, ++frame) {
        // pop out frames from the tmuxer for printing. for each sector, we put the 3 links for 3 set of events next to each other
        tk_tmuxer.pop_frame(channelsTM, tk_offs, true, tailOfTrain);
        mu_tmuxer.pop_frame(channelsTM, mu_offs_raw, true, tailOfTrain);
        // calorimeter is not muxed 
        for (int s = 0, ilink = 0; s < NCALOSECTORS; ++s) {
            bool orbit = (indexWithinTrain == 0);
            for (int f = 0; f < NCALOFIBERS; ++f, ++ilink) {
                int ical = iclock*NCALOFIBERS + f;
                if (!tailOfTrain && ical < in.hadcalo[s].size()) {
                    channelsTM.data[calo_offs+ilink] = in.hadcalo[s][ical].pack();
                    channelsTM.orbit[calo_offs+ilink] = (iclock == 0) && (indexWithinTrain == 0);
                    channelsTM.start[calo_offs+ilink] = (iclock == 0);
                    channelsTM.end[calo_offs+ilink] = (iclock+1)*NCALOFIBERS + f >= in.hadcalo[s].size();
                    channelsTM.valid[calo_offs+ilink] = true;
                } else {
                    channelsTM.data[calo_offs+ilink] = 0;
                    channelsTM.orbit[calo_offs+ilink] = !tailOfTrain && (iclock == 0) && (indexWithinTrain == 0);
                    channelsTM.start[calo_offs+ilink] = !tailOfTrain && (iclock == 0);
                    channelsTM.end[calo_offs+ilink] = !tailOfTrain && (iclock == 0);
                    channelsTM.valid[calo_offs+ilink] = !tailOfTrain && (iclock == 0);
                }
            }
            for (int f = 0; f < NEMFIBERS; ++f, ++ilink) {
                int ical = iclock*NEMFIBERS + f;
                if (!tailOfTrain && ical < in.emcalo[s].size()) {
                    channelsTM.data[calo_offs+ilink] = in.emcalo[s][ical].pack();
                    channelsTM.orbit[calo_offs+ilink] = (iclock == 0) && (indexWithinTrain == 0);
                    channelsTM.start[calo_offs+ilink] = (iclock == 0);
                    channelsTM.end[calo_offs+ilink] = (iclock+1)*NEMFIBERS + f >= in.emcalo[s].size();
                    channelsTM.valid[calo_offs+ilink] = true;
                } else {
                    channelsTM.data[calo_offs+ilink] = 0;
                    channelsTM.orbit[calo_offs+ilink] = !tailOfTrain && (iclock == 0) && (indexWithinTrain == 0);
                    channelsTM.start[calo_offs+ilink] = !tailOfTrain && (iclock == 0);
                    channelsTM.end[calo_offs+ilink] = !tailOfTrain && (iclock == 0);
                    channelsTM.valid[calo_offs+ilink] = !tailOfTrain && (iclock == 0);
                }
            }
        }

        // the vertex is not TMUXed so we just add it at the end, but delayed by GTT latency
        ap_uint<64> pvword_gtt = !tailOfTrain ? inputs.event().pv_emu(iclock) : ap_uint<64>(0); 
        bool pvword_gtt_orbit =  !tailOfTrain ? (iclock == 0 && indexWithinTrain == 0) : false;
        bool pvword_gtt_start =  !tailOfTrain ? (iclock == 0)                 : false;
        bool pvword_gtt_end   =  !tailOfTrain ? (iclock == 9)                 : false;
        bool pvword_gtt_valid =  !tailOfTrain ? (iclock < 10)                 : false;
        pv_gttlatency(pvword_gtt, 
                      pvword_gtt_orbit, 
                      pvword_gtt_start, 
                      pvword_gtt_end, 
                      pvword_gtt_valid, 
                      channelsTM.data[vtx_offs_raw], 
                      channelsTM.orbit[vtx_offs_raw], 
                      channelsTM.start[vtx_offs_raw], 
                      channelsTM.end[vtx_offs_raw], 
                      channelsTM.valid[vtx_offs_raw]);
        channelsTM.dump();

        // make also version with vu9p link mapping
        for (ilink = 0; ilink <= vtx_offs_raw; ++ilink) {
            channelsVU9P.data[vu9p_links[ilink]] = channelsTM.data[ilink];
            channelsVU9P.orbit[vu9p_links[ilink]] = channelsTM.orbit[ilink];
            channelsVU9P.start[vu9p_links[ilink]] = channelsTM.start[ilink];
            channelsVU9P.end[vu9p_links[ilink]] = channelsTM.end[ilink];
            channelsVU9P.valid[vu9p_links[ilink]] = channelsTM.valid[ilink];
        }
        channelsVU9P.dump();

        // now let's run the time demultiplexer
        for (int s = 0; s < NTKSECTORS; ++s) {
          tk_tdemuxer[s](&channelsTM.data[3 * s],
                         &channelsTM.orbit[3 * s],
                         &channelsTM.start[3 * s],
                         &channelsTM.end[3 * s],
                         &channelsTM.valid[3 * s],
                         &channelsTDemux.data[3 * s],
                         &channelsTDemux.orbit[3 * s],
                         &channelsTDemux.start[3 * s],
                         &channelsTDemux.end[3 * s],
                         &channelsTDemux.valid[3 * s]);
        }
        for (int s = 0, ic = 0; s < NCALOSECTORS; ++s) {
            for (int f = 0; f < NRAWCALOFIBERS; ++f, ++ic) {
              calo_delayer[ic](channelsTM.data[calo_offs + ic],
                               channelsTM.orbit[calo_offs + ic],
                               channelsTM.start[calo_offs + ic],
                               channelsTM.end[calo_offs + ic],
                               channelsTM.valid[calo_offs + ic],
                               channelsTDemux.data[calo_offs + ic],
                               channelsTDemux.orbit[calo_offs + ic],
                               channelsTDemux.start[calo_offs + ic],
                               channelsTDemux.end[calo_offs + ic],
                               channelsTDemux.valid[calo_offs + ic]);
            }
        }
        // muons are merged and delayed, not demultiplexed
        ap_uint<68> mu_merged;
        mu_merger.merge(channelsTM.wext(mu_offs_raw+0), 
                        channelsTM.wext(mu_offs_raw+1),
                        channelsTM.wext(mu_offs_raw+2),
                        mu_merged);
        channelsTDemux.setExt(mu_offs_raw, mu_delayer(mu_merged));
;
        // note: the PV is delayed to realign it to the other demuxed channels
        pv_delayer(pvword_gtt, 
                      pvword_gtt_orbit, 
                      pvword_gtt_start, 
                      pvword_gtt_end, 
                      pvword_gtt_valid, 
                      channelsTDemux.data[vtx_offs_demux], 
                      channelsTDemux.orbit[vtx_offs_demux], 
                      channelsTDemux.start[vtx_offs_demux], 
                      channelsTDemux.end[vtx_offs_demux], 
                      channelsTDemux.valid[vtx_offs_demux]);


        channelsTDemux.dump();

        // and now we unpack and decode
        ilink = 0; unsigned int iout = 0;
        for (int s = 0; s < NTKSECTORS; ++s, ilink += 3) {
            ap_uint<96> tkword[2];
            tkword[0](63, 0) = channelsTDemux.data[ilink+0](63, 0);
            tkword[0](95,64) = channelsTDemux.data[ilink+1](31, 0);
            tkword[1](31, 0) = channelsTDemux.data[ilink+1](63,32);
            tkword[1](95,32) = channelsTDemux.data[ilink+2](63, 0);
            channelsDecode.valid[iout+0] = channelsTDemux.valid[ilink+0] && channelsTDemux.valid[ilink+1];
            channelsDecode.valid[iout+1] = channelsTDemux.valid[ilink+1] && channelsTDemux.valid[ilink+2];
            channelsDecode.orbit[iout+0] = (channelsTDemux.orbit[ilink+0] || channelsTDemux.orbit[ilink+1]) && channelsDecode.valid[iout+0];
            channelsDecode.orbit[iout+1] = (channelsTDemux.orbit[ilink+1] || channelsTDemux.orbit[ilink+2]) && channelsDecode.valid[iout+1];
            channelsDecode.start[iout+0] = (channelsTDemux.start[ilink+0] || channelsTDemux.start[ilink+1]) && channelsDecode.valid[iout+0];
            channelsDecode.start[iout+1] = (channelsTDemux.start[ilink+1] || channelsTDemux.start[ilink+2]) && channelsDecode.valid[iout+1];
            for (int t = 0; t < 2; ++t, ++iout) {
                auto out = tkDecoderEmulator.decodeTrack(tkword[t], raw.track[s].region, true);
                if (!out.second) out.first.clear();
                channelsDecode.data[iout] = out.first.pack();
            }
        }
        for (int s = 0; s < NCALOSECTORS; ++s) {
            for (int f = 0; f < NRAWCALOFIBERS; ++f, ++iout, ++ilink) {
                channelsDecode.data[iout] = channelsTDemux.data[ilink];
                channelsDecode.orbit[iout] = channelsTDemux.orbit[ilink];
                channelsDecode.start[iout] = channelsTDemux.start[ilink];
                channelsDecode.end[iout] = channelsTDemux.end[ilink];
                channelsDecode.valid[iout] = channelsTDemux.valid[ilink];
            }
        }
        // decode muons (only 1)
        assert(NMUFIBERS==1);
        channelsDecode.data[iout] = muDecoderEmulator.decode(channelsTDemux.data[ilink]).pack();
        channelsDecode.orbit[iout] = channelsTDemux.orbit[ilink];
        channelsDecode.start[iout] = channelsTDemux.start[ilink];
        channelsDecode.end[iout]   = channelsTDemux.end[ilink];
        channelsDecode.valid[iout] = channelsTDemux.valid[ilink];
        ilink++; iout++;
        // the vertex is trivial
        ap_uint<l1ct::PVObj::BITWIDTH> pv_decoded; 
        packed_l1vertex_input_convert(channelsTDemux.data [ilink], 
                                      channelsTDemux.valid[ilink], 
                                      pv_decoded,  
                                      channelsDecode.valid[iout]);
        channelsDecode.data[iout]  = pv_decoded;
        channelsDecode.orbit[iout] = channelsTDemux.orbit[ilink];
        channelsDecode.start[iout] = channelsTDemux.start[ilink];
        channelsDecode.end[iout]   = channelsTDemux.end[ilink];
        channelsDecode.dump();

        // VALIDATION
        if (indexWithinTrain >= 2 && (!tailOfTrain ? iclock > 0 : iclock <= 2*TLEN)) {
            bool validation_ok = true;
            int ishift = (!tailOfTrain) ? -3*TLEN-1 : -2*TLEN-1;
            unsigned int dvi = (decoded_validation_index + iclock + ishift + decoded_validation_size) % (decoded_validation_size);
            for (unsigned int i = 0; i <= iout; ++i) {
                if (channelsDecode.data[i]  != decoded_validation_data[dvi][i] ||
                    channelsDecode.valid[i] != decoded_validation_valid[dvi][i]) {
                    if (validation_ok == true) { // print this only once
                        std::cout << "error in decoded validation at itest " << itest << " (within train " << indexWithinTrain << ", tail? " << tailOfTrain << ")" <<
                                        ", iclock " << iclock << " (tail? " << tailOfTrain <<") vs index " << dvi << ", decoded_validation_index " << decoded_validation_index << std::endl;
                        validation_ok = false;
                    }
                    std::cout << "channel " << i << " : " <<
                                 "expected " << decoded_validation_data[dvi][i].to_string(16) << " (valid? " << decoded_validation_valid[dvi][i] << "), " <<
                                 "found " << channelsDecode.data[i].to_string(16) << " (valid? " << channelsDecode.valid[i] << ")" << std::endl;
                    ok = false;
                }
            }
            if (!ok) break;
        }

    }
    return ok;

}

void Tester::runRegionizer(const l1ct::RegionizerDecodedInputs &in,
                           const std::vector<l1ct::PFInputRegion> &allpfin,
                           unsigned int nclocks,
                           unsigned int indexWithinTrain,
                           bool tailOfTrain) {
  int ilink;
  for (int iclock = 0; iclock < nclocks; ++iclock) {
    // emulate regionizer
    std::vector<l1ct::TkObjEmu> tk_links_in, tk_out;
    std::vector<l1ct::EmCaloObjEmu> em_links_in, em_out;  // not used but needed by interface
    std::vector<l1ct::HadCaloObjEmu> calo_links_in, calo_out;
    std::vector<l1ct::MuObjEmu> mu_links_in, mu_out;
    std::vector<bool> tk_links_valid, calo_links_valid, em_links_valid, mu_links_valid;

    if (!tailOfTrain) {
      regEmulator.fillLinks(iclock, in, tk_links_in, tk_links_valid);
      regEmulator.fillLinks(iclock, in, calo_links_in, calo_links_valid);
      regEmulator.fillLinks(iclock, in, em_links_in, em_links_valid);
      regEmulator.fillLinks(iclock, in, mu_links_in, mu_links_valid);
      cnt_.tk += count_nz(tk_links_in);
      cnt_.had += count_nz(calo_links_in);
      cnt_.em += count_nz(em_links_in);
      cnt_.mu += count_nz(mu_links_in);

      ilink = 0;
      channelsIn.clear(/*valid=*/(iclock < TLEN - 1));
      for (int isec = 0, itk = 0; isec < NTKSECTORS; ++isec) {
        for (int ifiber = 0; ifiber < NTKFIBERS; ++ifiber, ++itk, ++ilink) {
          channelsIn.valid[ilink] = tk_links_valid[itk] || (iclock == 0 && ifiber == 0);
          channelsIn.data[ilink] = tk_links_in[itk].pack();
        }
      }

      for (int icalo = 0; icalo < NCALOSECTORS; ++icalo) {
        for (int f = 0; f < NCALOFIBERS; ++f, ++ilink) {
          int iobj = iclock * NCALOFIBERS + f;
          channelsIn.data[ilink] = calo_links_in[icalo * NCALOFIBERS + f].pack();
          channelsIn.valid[ilink] = (iclock == 0) || calo_links_valid[icalo * NCALOFIBERS + f];
        }
        for (int f = 0; f < NEMFIBERS; ++f, ++ilink) {
          int iobj = iclock * NEMFIBERS + f;
          channelsIn.data[ilink] = em_links_in[icalo * NEMFIBERS + f].pack();
          channelsIn.valid[ilink] = (iclock == 0) || em_links_valid[icalo * NEMFIBERS + f];
        }
      }
      for (int imu = 0; imu < NMUFIBERS; ++imu, ++ilink) {
        channelsIn.valid[ilink] = iclock < NMUON_PROMPT;
        channelsIn.data[ilink] = mu_links_in[imu].pack();
      }
      channelsIn.valid[ilink] = inputs.event().pvs.size() > unsigned(iclock);
      channelsIn.data[ilink++] = inputs.event().pv(iclock).pack();
      channelsIn.dump();

      // put good decoded data for validation in the ring buffer
      for (unsigned int i = 0; i < nchann_decoded; ++i) {
        decoded_validation_data[decoded_validation_index][i] = channelsIn.data[i];
        decoded_validation_valid[decoded_validation_index][i] = channelsIn.valid[i];
      }
      decoded_validation_index = (decoded_validation_index + 1) % (decoded_validation_size);
    } else {
      tk_links_in.resize(NTKSECTORS * NTKFIBERS);
      calo_links_in.resize(NCALOSECTORS * NCALOFIBERS);
      em_links_in.resize(NCALOSECTORS * NEMFIBERS);
      mu_links_in.resize(NMUFIBERS);
      for (auto &t : tk_links_in)
        t.clear();
      for (auto &c : calo_links_in)
        c.clear();
      for (auto &c : em_links_in)
        c.clear();
      for (auto &m : mu_links_in)
        m.clear();
    }

    bool newevt_ref = (iclock == 0);
    regEmulator.step(
        newevt_ref, tk_links_in, calo_links_in, em_links_in, mu_links_in, tk_out, calo_out, em_out, mu_out, ROUTER_ISMUX);

    unsigned int ireg = iclock / (REGOUTII + REGPAUSEII);
    bool is_pause = (iclock % (REGOUTII + REGPAUSEII) >= REGOUTII);
    bool region_valid = ireg < allpfin.size();
    ilink = 0;
    bool orbit = (indexWithinTrain == 1) && iclock == 0;
    bool start = (iclock % (REGOUTII + REGPAUSEII)) == 0;
    bool end = (iclock % (REGOUTII + REGPAUSEII)) == REGOUTII - 1;
    channelsReg.init(orbit, start, end, region_valid && !is_pause);
    for (int i = 0; i < NTKOUT; ++i)
      channelsReg.data[ilink++] = tk_out[i].pack();
    for (int i = 0; i < NCALOOUT; ++i)
      channelsReg.data[ilink++] = calo_out[i].pack();
    for (int i = 0; i < NEMOUT; ++i)
      channelsReg.data[ilink++] = em_out[i].pack();
    for (int i = 0; i < NMUOUT; ++i)
      channelsReg.data[ilink++] = mu_out[i].pack();
    channelsReg.data[ilink++] = region_valid ? allpfin[ireg].region.pack() : ap_uint<l1ct::PFRegion::BITWIDTH>(0);

    if (indexWithinTrain > 0) {
      channelsReg.dump();
      channelsRegSplit.copy(channelsReg).dump();
    }
  }
}

bool Tester::runPFPuppi(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) 
{

    l1ct::OutputRegion pfout;
    int ilink;
    cnt_.ev++;
    bool ok = true;

    for (int ireg = 0; ireg < NPFREGIONS; ++ireg) {
        if (debug) printf("Will run PF event %d, region %d\n", itest, ireg);
        bool orb_tag = newOrbit && ireg == 0;
        bool start_tag = (ireg == 0);
        bool end_tag = (ireg == NPFREGIONS-1);
        pfEmulator.setDebug(debug);
        pfEmulator.run(allpfin[ireg], pfout);
        pfEmulator.mergeNeutrals(pfout);
        cnt_.pf += count_nz(pfout.pfcharged);
        cnt_.pf += count_nz(pfout.pfneutral);

#if 1 //def CHECK_CMSSW_REF
        const auto & cmsswPFC = inputs.event().out[pfRegToRef[ireg]].pfcharged;
        const auto & cmsswPFN = inputs.event().out[pfRegToRef[ireg]].pfneutral;
        bool match = count_nz(pfout.pfcharged) == count_nz(cmsswPFC) && count_nz(pfout.pfneutral) == count_nz(cmsswPFN);
        for (int i = 0, n = std::min<int>(pfout.pfcharged.size(), cmsswPFC.size()); i < n; ++i) {
            if (!pf_equals(cmsswPFC[i], pfout.pfcharged[i], "PFcharged", i)) match = false;
        }
        for (int i = 0, n = std::min<int>(pfout.pfneutral.size(), cmsswPFC.size()); i < n; ++i) {
            if (!pf_equals(cmsswPFN[i], pfout.pfneutral[i], "PFneutral", i)) match = false;
        }
        if (debug || !match) {
            printf("Done comparison between this PF (%u/%u) and the CMSSW one (%u/%u). %s\n", 
                        count_nz(pfout.pfcharged), count_nz(pfout.pfneutral), 
                        count_nz(cmsswPFC), count_nz(cmsswPFN),
                        match ? "ok" : "FAILED");
            //if (!match) ok = false;
        }
#endif

        channelsPf.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (int i = 0, n = pfout.pfcharged.size(), ilink = 0; i < n; ++i, ++ilink)
            channelsPf.data[ilink] = pfout.pfcharged[i].pack();
        for (int i = 0, n = pfout.pfneutral.size(), ilink = NTRACK; i < n; ++i, ++ilink)
            channelsPf.data[ilink] = pfout.pfneutral[i].pack();
        for (int i = 0, n = pfout.pfmuon.size(), ilink = NTRACK+NALLNEUTRALS; i < n; ++i, ++ilink)
            channelsPf.data[ilink] = pfout.pfmuon[i].pack();

        // Puppi objects
        if (debug) printf("Will run Puppi with z0 = %d in event %d, region %d\n", inputs.event().pv().hwZ0.to_int(), itest, ireg);
        puEmulator.setDebug(debug);

        std::vector<l1ct::PVObjEmu> pvs(NPV, inputs.event().pv());

        std::vector<l1ct::PuppiObjEmu> outallch, outallne_nocut, outallne, outselne;
        puEmulator.linpuppi_chs_ref(allpfin[ireg].region, pvs, pfout.pfcharged, outallch);
        puEmulator.linpuppi_ref(allpfin[ireg].region, allpfin[ireg].track, pvs, pfout.pfneutral, outallne_nocut, outallne, outselne);

        outallch.resize(NTRACK);
        outallne.resize(NALLNEUTRALS);
        outallch.insert(outallch.end(), outallne.begin(), outallne.end());
        ilink = 0; channelsPuppi.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (auto & pup : outallch) channelsPuppi.data[ilink++] = pup.pack();
        cnt_.puppi += count_nz(outallch);

        pfout.puppi.resize(NPUPPIFINALSORTED);
        folded_hybrid_bitonic_sort_and_crop_ref(NTRACK+NALLNEUTRALS, NPUPPIFINALSORTED, &outallch[0], &pfout.puppi[0]);
        cnt_.puppisort += count_nz(pfout.puppi);

        ilink = 0;
        channelsPuppiSort.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS - 1, true);
        for (auto & pup : pfout.puppi) channelsPuppiSort.data[ilink++] = pup.pack();

#if 1 //def CHECK_CMSSW_REF
        const auto & cmsswPuppi = inputs.event().out[pfRegToRef[ireg]].puppi;
        match = count_nz(pfout.puppi) == count_nz(cmsswPuppi);
        for (int i = 0, n = std::min<int>(pfout.puppi.size(), cmsswPuppi.size()); i < n; ++i) {
            if (!puppi_equals(cmsswPuppi[i], pfout.puppi[i], "Puppi", i)) match = false;
        }
        if (debug || !match) {
            printf("Done comparison between this puppi (%u) and the CMSSW one (%u). %s\n", 
                    count_nz(pfout.puppi), count_nz(cmsswPuppi),
                    match ? "ok" : "FAILED");
            //if (!match) ok = false;
        }
#endif
        const unsigned int OUTII360 = REGOUTII+REGPAUSEII;
        channelsPf.dumpNCopies(OUTII360);
        channelsPuppi.dumpNCopies(OUTII360);
        channelsPuppiSort.dumpNCopies(OUTII360);
        channelsPuppiSortStream.copy(channelsPuppiSort).dump();
        channelsPuppiSortStreamSplit.copy(channelsPuppiSort).dump();

        if (!ok) break;
    }

    return ok;
}


bool Tester::runTkEg(int itest, bool newOrbit, const std::vector<l1ct::PFInputRegion> & allpfin, bool debug) 
{

    l1ct::OutputRegion egout;
    int ilink;
    bool ok = true;

    for (int ireg = 0; ireg < NPFREGIONS; ++ireg) {
        if (debug) printf("Will run TkEG event %d, region %d\n", itest, ireg);
        if (debug) egEmulator.setDebug(3);
        else egEmulator.setDebug(0);
        egEmulator.run(allpfin[ireg], egout);

        channelsEg.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (int i = 0, n = egout.egphoton.size(), ilink = 0; i < n; ++i, ++ilink){
            channelsEg.data[ilink] = egout.egphoton[i].pack();
        }
        for (int i = 0, n = egout.egelectron.size(), ilink = NEM_EGOUT; i < n; ++i, ++ilink){
            channelsEg.data[ilink] = egout.egelectron[i].pack();
        }
        if(debug) {
          for (const auto & o : egout.egphoton) { 
              if (o.hwPt) printf("Event %2d region %d:     photon   with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str());
          }
          for (const auto & o : egout.egelectron) { 
              if (o.hwPt) printf("Event %2d region %d:     electron with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str()); 
          }
        }
        egEmulator.runIso(allpfin[ireg], inputs.event().pvs, egout);
        cnt_.eg += count_nz(egout.egphoton);
        cnt_.ele += count_nz(egout.egelectron);

        int nphotons = 0, neles = 0;
        channelsEgTkIso.init(newOrbit && ireg == 0, ireg == 0, ireg == NPFREGIONS-1, true);
        for (int i = 0, n = egout.egphoton.size(), ilink = 0; i < n; ++i, ++ilink){
            if(egout.egphoton[i].hwPt > 0) nphotons++;
            channelsEgTkIso.data[ilink] = egout.egphoton[i].pack();
        }
        for (int i = 0, n = egout.egelectron.size(), ilink = NEM_EGOUT; i < n; ++i, ++ilink){
            channelsEgTkIso.data[ilink] = egout.egelectron[i].pack();
        }        
        if(debug) {
          std::cout << "# photons: " << nphotons << " # eles: " << neles << std::endl;
          for (const auto & o : egout.egphoton) { 
              if (o.hwPt) printf("Event %2d region %d: iso photon   with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str());
          }
          for (const auto & o : egout.egelectron) { 
              if (o.hwPt) printf("Event %2d region %d: iso electron with pT %6.2f, iso %7d  packed %s\n", itest, ireg, o.floatPt(), o.intIso(), o.pack().to_string(16).c_str()); 
          }
        }

        
#ifdef CHECK_CMSSW_REF
        const std::vector<l1ct::EGIsoObjEmu> & cmsswPho = inputs.event().out[pfRegToRef[ireg]].egphoton;
        const std::vector<l1ct::EGIsoEleObjEmu> & cmsswEle = inputs.event().out[pfRegToRef[ireg]].egelectron;
        bool match = count_nz(egout.egphoton) == count_nz(cmsswPho) && count_nz(egout.egelectron) == count_nz(cmsswEle);
        if (!match) {
            HumanReadablePatternSerializer debugHR("-", /*zerosuppress=*/true); // this will print on stdout, we'll use it for errors
            debugHR.dump_tkeg("Standalone Photon", egout.egphoton);
            debugHR.dump_tkeg("CMSSW      Photon", cmsswPho);
            debugHR.dump_tkele("Standalone Electron", egout.egelectron);
            debugHR.dump_tkele("CMSSW      Electron", cmsswEle);
        }
        for (int i = 0, n = std::min<int>(egout.egphoton.size(), cmsswPho.size()); i < n; ++i) {
            if (!egiso_equals(cmsswPho[i], egout.egphoton[i],   "EG Photon", i)) match = false;
        }
        for (int i = 0, n = std::min<int>(egout.egelectron.size(), cmsswEle.size()); i < n; ++i) {
            if (!egisoele_equals(cmsswEle[i], egout.egelectron[i], "EG Electron", i)) match = false;
        }
        if (debug || !match) {
            printf("Done comparison between this EG Photons & Electrons (%u, %u) and the CMSSW ones (%u, %u). %s\n", 
                    count_nz(egout.egphoton), 
                    count_nz(egout.egelectron), 
                    count_nz(cmsswPho),
                    count_nz(cmsswEle),
                    match ? "ok" : "FAILED");
            //if (!match) ok = false;
        }
#endif

        const unsigned int OUTII360 = REGOUTII+REGPAUSEII;
        channelsEg.dumpNCopies(OUTII360);
        channelsEgTkIso.dumpNCopies(OUTII360);

        // now make more 64-bit friendly versions
        assert(l1ct::EGIsoObj::BITWIDTH <= 64);  // photons are <= 64 bits
        assert(64 < l1ct::EGIsoEleObj::BITWIDTH && l1ct::EGIsoEleObj::BITWIDTH <= 128);  // electrons are > 64 bits, but within 128
        unsigned int outii = REGOUTII+REGPAUSEII;
        for (int j = 0; j < outii; ++j) {
          bool start = ireg == 0 && j == 0;
          bool end = ireg == NPFREGIONS -1 && j == outii - 1;
          channelsEgTkIsoStream.init(newOrbit && start, start, end, true);
          for (int i = 0, ilink = 0; i < nchann_eg_pho_stream; ++i) {
            unsigned int ipho = i * outii + j;
            if (ipho >= NEM_EGOUT)
              break;
            channelsEgTkIsoStream.data[ilink++] = channelsEgTkIso.data[ipho];
            }
            for (int i = 0, ilink = nchann_eg_pho_stream; i < nchann_eg_ele_stream; ++i) {
                unsigned int iele = i*outii+j;
                if (iele >= NEM_EGOUT) break;
                channelsEgTkIsoStream.data[ilink++] = channelsEgTkIso.data[iele+NEM_EGOUT](63,0);
                channelsEgTkIsoStream.data[ilink++] = channelsEgTkIso.data[iele+NEM_EGOUT](l1ct::EGIsoEleObj::BITWIDTH-1,64);
            }
            channelsEgTkIsoStream.dump();
            channelsEgTkIsoStreamSplit.copy(channelsEgTkIsoStream).dump();
        }

        if (!ok) break;
    }
    return ok;
}


bool Tester::run() {
    unsigned int events_per_chunk = 12;
    unsigned int frame = 0, ilink; 
    bool ok = true, firstOfTrain = true;
    l1ct::PVObjEmu pv_prev; // we have 1 event of delay in the reference regionizer, so we need to use the PV from 54 clocks before

    for (int itest = 0, trainStart = 0; itest < NTEST; ++itest) {
        if (!inputs.nextEvent()) break;
        printf("Processing event %d run:lumi:event %u:%u:%lu (train Start %d, index in train %d)\n",
                itest, inputs.event().run, inputs.event().lumi, inputs.event().event, trainStart, itest-trainStart);

        // now we make a single endcap setup
        l1ct::RawInputs raw; l1ct::RegionizerDecodedInputs in; std::vector<l1ct::PFInputRegion> allpfin;
        readInputs(itest, itest - trainStart <= 2, raw, in, allpfin);
        if (itest == 0) {
            printf("Tk   eta: center %d --> center %d, halfsize %d , extra %d \n", in.track.front().region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
            printf("Calo eta: center %d --> center %d, halfsize %d , extra %d \n", in.hadcalo.front().region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
            printf("Mu   eta: center %d --> center %d, halfsize %d , extra %d \n", in.muon.region.intEtaCenter(), allpfin.front().region.intEtaCenter(), allpfin.front().region.hwEtaHalfWidth.to_int(), allpfin.front().region.hwEtaExtra.to_int());
        }

        runRegionizer(in, allpfin, TLEN, itest - trainStart, /*tail=*/false);
        ok = runPFPuppi(itest, itest == trainStart, allpfin, itest <= 2) && ok;
        ok = runTkEg(itest, itest == trainStart, allpfin, itest <= 1) && ok;
        
        ok = runTMuxAndDemux(itest, itest - trainStart, TLEN, /*tail=*/false, in, raw) && ok;

        #ifdef PAUSES
        if (!ok) break;
        if (itest - trainStart == (events_per_chunk-1)) { // put a pause after N events
            unsigned int pause_length = 300;
            assert(pause_length > GTTLATENCY);
            // flush data: regionizer
            runRegionizer(in, allpfin, TLEN, itest - trainStart, /*tail=*/true);
            // flush data and add pause; tdemux and inputs
            ok = runTMuxAndDemux(itest + 1, itest + 1 - trainStart, pause_length, /*tail=*/true, in, raw) && ok;
            // add pause in the expected outputs
            channelsIn.dumpNulls(pause_length);
            channelsReg.dumpNulls(pause_length);
            channelsPf.dumpNulls(pause_length);
            channelsPuppi.dumpNulls(pause_length);
            channelsPuppiSort.dumpNulls(pause_length);
            channelsPuppiSortStream.dumpNulls(pause_length/(REGOUTII+REGPAUSEII));
            channelsEg.dumpNulls(pause_length);
            channelsEgTkIso.dumpNulls(pause_length);
            channelsEgTkIsoStream.dumpNulls(pause_length); // no divide by II since the streaming is done explicitly in C++ 
            // re-init muxers
            tk_tmuxer.init(); mu_tmuxer.init(); 
            decoded_validation_index = 0; frame = 0;
            trainStart = itest + 1;
        }
        #endif
        if (((itest % events_per_chunk) == (events_per_chunk-1)) && (itest != NTEST-1)) { 
            channelsVU9P.newFile(); 
            channelsRegSplit.newFile();
            channelsPuppiSortStreamSplit.newFile();
            channelsEgTkIsoStreamSplit.newFile();
        }
        if (!ok) break;
    } 

    return ok;
}

int main(int argc, char **argv) {
    Tester test(argv[1]);
    return test.run() ? 0 : 1;
}
